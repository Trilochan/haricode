<?php
class ControllerProductCompare extends Controller {
	public function index() {
		$this->load->language('product/compare');

		$this->load->model('catalog/product');

		$this->load->model('catalog/category');

		$this->load->model('tool/image');


		if (!isset($this->session->data['compare'])) {
			$this->session->data['compare'] = array();
		}

		if (isset($this->request->get['remove'])) {

		foreach ($this->session->data['compare'] as $category_id => $category_compare){
			$key = array_search($this->request->get['remove'], $this->session->data['compare'][$category_id]);

			if ($key !== false) {
				unset($this->session->data['compare'][$category_id][$key]);
			}
		}
			$this->session->data['success'] = $this->language->get('text_remove');
			$this->get();
			$this->response->redirect($this->url->link('product/compare', (isset($this->request->get['tab']))? "tab=" . $this->request->get['tab'] : "desk=desk"));
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/compare')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_product'] = $this->language->get('text_product');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_image'] = $this->language->get('text_image');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_availability'] = $this->language->get('text_availability');
		$data['text_rating'] = $this->language->get('text_rating');
		$data['text_summary'] = $this->language->get('text_summary');
		$data['text_weight'] = $this->language->get('text_weight');
		$data['text_dimension'] = $this->language->get('text_dimension');
		$data['text_empty'] = $this->language->get('text_empty');
		$data['button_compare'] = $this->language->get('text_button_compare');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_remove'] = $this->language->get('button_remove');


		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['review_status'] = $this->config->get('config_review_status');

		$temp['products_all'] = array();

		$data['attribute_groups'] = array();

		$data['tab'] = (empty($this->request->get['tab']))? 0: $this->request->get['tab'];

		foreach ($this->session->data['compare'] as $category_id => $category_compare){
			foreach ($category_compare as $key => $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_compare_width'), $this->config->get('config_image_compare_height'));
					} else {
						$image = false;
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}

					if ($product_info['quantity'] <= 0) {
						$availability = $product_info['stock_status'];
					} elseif ($this->config->get('config_stock_display')) {
						$availability = $product_info['quantity'];
					} else {
						$availability = $this->language->get('text_instock');
					}

					$attribute_data = array();

					$attribute_groups = $this->model_catalog_product->getProductAttributes($product_id);

					foreach ($attribute_groups as $attribute_group) {
						foreach ($attribute_group['attribute'] as $attribute) {
							$attribute_data[$attribute['attribute_id']] = $attribute['text'];
						}
					}

					$data['category_name'][$category_id] = $this->model_catalog_category->getCategory($category_id)['name'];

					$data['products_all'][$category_id][$product_id] = array(
						'product_id'   => $product_info['product_id'],
						'name'         => $product_info['name'],
						'thumb'        => $image,
						'price'        => $price,
						'special'      => $special,
						'description'  => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '..',
						'model'        => $product_info['model'],
						'manufacturer' => $product_info['manufacturer'],
						'availability' => $availability,
						'minimum'      => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
						'rating'       => (int)$product_info['rating'],
						'reviews'      => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
						'weight'       => $this->weight->format($product_info['weight'], $product_info['weight_class_id']),
						'length'       => $this->length->format($product_info['length'], $product_info['length_class_id']),
						'width'        => $this->length->format($product_info['width'], $product_info['length_class_id']),
						'height'       => $this->length->format($product_info['height'], $product_info['length_class_id']),
						'attribute'    => $attribute_data,
						'href'         => $this->url->link('product/product', 'product_id=' . $product_id),
						'remove'       => $this->url->link('product/compare', 'remove=' . $product_id . ((isset($this->request->get['desk']))? '&desk=desk' : '&tab=' . $category_id))
					);

					foreach ($attribute_groups as $attribute_group) {
						$data['attribute_groups'][$attribute_group['attribute_group_id']]['name'] = $attribute_group['name'];

						foreach ($attribute_group['attribute'] as $attribute) {
							$data['attribute_groups'][$attribute_group['attribute_group_id']]['attribute'][$attribute['attribute_id']]['name'] = $attribute['name'];
						}
					}
				} else {
					unset($this->session->data['compare'][$key]);
				}
			}
		}


		$this->get();
		$data['continue'] = $this->url->link('common/home');

		$data['url_compare'] = $this->url->link('product/compare');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if(isset($this->request->get['desk'])) $compare_view = "compare_desk.tpl";
		else $compare_view = "compare.tpl";
		if(!empty($data['products_all']))
			if(!isset($data['products_all'][$data['tab']]) || (count($data['products_all'][$data['tab']]) <= 1))
				$compare_view = "compare_desk.tpl";

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/'.$compare_view)) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/'.$compare_view, $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/'.$compare_view, $data));
		}
	}

	public function add() {
		$this->load->language('product/compare');

		$json = array();

		if (!isset($this->session->data['compare'])) {
			$this->session->data['compare'] = array();
		}

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$this->load->model('catalog/category');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (!in_array($this->request->post['product_id'], $this->session->data['compare'])) {
					$product_category = $this->model_catalog_product->getCategories($this->request->post['product_id']);
					foreach ($product_category as $value) { 
						$index = (empty($value['main_category']))? $value['category_id'] : $value['main_category'];
			            $this->session->data['compare'][$index][] = $this->request->post['product_id'];
			        }
			        foreach ($this->session->data['compare'] as $key => $value) {
						if (count($value) >= 6) {
							array_shift($this->session->data['compare'][$key]);
						}
			        }
			}

			$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('product/compare'));

			$json['total'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		}
		$this->get();
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function get()
	{
		// создание и запись данных в сессию о сравниваемых товарах
		$this->load->model('catalog/category');
		$count = array();
		if(!empty($this->session->data['compare'])){
			foreach ($this->session->data['compare'] as $key => $value) {
					if(count($value) > 0){
						$value = array_unique($value);
						$this->session->data['compare'][$key] = $value;
						$count[$this->model_catalog_category->getCategory($key)['name']][0] = count($value);
						$count[$this->model_catalog_category->getCategory($key)['name']][1] = $this->url->link('product/compare', 'tab=' . $key);
					}
					else unset($this->session->data['compare'][$key]);
			}
		}
		$this->session->data['compare_title'] = $count;

		if(isset($this->session->data['compare'])){
	      $keys = array();
	      foreach ($this->session->data['compare'] as $value) {
	                  $key = array_values($value);
	                  $keys = array_merge($key, $keys);
	                }
	      $keys = array_flip($keys);
	      $count_compare = array_unique($keys);
			$this->session->data['count_compare'] = $count_compare;
	    }
	    else unset($this->session->data['count_compare']);
	}

	public function getTitle()
	{
			// получаем название категорий в которых присуцтвуют продукты для сравнения и количество этих продуктов в них
			$this->load->language('product/compare');
			$data['url_compare_desk'] = $this->url->link('product/compare', 'desk=desk');
			$data['text_compare_button'] = $this->language->get('text_compare_button');
		    if(isset($this->session->data['count_compare'])){
		      $data['compare_counter'] = count($this->session->data['count_compare']);
		    }
		    else $data['compare_counter'] = 0;

		    $data['compare_title'] = (isset($this->session->data['compare_title']))? $this->session->data['compare_title'] : array();

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/compare_button.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/compare_button.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/compare_button.tpl', $data));
		}
	}


	public function getStringId()
	{
		// получаем список id продуктов присуцтыующих в сравнении
		$array[0] = implode("');", array_keys($this->session->data['count_compare']))."');";
		$compare = $this->session->data['compare'];
		$result = array();
		foreach ($compare as $key => $value) {
			if(count($value) > 1) {
				foreach ($value as $value_sub) {
					$result[] = $value_sub."=".$key;
				}
			}
		}
		$array[1] = json_encode($result);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($array));
		
	}
}

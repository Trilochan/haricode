<?php
class ControllerProductCategory extends Controller {
	public function index() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

// ############### SMART FILTER  BEGIN ##########################################################
		$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		header('X-link:' . $actual_link);

		//###################################
		if(isset($this->request->get['path'])){
			$parts = explode('_', (string)$this->request->get['path']);
		}else $parts = array(0);
		$category_id = (int)array_pop($parts);
		// manufacture to array
		if(isset($this->request->get['manuf']))
		{
			$filter_manuf = explode(',', $this->request->get['manuf']);
			$filter_manufacture = array();
			foreach ($filter_manuf as  $filter_manuf_id) {
				$filter_manufacture[] = (int)$filter_manuf_id;
			}
		}
		else $filter_manufacture = array();
		// stock to normalise
		if(isset($this->request->get['stock']))
		{
			$filter_stock = (int)$this->request->get['stock'];
		}
		else $filter_stock = "";
		// filters to normalise
		if(isset($this->request->get['filter']))
		{
			$filter_filter = explode(',',$this->request->get['filter']);
			$filter_filters = array();
			foreach ($filter_filter as  $filter_filter_id) {
				$filter_filters[] = (int)$filter_filter_id;
			}
		}
		else $filter_filters = array();
		$filter_filters = implode(',', $filter_filters);

		if(isset($this->request->get['activefilters'])){
			if((!empty($filter_filters)) || (!empty($filter_stock)) || (!empty($filter_manufacture))){


				$result = $this->model_catalog_product->getActiveFilters($filter_filters, $category_id, $filter_manufacture, $filter_stock);
			}
			else $result[] = 'false';
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($result));
			return;
		}
		// $begin_time = microtime(true);
		// print_r($this->model_catalog_product->getActiveFilters($filter_filters, $category_id, $filter_manufacture, $filter_stock));
		// echo "time = ".(microtime(true) - $begin_time);
		//###################################

		if (isset($this->request->get['filter'])) {
		  $filter = $this->request->get['filter'];
		} else {
		  $filter = '';
		}
		
		/*******Min max price******/
		if (isset($this->request->get['min_price']) && isset($this->request->get['max_price'])) {
			
			if((int)$this->request->get['min_price'] > (int)$this->request->get['max_price']){
				 $min_price = (int)$this->request->get['max_price'];
			}else{
				 $min_price = (int)$this->request->get['min_price'];
			}
	   
		} else if(isset($this->request->get['min_price'])){
			
		  $min_price = (int)$this->request->get['min_price'];
		 
		}else{
			 $min_price = '';
		}
		
		
		if (isset($this->request->get['max_price'])) {
		  $max_price = (int)$this->request->get['max_price'];
		 
		} else {
		  $max_price = '';
		 
		}
		/***************************/

		$this->load->model('catalog/review');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		
		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}
		
		if (isset($this->request->get['manuf'])) {
			$filt_manuf = $this->request->get['manuf'];
		} else {
			$filt_manuf = '';
		}
		
		if (isset($this->request->get['stock'])) {
			$filt_stock = $this->request->get['stock'];
		} else {
			$filt_stock = '';
		}
// ############### SMART FILTER  END ##########################################################		

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'rating';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);
				}
			}
		} else {
			$category_id = 0;
		}

		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {

			$data['heading_title'] = $category_info['name'];
//#################### SMART FILTER BEGIN ##########################################
			  $temp_category_info = $category_info['name'];
			  $this->load->model('catalog/smartgroup');
			  $smart_seo = $this->model_catalog_smartgroup->getbyfilter($filter,$category_id);
			  $data['check_gr'] = false;
			  if (!empty($smart_seo)) {
				$category_info['meta_title'] = $smart_seo['seo_title'];
				$category_info['name'] = $smart_seo['seo_title'];
				$category_info['description'] = $smart_seo['seo_desc'];
				$data['heading_title']        = $smart_seo['seo_title'];
				$data['check_gr'] = true;
			  }
//#################### SMART FILTER END ##########################################

			$this->document->setTitle($category_info['meta_title']);
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path']), 'canonical');


			$data['text_refine'] = $this->language->get('text_refine');
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');


//#################### SMART FILTER BEGIN ##########################################
			$data['breadcrumbs'][] = array(
				'text' => $temp_category_info,
				'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
			);

			  $filter_breadcrumbs = $this->model_catalog_smartgroup->getbreadcrumbs($filter);

			  if (!empty($filter_breadcrumbs)) {
				foreach ($filter_breadcrumbs as $fbc) {
				  $data['breadcrumbs'][] = $fbc;
				}
			  }
//#################### SMART FILTER END ##########################################

			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$data['compare'] = $this->url->link('product/compare');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['categories'] = array();

			$results = $this->model_catalog_category->getCategories($category_id);

			foreach ($results as $result) {
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);

				if($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_subcategory_width'), $this->config->get('config_image_subcategory_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_subcategory_width'), $this->config->get('config_image_subcategory_height'));
				}

				$data['categories'][] = array(
					'name'  => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'thumb' => $image,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
				);
			}

			$data['products'] = array();

			/* DELETE SMART FILTER
			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			); */

// ############### SMART FILTER  BEGIN ##########################################################

			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter,
				'min_price'		   => $min_price,
				'max_price'		   => $max_price,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit,
				'filt_manuf'		 => $filt_manuf,
				'filt_stock'         => $filt_stock
			); 

			if($page == "all") {
				$filter_data = array(
						'filter_category_id' => $category_id,
						'filter_filter'      => $filter,
						'sort'               => $sort,
						'order'              => $order,
						'filt_manuf'		 => $filt_manuf,
						'filt_stock'         => $filt_stock,
						'min_price'		   => $min_price,
						'max_price'		   => $max_price,
				);
			}
// ############### SMART FILTER  END ##########################################################

			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

// ############### SMART FILTER  BEGIN ##########################################################
$data['tot_prod'] = $product_total;
// ############### SMART FILTER  END ##########################################################


			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'reviews'	  => $result['reviews'],
					'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				);
			}


			/* DELETE SMART FILTER
			 $url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			} */
// ############### SMART FILTER  BEGIN ##########################################################

        	//#####################################
				$this->load->model('module/smartfilter');
        		$price_limits = $this->model_module_smartfilter->getPriceLimits($category_id);
        		$price_limits['min_price'] = (int)$price_limits['min_price'];
        		$price_limits['max_price'] = (int)$price_limits['max_price'];
        	//#####################################


			

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
			
			if (isset($this->request->get['min_price']) && ($this->request->get['min_price'] != $price_limits['min_price'])) {
				$url .= '&min_price=' . $this->request->get['min_price'];
			}
			
			if (isset($this->request->get['max_price']) && ($this->request->get['max_price'] != $price_limits['max_price'])) {
				$url .= '&max_price=' . $this->request->get['max_price'];
			}
			
			if (isset($this->request->get['manuf'])) {
				$url .= '&manuf=' . $this->request->get['manuf'];
			}
			
			if (isset($this->request->get['stock_status_id'])) {
				$url .= '&stock_status_id=' . $this->request->get['stock_status_id'];
			}

// ############### SMART FILTER  END ##########################################################

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				);

				/*$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);*/
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_latest'),
				'value' => 'p.date_added-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.date_added&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			);

			/*$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);*/

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/category.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/category.tpl', $data));
			}
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			//$data['column_right'] = $this->load->controller('common/column_right');
			$data['column_right'] = '';
			$data['content_top'] = $this->load->controller('common/content_top');
			// $data['content_bottom'] = $this->load->controller('common/content_bottom');

			// modules in bottom for 404 page (cosmos)
			$data['modules'] = array();

			if ($this->model_design_layout->getLayoutModules(17, 'content_bottom')) {

				$modules = $this->model_design_layout->getLayoutModules(17, 'content_bottom');

				foreach ($modules as $module) {
					$part = explode('.', $module['code']);

					if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
						$data['modules'][] = $this->load->controller('module/' . $part[0]);
					}

					if (isset($part[1])) {
						$setting_info = $this->model_extension_module->getModule($part[1]);

						if ($setting_info && $setting_info['status']) {
							$data['modules'][] = $this->load->controller('module/' . $part[0], $setting_info);
						}
					}
				}
				
				$data['content_bottom'] = $this->load->view($this->config->get('config_template') . '/template/common/content_bottom.tpl', $data);
			} else {
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
			}
			// and modules in bottom

			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
	public function getLastProduct(){
		$this->load->language('product/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/review');
		$this->load->model('tool/image');

		if (isset($this->request->post['limit']))
		{
			$limit = $this->request->post['limit'];
			$category_id = $this->request->post['category'];
			$json = array();

			$lastAdd = $this->model_catalog_category->getLastByProduct($limit,$category_id);


			$json['sell'] = array();
			foreach($lastAdd as $descript)
			{

				$for_prices = $this->model_catalog_category->getLastByProductSpecial($descript['product_id']);

				foreach($for_prices as $for_price)
				{
					$json['price']   = $for_price['price'];
					$json['special'] = $for_price['special'];
					$json['image']   = $for_price['image'];
					$json['tax']     = $for_price['tax_class_id'];
				}

				if($json['special']){
					$json['special'] = $this->currency->format($this->tax->calculate($json['special'], $json['tax'], $this->config->get('config_tax')));
				} else {
					$json['special'] = false;
				}

				$json['sell'][] = array(
					'date_added' 		=> $descript['date_added'],
					'name'       		=> $descript['name'],
					'order_id'   		=> $descript['order_id'],
					'product_id' 		=> $descript['product_id'],
					'category_id'		=> $descript['category_id'],
					'total_review'		=> (int)$this->model_catalog_review->getTotalReviewsByProductId($descript['product_id']),
					'review' 			=> $this->model_catalog_review->getReviewsByProductId($descript['product_id']),
					'price'             => $this->currency->format($this->tax->calculate($json['price'], $json['tax'], $this->config->get('config_tax'))),
					'special'           => $json['special'],
					'image'             => $this->model_tool_image->resize($json['image'], 200, 200),
				);


			}

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	}
}
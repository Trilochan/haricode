<?php

class ControllerModuleManufacturers extends Controller {
  public function index() {
    static $module = 0;

    $this->load->language('module/manufacturers');

    $data['heading_title'] = $this->language->get('heading_title');

    $this->load->model('catalog/manufacturer');

    $data['manufacturers'] = array();

    $manufacturers = $this->model_catalog_manufacturer->getManufacturers();

    //var_dump($manufacturers);

    $this->load->model('tool/image');

    foreach ($manufacturers as $manufacturer) {
      if ($manufacturer['image']) {
        $image = $this->model_tool_image->resize($manufacturer['image'], $this->config->get('manufacturers_width'), $this->config->get('manufacturers_height'));
      } else {
        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('manufacturers_width'), $this->config->get('manufacturers_height'));
      }

      if($image == NULL) {
        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('manufacturers_width'), $this->config->get('manufacturers_height'));
      }

      $data['manufacturers'][] = array(
          'manufacturers_id' => $manufacturer['manufacturer_id'],
          'name'             => $manufacturer['name'],
          'thumb'            => $image,
          'href'             => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'])
      );
    }

    $data['module'] = $module++;

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturers.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/module/manufacturers.tpl', $data);
    } else {
      return $this->load->view('default/template/module/manufacturers.tpl', $data);
    }
  }
}
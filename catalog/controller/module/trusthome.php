<?php
class ControllerModuleTrustHome extends Controller {
	public function index() {
		$this->load->model('module/trusthome');
		$this->load->model('tool/image');
		//$this->load->model('setting/setting');
		//$this->load->language('module/trusthome');

		//$data['read_more'] = $this->language->get('read_more');

		$results = $this->model_module_trusthome->getTrasts();

		$data['trusts'] = $results;

		if ($results) {
			$lang_id = (int)$this->config->get('config_language_id');

			$data['trusts'] = array();

			foreach ($results as $result) {
				if ((int)$result['language_id'] == $lang_id) {

					if ($result['image']) {
						$result['image'] = $this->model_tool_image->resize($result['image'], 100, 100);
					} else {
						$result['image'] = $this->model_tool_image->resize('no_image.png', 100, 100);
					}

				$data['trusts'][] = $result;
				}
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/trusthome.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/trusthome.tpl', $data);
			} else {
				return $this->load->view('default/template/module/trusthome.tpl', $data);
			}
		}

	}
}
<?php
class ControllerModuleTrust extends Controller {
	public function index() {
		$this->load->model('module/trust');

		$results = $this->model_module_trust->getTrasts();

		if ($results) {

			$lang_id = (int)$this->config->get('config_language_id');

			$data['trusts'] = array();

			$this->load->model('catalog/information');

			foreach ($results as $result) {
				if ((int)$result['language_id'] == $lang_id) {
					$information_info = $this->model_catalog_information->getInformation((int)$result['related_article']);

					if ($information_info) {
						$result['article_href'] = $this->url->link('information/information', 'information_id=' . (int)$result['related_article']);
					}

					
					$data['trusts'][] = $result;
				}
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/trust.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/trust.tpl', $data);
			} else {
				return $this->load->view('default/template/module/trust.tpl', $data);
			}
		}

	}

	public function article() {
		$this->load->language('module/trust');

		$this->load->model('catalog/information');

		if (isset($this->request->get['id'])) {
			$information_id = (int)$this->request->get['id'];
		} else {
			$information_id = 0;
		}

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {

			$data['button_close'] = $this->language->get('button_close');
			$data['title'] = $information_info['title'];
			$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/trust_article.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/trust_article.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/module/trust_article.tpl', $data));
			}
		}
	}

}
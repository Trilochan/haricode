<?php

class ControllerModuleSimilarProducts extends Controller {

  public function index($setting) {
    $this->load->language('module/similar_products');

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_tax'] = $this->language->get('text_tax');

    $data['button_cart']     = $this->language->get('button_cart');
    $data['button_wishlist'] = $this->language->get('button_wishlist');
    $data['button_compare']  = $this->language->get('button_compare');

    $this->load->model('catalog/product');

    $this->load->model('tool/image');

    $data['products'] = array();

    if (!$setting['limit']) {
      $setting['limit'] = 4;
    }
	
	if (isset($this->request->get['path'])) {
		$parts = explode('_', (string)$this->request->get['path']);
			
		$category_id = array_pop($parts);
			
	} else {
		$parts = array();
		$category_id = 0;
	}
	
	$data_products = array(
		'filter_category_id' => $category_id,
		'limit' => $setting['limit'],
		'rand' => 'true',
		'start' => 0
	);
    
      $products = $this->model_catalog_product->getProducts($data_products);
	
      foreach ($products as $product_info) {
      
        if ($product_info) {
          if ($product_info['image']) {
            $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
          } else {
            $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
          }

          if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
            $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
          } else {
            $price = false;
          }

          if ((float)$product_info['special']) {
            $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
          } else {
            $special = false;
          }

          if ($this->config->get('config_tax')) {
            $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
          } else {
            $tax = false;
          }

          if ($this->config->get('config_review_status')) {
            $rating = $product_info['rating'];
          } else {
            $rating = false;
          }

          $data['stickers'] = false;
          $sticker_new      = false;
          $sticker_special  = false;
          $sticker_rating   = false;
          $sticker_vip      = false;
          if ($this->config->get('stickers_status')) {
            $data['stickers'] = true;
            if ($this->config->get('stickers_new_status')) {
              $date_added = strtotime($product_info['date_added']);
              $date_now   = strtotime(date("Y-m-d H:i:s"));
              $days       = (int)date('d', $date_now - $date_added);

              if ($days < (int)$this->config->get('stickers_new_days')) {
                $sticker_new = array(
                    'status'  => true,
                    'z_index' => $this->config->get('stickers_new_sort_order'),
                );
              } else {
                $sticker_new = false;
              }
            }

            if ($this->config->get('stickers_special_status')) {
              if ($special) {
                $sticker_special = array(
                    'status'  => true,
                    'z_index' => $this->config->get('stickers_special_sort_order'),
                );
                true;
              } else {
                $sticker_special = false;
              }
            }

            if ($this->config->get('stickers_rating_status')) {
              switch ($this->config->get('stickers_rating_criteria')) {
                case 'view':
                  if ($product_info['viewed'] >= (int)$this->config->get('stickers_rating_value')) {
                    $sticker_rating = array(
                        'status'  => true,
                        'z_index' => $this->config->get('stickers_rating_sort_order'),
                    );
                  } else {
                    $sticker_rating = false;
                  }
                  break;
                case 'rating':
                  if ($product_info['reviews'] >= (int)$this->config->get('stickers_rating_value')) {
                    $sticker_rating = array(
                        'status'  => true,
                        'z_index' => $this->config->get('stickers_rating_sort_order'),
                    );
                  } else {
                    $sticker_rating = false;
                  }
                  break;
                case 'purchase':
                  if ((int)$this->model_catalog_product->getNumberOfSales($product_info['product_id']) >= (int)$this->config->get('stickers_rating_value')) {
                    $sticker_rating = array(
                        'status'  => true,
                        'z_index' => $this->config->get('stickers_rating_sort_order'),
                    );
                  } else {
                    $sticker_rating = false;
                  }
                  break;
              }
            }

            if ($this->config->get('stickers_vip_status')) {
              if (in_array($product_info['product_id'], $this->config->get('stickers_vip_products'))) {
                $sticker_vip = array(
                    'status'  => true,
                    'z_index' => $this->config->get('stickers_vip_sort_order'),
                );
              } else {
                $sticker_vip = false;
              }
            }
          }

          if ($this->config->get('config_review_status')) {
            $rating = $product_info['rating'];
          } else {
            $rating = false;
          }

          $data['products'][] = array(
              'product_id'      => $product_info['product_id'],
              'thumb'           => $image,
              'name'            => $product_info['name'],
              'description'     => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
              'price'           => $price,
              'special'         => $special,
              'tax'             => $tax,
              'reviews'         => $product_info['reviews'],
              'reviews_count'   => sprintf($this->language->get('reviews_count'), $rating),
              'rating'          => $rating,
              'href'            => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
              'sticker_rating'  => $sticker_rating,
              'sticker_special' => $sticker_special,
              'sticker_new'     => $sticker_new,
              'sticker_vip'     => $sticker_vip,
          );
        }
      }
    
    @$data['route_slider'] = $this->request->get['route'];
    if ($data['products']) {
      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/product_slider.tpl')) {
        return $this->load->view($this->config->get('config_template') . '/template/module/product_slider.tpl', $data);
      } else {
        return $this->load->view('default/template/module/product_slider.tpl', $data);
      }
    }
  }
}
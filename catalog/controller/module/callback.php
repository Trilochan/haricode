<?php

class ControllerModuleCallback extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('common/footer');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $name = $this->request->post['name'];
            $phone = $this->request->post['telephone'];
            $lecationhref = $this->request->post['lecationhref'];

            $HTTP_HOST = parse_url ("http://".$_SERVER["HTTP_HOST"]);
            $HTTP_HOST = str_replace (array ("http://","www."), "", $HTTP_HOST["host"]);
            $from = "noreply@".$HTTP_HOST;

            $subject = sprintf($this->language->get('text_callback_email_subject'), $this->config->get('config_name'));

            $html = "<b>" . $this->language->get('text_callback_email_name') . ": </b>$name<br /><b>" . $this->language->get('text_callback_email_phone') . ": </b>$phone<br><b>" . $this->language->get('text_callback_referer') . "</b>: <a href='$lecationhref'>$lecationhref</a>";
            $text = "Имя: $name\n\nТелефон: $phone";

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($this->config->get('config_email'));
            $mail->setFrom($from);
            $mail->setSender($this->config->get('config_name'));
            $mail->setSubject($subject);
            $mail->setHtml($html);
            $mail->setText($text);
            $mail->send();


            echo json_encode(array('status' => true));
        }
        else {
            echo json_encode(array('status' => false, 'msg' => $this->error));
        }
    }

    private function validate()
    {
        if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 32)) {
            $this->error['name'] = $this->language->get('callback_error_name');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['phone'] = $this->language->get('callback_error_phone');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}

?>

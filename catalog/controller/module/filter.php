<?php
class ControllerModuleFilter extends Controller {
	public function index() {
		
		$this->document->addScript('catalog/view/javascript/jquery/slider/nouislider.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/slider/wNumb.js');
		$this->document->addStyle('catalog/view/javascript/jquery/slider/nouislider.min.css');
		
		$this->load->model('catalog/category');
		$this->load->model('module/smartfilter');
		$this->load->model('catalog/manufacturer');
		$this->load->language('module/filter');
		
		$data['link_for_cansel'] = $this->url->link('product/category', (isset($this->request->get['path']))? 'path=' . $this->request->get['path'] : ""); // Ссылка на текущую категорию для сброса фильтра
		
		
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}
		
		
		$gets = $this->request->get;
		$text_stock = $this->language->get('text_stock');
		$text_manufacturers = $this->language->get('text_manufacturers');
		
		$all_values = array();
		
		foreach($gets as $key => $get){
			
			if($key == 'filter'){
				
				$filter = explode(",",$get);
				foreach($filter as $filt){
				
					$filter_info = $this->model_module_smartfilter->getGroupFilterById($filt);
					$all_values[$filter_info['filter_group_name']][] = array( 
						
						'name' => $filter_info['filter_name'],
						'value' => $filt,
						'filter_type' => $key
					);
				}
			}
			
			if($key == 'stock'){
				$stock_rers = explode(",",$get);
				foreach($stock_rers as $stock){
					
					$status_info = $this->model_module_smartfilter->getStockStatusName($stock);
					
					$all_values[$text_stock][] = array(
						'name' => $status_info['name'],
						'value' => $stock,
						'filter_type' => $key
					);
				}
			}
			
			
			if($key == 'manuf'){
				$man_rers = explode(",",$get);
				foreach($man_rers as $man){
					
					$manuf_info = $this->model_catalog_manufacturer->getManufacturer($man);
					
					$all_values[$text_manufacturers][] = array(
						'name' => $manuf_info['name'],
						'value' => $manuf_info['manufacturer_id'],
						'filter_type' => $key	
					);
				}
			}
		}
		
		$data['all_values'] = $all_values;

		$category_id = end($parts);

		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info||$category_id==0) {
			
			$data['heading_title'] = $this->language->get('heading_title');

			$data['button_filter'] = $this->language->get('button_filter');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
				$this->load->model('catalog/product');
				$filter_data = array('filter_category_id' => $category_id);
				$product_total = $this->model_catalog_product->getTotalProducts($filter_data);
				if($product_total > 1){
					/****Min max price****/

					$price_limits = $this->model_module_smartfilter->getPriceLimits($category_id);

					$data['price_limits'] = $price_limits;

					if(isset($this->request->get['min_price']) && !empty($this->request->get['min_price'])){
						
						$get_min_price = (int)$this->request->get['min_price'];
						
					}else{
						
						$get_min_price = 0;
					}
					
					if(isset($this->request->get['max_price']) && !empty($this->request->get['max_price'])){
						
						$get_max_price = (int)$this->request->get['max_price'];
						
					}else{
						
						$get_max_price = 0;
					}
					
					if($get_min_price && $get_max_price){
						
						if($get_min_price > $get_max_price){
							
							$data['min_price'] = $get_max_price;
							
						}else{
							$data['min_price'] = $get_min_price;
						}
						
					}else if($get_min_price){
						$data['min_price'] = $get_min_price;
						
					}else{
						$data['min_price'] = (int)$price_limits['min_price'];
					}
					
					if($get_max_price && $get_max_price <= (int)$price_limits['max_price']){
						
						$data['max_price'] = $get_max_price;
						
					}else{
						$data['max_price'] = (int)$price_limits['max_price'];
					}
						
					/****Stock_statuses****/
					if (isset($this->request->get['stock'])) {
						
						$data['stock_id'] = $this->request->get['stock'];
					}else{
						$data['stock_id'] = '';
					}
					
					$statuses = $this->model_module_smartfilter->getStockStatuses($category_id);

					$def_stat_id = $this->config->get('filter_stock_status');
					
					if($def_stat_id){
					
						$def_stock_stat = $def_stat_id;
					
					}else{
						
						$def_stock_stat = 7;
					}
					
					$count_def = $this->model_module_smartfilter->getTotalProdustsByStatus($def_stock_stat,$category_id); // Считаем кол-во товаров для статуса по умолчанию
					
					if($count_def > 0){
						$status_name = $this->model_module_smartfilter->getStatusNameById($def_stock_stat); // Получаем название статуса
						
						$data['stock_statuses'][] = array(
						
							'stock_status_id' => $this->config->get('filter_stock_status'),
							'name'            => $status_name
						);
					}
					
					if($statuses){
						
						foreach ($statuses as $status) {
							$count = $this->model_module_smartfilter->getTotalProdustsByStatus($status['stock_status_id'],$category_id);
							
							if($count > 0){
								
								$data['stock_statuses'][] = array(
									'stock_status_id' => $status['stock_status_id'],
									'name'            => $status['name']
								);
							}
						}
					}
					/****Manufacturers****/
					if (isset($this->request->get['manuf'])) {
						$data['filter_manufacturer'] = explode(',', $this->request->get['manuf']);
					} else {
						$data['filter_manufacturer'] = array();
					}
					
					$manufaturers = $this->model_module_smartfilter->getManufacturersByCategoryId($category_id);
					
					if($manufaturers){ 
					
						foreach ($manufaturers as $manufacturer) {
							$data['manufacturers'][] = array(
								'manufacturer_id' => $manufacturer['manufacturer_id'],
								'name'            => $manufacturer['name']
							);
						}
					}else{
						$data['manufacturers'] = '';
					}

					/****************/

					if($category_id==0) {
						
						$data['action'] = str_replace('&amp;', '&', $this->url->link('product/category',  $url));
						
					} else {

					$data['action'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url));
					}

					if (isset($this->request->get['filter'])) {
						$data['filter_category'] = explode(',', $this->request->get['filter']);
					} else {
						$data['filter_category'] = array();
					}

					$this->load->model('catalog/product');

					$data['filter_groups'] = array();

					//$filter_groups = $this->model_catalog_category->getAllCategoryFilters($category_id);
					$filter_groups = $this->model_catalog_category->getFiltersByCategoryId($category_id);
					
					if($filter_groups){
						foreach($filter_groups as $filter){
							$filter_arr[$filter['filter_group_name']][] = $filter;

						}
					
						foreach ($filter_arr as $key => $filter_group) {
							$childen_data = array();

							foreach ($filter_group as $filter) {
									$filter_data = array(
									'filter_category_id' => $category_id,
									'filter_filter'      => $filter['filter_id']
								);
								
								$childs=array();
								$childs = $this->model_catalog_category->getFiltersChildrens($filter['filter_id']);

								$childen_data[] = array(
									'filter_id' => $filter['filter_id'],
									//'name'      => $filter['name'],
									'name'      => $filter['filter_name'],
									
									'childs'=> $childs 
									//'name'      => $filter['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : '')
								);
							}

							$data['filter_groups'][] = array(
								///'filter_group_id' => $filter_group['filter_group_id'],
								//'name'            => $filter_group['name'],
								'name'            => $key,
								'filter'          => $childen_data
							);
						}
					}

					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/filter.tpl')) {
						return $this->load->view($this->config->get('config_template') . '/template/module/filter.tpl', $data);
					} else {
						return $this->load->view('default/template/module/filter.tpl', $data);
					}
			}
		}
	}
}
<?php
class ControllerErrorNotFound extends Controller {
	public function index() {
		$this->load->language('error/not_found');

		$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['route'])) {
			$url_data = $this->request->get;

			unset($url_data['_route_']);

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link($route, $url, $this->request->server['HTTPS'])
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_error'] = $this->language->get('text_error');

		$data['button_continue'] = $this->language->get('button_continue');

		$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		// $data['content_bottom'] = $this->load->controller('common/content_bottom');

		// modules in bottom for 404 page (cosmos)
		$data['modules'] = array();

		if ($this->model_design_layout->getLayoutModules(17, 'content_bottom')) {

			$modules = $this->model_design_layout->getLayoutModules(17, 'content_bottom');

			foreach ($modules as $module) {
				$part = explode('.', $module['code']);

				if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
					$data['modules'][] = $this->load->controller('module/' . $part[0]);
				}

				if (isset($part[1])) {
					$setting_info = $this->model_extension_module->getModule($part[1]);

					if ($setting_info && $setting_info['status']) {
						$data['modules'][] = $this->load->controller('module/' . $part[0], $setting_info);
					}
				}
			}
			
			$data['content_bottom'] = $this->load->view($this->config->get('config_template') . '/template/common/content_bottom.tpl', $data);
		} else {
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
		}
		// and modules in bottom

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
		}
	}
}
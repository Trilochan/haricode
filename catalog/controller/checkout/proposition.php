<?php

class ControllerCheckoutProposition extends Controller {

  public function add() {
    $this->load->language('checkout/cart');
    $this->load->model('catalog/product');
    $this->load->model('catalog/proposition');

    $json = array();

    if (!empty($this->request->post['products'])) {
      foreach ($this->request->post['products'] as $product) {
        if ($product['options']) {
          $options_GET = $product['options'];
          $options_GET = html_entity_decode($options_GET);
          $options_GET = urldecode($options_GET);

          parse_str($options_GET, $options);
          $option = $options['option'];
        } else {
          $option = array();
        }

        $product_info = $this->model_catalog_product->getProduct($product['product_id']);

        if ($product_info) {
          $product_options = $this->model_catalog_product->getProductOptions($product['product_id']);

          foreach ($product_options as $product_option) {
            if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
              $json['error']['option'][$product['product_id']][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
            }
          }
        }
      }

      if (!$json) {
        $proposition = array(
            'proposition_id' => $this->request->post['proposition_id'],
            'quantity'       => 1,
            'products'       => array(),
        );

        $total_price = 0;

        $products = array();
        
        foreach ($this->request->post['products'] as $product) {
          $product_info = $this->model_catalog_product->getProduct($product['product_id']);

          if ($product_info) {
            if ($product['options']) {
              $options_GET = $product['options'];
              $options_GET = html_entity_decode($options_GET);
              $options_GET = urldecode($options_GET);

              parse_str($options_GET, $options);
              $option = $options['option'];
            } else {
              $option = array();
            }

            $recurring_id = 0;
            $recurrings   = $this->model_catalog_product->getProfiles($product_info['product_id']);

            if ($recurrings) {
              $recurring_ids = array();

              foreach ($recurrings as $recurring) {
                $recurring_ids[] = $recurring['recurring_id'];
              }

              if (!in_array($recurring_id, $recurring_ids)) {
                $json['error']['recurring'] = $this->language->get('error_recurring_required');
              }
            }

            $products[] = array(
                'product_id'   => $product['product_id'],
                'option'       => $option,
                'recurring_id' => $recurring_id,
            );

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);

            // Totals
            $this->load->model('extension/extension');

            $total_data = array();
            $total      = 0;
            $taxes      = $this->cart->getTaxes();

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
              $sort_order = array();

              $results = $this->model_extension_extension->getExtensions('total');

              foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
              }

              array_multisort($sort_order, SORT_ASC, $results);

              foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                  $this->load->model('total/' . $result['code']);

                  $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                }
              }

              $sort_order = array();

              foreach ($total_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
              }

              array_multisort($sort_order, SORT_ASC, $total_data);
            }

            $total_price += $total;
          }
        }

        $proposition['products'] = $products;

        $this->cart->addProposition($proposition);

        $json['success'] = $this->language->get('text_proposition_success');

        //$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total_price));

        $json['count'] = ($this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0));
        $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total_price));
      } else {
        $json['error']['message'] = $this->language->get('error_proposition_options');
        $json['error']['info'] = $this->language->get('text_proposition_option');
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function edit() {
    $json = array();

    // Update
    if (!empty($this->request->post['quantity'])) {
      $key      = $this->request->post['key'];
      $quantity = $this->request->post['quantity'];

      $this->cart->updateProposition($key, $quantity);

      unset($this->session->data['shipping_method']);
      unset($this->session->data['shipping_methods']);
      unset($this->session->data['payment_method']);
      unset($this->session->data['payment_methods']);
      unset($this->session->data['reward']);

      $json['count']       = $this->cart->countProducts();
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function remove() {
    $this->load->language('checkout/cart');

    $json = array();

    // Remove
    if (isset($this->request->post['key'])) {
      $this->cart->removeProposition($this->request->post['key']);

      unset($this->session->data['vouchers'][$this->request->post['key']]);

      $this->session->data['success'] = $this->language->get('text_remove');

      unset($this->session->data['shipping_method']);
      unset($this->session->data['shipping_methods']);
      unset($this->session->data['payment_method']);
      unset($this->session->data['payment_methods']);
      unset($this->session->data['reward']);

      // Totals
      $this->load->model('extension/extension');

      $total_data = array();
      $total      = 0;
      $taxes      = $this->cart->getTaxes();

      // Display prices
      if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
        $sort_order = array();

        $results = $this->model_extension_extension->getExtensions('total');

        foreach ($results as $key => $value) {
          $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
          if ($this->config->get($result['code'] . '_status')) {
            $this->load->model('total/' . $result['code']);

            $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
          }
        }

        $sort_order = array();

        foreach ($total_data as $key => $value) {
          $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $total_data);
      }

      $json['count'] = ($this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0));
      $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}
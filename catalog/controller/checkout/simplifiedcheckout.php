<?php

class ControllerCheckoutSimplifiedCheckout extends Controller {
  public function index() {
    if (!isset($this->session->data['shipping_address']['zone_id']) || $this->session->data['shipping_address']['zone_id'] == 0) {
      $this->session->data['shipping_address']['zone_id'] = $this->config->get('config_zone_id');
    }
    if (!isset($this->session->data['shipping_address']['country_id']) || $this->session->data['shipping_address']['country_id'] == 0) {
      $this->session->data['shipping_address']['country_id'] = $this->config->get('config_country_id');
    }

    if (!isset($this->session->data['payment_address']['zone_id']) || $this->session->data['payment_address']['zone_id'] == 0) {
      $this->session->data['payment_address']['zone_id'] = $this->config->get('config_zone_id');
    }
    if (!isset($this->session->data['payment_address']['country_id']) || $this->session->data['payment_address']['country_id'] == 0) {
      $this->session->data['payment_address']['country_id'] = $this->config->get('config_country_id');
    }

    if (!isset($this->session->data['shipping_address']['custom_field'])) {
      $this->session->data['payment_address']['custom_field'] = array();
    }

    // Validate cart has products and has stock.
    if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
      $this->response->redirect($this->url->link('common/home', '', 'SSL'));
    }

    // Validate minimum quantity requirements.
    $products = $this->cart->getProducts();

    foreach ($products as $product) {
      $product_total = 0;

      foreach ($products as $product_2) {
        if ($product_2['product_id'] == $product['product_id']) {
          $product_total += $product_2['quantity'];
        }
      }

      if ($product['minimum'] > $product_total) {
        $this->response->redirect($this->server->get('HTTP_REFERER'));
      }
    }

    $this->load->language('checkout/simplifiedcheckout');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
    $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
    $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

    // Required by klarna
    if ($this->config->get('klarna_account') || $this->config->get('klarna_invoice')) {
      $this->document->addScript('http://cdn.klarna.com/public/kitt/toc/v1.0/js/klarna.terms.min.js');
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_home'),
        'href' => $this->url->link('common/home')
    );
    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('checkout/checkout', '', 'SSL')
    );

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_checkout_option']           = $this->language->get('text_checkout_option');
    $data['text_checkout_account']          = $this->language->get('text_checkout_account');
    $data['text_checkout_payment_address']  = $this->language->get('text_checkout_payment_address');
    $data['text_checkout_shipping_address'] = $this->language->get('text_checkout_shipping_address');
    $data['text_checkout_shipping_method']  = $this->language->get('text_checkout_shipping_method');
    $data['text_checkout_payment_method']   = $this->language->get('text_checkout_payment_method');
    $data['text_checkout_confirm']          = $this->language->get('text_checkout_confirm');

    if (isset($this->session->data['error'])) {
      $data['error_warning'] = $this->session->data['error'];
      unset($this->session->data['error']);
    } else {
      $data['error_warning'] = '';
    }

    $data['logged'] = $this->customer->isLogged();

    if (isset($this->session->data['account'])) {
      $data['account'] = $this->session->data['account'];
    } else {
      $data['account'] = '';
    }

    $data['shipping_required'] = $this->cart->hasShipping();

    if ($this->customer->isLogged()) {
      unset($this->session->data['guest']);

      $this->load->model('account/address');

      if ($this->config->get('config_tax_customer') == 'payment') {
        $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
      }

      if ($this->config->get('config_tax_customer') == 'shipping') {
        $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
      }

      if (isset($this->session->data['geo']) && !empty($this->session->data['geo'])) {
        $this->session->data['payment_address']['city']           = $this->session->data['geo']['city'];
        $this->session->data['payment_address']['country_id']     = $this->session->data['geo']['country_id'];
        $this->session->data['payment_address']['country']        = $this->session->data['geo']['country'];
        $this->session->data['payment_address']['iso_code_2']     = $this->session->data['geo']['iso_code_2'];
        $this->session->data['payment_address']['iso_code_3']     = $this->session->data['geo']['iso_code_3'];
        $this->session->data['payment_address']['address_format'] = $this->session->data['geo']['address_format'];
      }

      if (isset($this->session->data['geo']) && !empty($this->session->data['geo'])) {
        $this->session->data['shipping_address']['city']          = $this->session->data['geo']['city'];
        $this->session->data['shipping_address']['country_id']    = $this->session->data['geo']['country_id'];
        $this->session->data['payment_address']['country']        = $this->session->data['geo']['country'];
        $this->session->data['payment_address']['iso_code_2']     = $this->session->data['geo']['iso_code_2'];
        $this->session->data['payment_address']['iso_code_3']     = $this->session->data['geo']['iso_code_3'];
        $this->session->data['payment_address']['address_format'] = $this->session->data['geo']['address_format'];
      }
    } else {
      $customer_group_id = $this->config->get('config_customer_group_id');

      $this->session->data['account'] = 'guest';

      $this->session->data['guest']['customer_group_id'] = $customer_group_id;
      $this->session->data['guest']['firstname']         = "";
      $this->session->data['guest']['lastname']          = "";
      $this->session->data['guest']['email']             = "";
      $this->session->data['guest']['telephone']         = "";
      $this->session->data['guest']['fax']               = "";

      $this->session->data['guest']['custom_field'] = array();

      $this->session->data['payment_address']['firstname'] = "";
      $this->session->data['payment_address']['lastname']  = "";
      $this->session->data['payment_address']['company']   = "";
      $this->session->data['payment_address']['address_1'] = "";
      $this->session->data['payment_address']['address_2'] = "";
      $this->session->data['payment_address']['postcode']  = "";
      $this->session->data['payment_address']['city']      = "";

      if (isset($this->session->data['geo']['city']) && $this->session->data['geo']['city'] != "") {
        $this->session->data['payment_address']['city'] = $this->session->data['geo']['city'];
      }

      $this->session->data['payment_address']['country_id'] = "";

      if (isset($this->session->data['geo']['country_id']) && $this->session->data['geo']['country_id'] != "") {
        $this->session->data['payment_address']['country_id'] = $this->session->data['geo']['country_id'];
      }

      $this->session->data['payment_address']['zone_id'] = "";

      $this->load->model('localisation/country');

      $country_info = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));

      if ($country_info) {
        $this->session->data['payment_address']['country']        = $country_info['name'];
        $this->session->data['payment_address']['iso_code_2']     = $country_info['iso_code_2'];
        $this->session->data['payment_address']['iso_code_3']     = $country_info['iso_code_3'];
        $this->session->data['payment_address']['address_format'] = $country_info['address_format'];
      } else {
        $this->session->data['payment_address']['country']        = '';
        $this->session->data['payment_address']['iso_code_2']     = '';
        $this->session->data['payment_address']['iso_code_3']     = '';
        $this->session->data['payment_address']['address_format'] = '';
      }

      if (isset($this->session->data['geo']) && !empty($this->session->data['geo'])) {
        $this->session->data['payment_address']['country']        = $this->session->data['geo']['country'];
        $this->session->data['payment_address']['iso_code_2']     = $this->session->data['geo']['iso_code_2'];
        $this->session->data['payment_address']['iso_code_3']     = $this->session->data['geo']['iso_code_3'];
        $this->session->data['payment_address']['address_format'] = $this->session->data['geo']['address_format'];
      }

      $this->session->data['payment_address']['custom_field'] = array();

      $this->load->model('localisation/zone');

      $zone_info = $this->model_localisation_zone->getZone($this->config->get('config_zone_id'));

      if ($zone_info) {
        $this->session->data['payment_address']['zone']      = $zone_info['name'];
        $this->session->data['payment_address']['zone_code'] = $zone_info['code'];
      } else {
        $this->session->data['payment_address']['zone']      = '';
        $this->session->data['payment_address']['zone_code'] = '';
      }

      $this->session->data['guest']['shipping_address'] = true;

      // Default Payment Address
      if (!$this->session->data['guest']['shipping_address']) {
        $this->session->data['shipping_address']['firstname'] = "";
        $this->session->data['shipping_address']['lastname']  = "";
        $this->session->data['shipping_address']['company']   = "";
        $this->session->data['shipping_address']['address_1'] = "";
        $this->session->data['shipping_address']['address_2'] = "";
        $this->session->data['shipping_address']['postcode']  = "";
        $this->session->data['shipping_address']['city']      = "";
        if (isset($this->session->data['geo']['city']) && $this->session->data['geo']['city'] != "") {
          $this->session->data['shipping_address']['city'] = $this->session->data['geo']['city'];
        }
        $this->session->data['shipping_address']['country_id'] = "";
        if (isset($this->session->data['geo']['country_id']) && $this->session->data['geo']['country_id'] != "") {
          $this->session->data['shipping_address']['country_id'] = $this->session->data['geo']['country_id'];
        }
        $this->session->data['shipping_address']['zone_id'] = "";

        if ($country_info) {
          $this->session->data['shipping_address']['country']        = $country_info['name'];
          $this->session->data['shipping_address']['iso_code_2']     = $country_info['iso_code_2'];
          $this->session->data['shipping_address']['iso_code_3']     = $country_info['iso_code_3'];
          $this->session->data['shipping_address']['address_format'] = $country_info['address_format'];
        } else {
          $this->session->data['shipping_address']['country']        = '';
          $this->session->data['shipping_address']['iso_code_2']     = '';
          $this->session->data['shipping_address']['iso_code_3']     = '';
          $this->session->data['shipping_address']['address_format'] = '';
        }

        if (isset($this->session->data['geo']) && !empty($this->session->data['geo'])) {
          $this->session->data['shipping_address']['country']        = $this->session->data['geo']['country'];
          $this->session->data['shipping_address']['iso_code_2']     = $this->session->data['geo']['iso_code_2'];
          $this->session->data['shipping_address']['iso_code_3']     = $this->session->data['geo']['iso_code_3'];
          $this->session->data['shipping_address']['address_format'] = $this->session->data['geo']['address_format'];
        }

        if ($zone_info) {
          $this->session->data['shipping_address']['zone']      = $zone_info['name'];
          $this->session->data['shipping_address']['zone_code'] = $zone_info['code'];
        } else {
          $this->session->data['shipping_address']['zone']      = '';
          $this->session->data['shipping_address']['zone_code'] = '';
        }

        $this->session->data['shipping_address']['custom_field'] = array();
      }
    }

    $data['column_left']    = $this->load->controller('common/column_left');
    $data['column_right']   = $this->load->controller('common/column_right');
    $data['content_top']    = $this->load->controller('common/content_top');
    $data['content_bottom'] = $this->load->controller('common/content_bottom');
    $data['footer']         = $this->load->controller('common/footer');
    $data['header']         = $this->load->controller('common/header');

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/simplifiedcheckout.tpl')) {
      $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/simplifiedcheckout.tpl', $data));
    } else {
      $this->response->setOutput($this->load->view('default/template/checkout/simplifiedcheckout.tpl', $data));
    }
  }

  public function country() {
    $json = array();

    $this->load->model('localisation/country');

    $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

    if ($country_info) {
      $this->load->model('localisation/zone');

      $json = array(
          'country_id'        => $country_info['country_id'],
          'name'              => $country_info['name'],
          'iso_code_2'        => $country_info['iso_code_2'],
          'iso_code_3'        => $country_info['iso_code_3'],
          'address_format'    => $country_info['address_format'],
          'postcode_required' => $country_info['postcode_required'],
          'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
          'status'            => $country_info['status']
      );
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function customfield() {
    $json = array();

    $this->load->model('account/custom_field');

    // Customer Group
    if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
      $customer_group_id = $this->request->get['customer_group_id'];
    } else {
      $customer_group_id = $this->config->get('config_customer_group_id');
    }

    $custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

    foreach ($custom_fields as $custom_field) {
      $json[] = array(
          'custom_field_id' => $custom_field['custom_field_id'],
          'required'        => $custom_field['required']
      );
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function getNovaPoshtaCities() {
    $cache    = md5(http_build_query($this->request->get));
    $response = $this->cache->get('novaposhta_city.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache);
    if (!$response) {
      $post_data = array(
          "apiKey"       => $this->config->get('novaposhta_api_key'),
          "modelName"    => "Address",
          "calledMethod" => "getCities",
      );

      $post = json_encode($post_data);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://api.novaposhta.ua/v2.0/json/');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $response = curl_exec($ch);
      curl_close($ch);
      $this->cache->set('novaposhta_city.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache, $response);
    }
    echo $response;
  }

  public function getNovaPoshtaWarehouses() {
    $cache    = md5(http_build_query($this->request->get));
    $response = $this->cache->get('novaposhta_warehouse.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache);
    if (!$response) {
      $post_data = array(
          "apiKey"           => $this->config->get('novaposhta_api_key'),
          "modelName"        => "Address",
          "calledMethod"     => "getWarehouses",
          "methodProperties" => array(
              "CityRef" => $this->request->get['city_ref']
          )
      );

      $post = json_encode($post_data);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://api.novaposhta.ua/v2.0/json/');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $response = curl_exec($ch);
      curl_close($ch);
      $this->cache->set('novaposhta_warehouse.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache, $response);
    }
    echo $response;
  }
}

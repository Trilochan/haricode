<?php

class ControllerPaymentcashondelivery extends Controller {

  public function index() {
    $this->load->language('payment/cash_on_delivery');

    $data['text_instruction'] = $this->language->get('text_instruction');
    $data['text_loading']     = $this->language->get('text_loading');

    $data['button_confirm'] = $this->language->get('button_confirm');

    $data['bank'] = nl2br($this->config->get('cash_on_delivery_bank' . $this->config->get('config_language_id')));

    $data['continue'] = $this->url->link('checkout/success');

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/cash_on_delivery.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/payment/cash_on_delivery.tpl', $data);
    } else {
      return $this->load->view('default/template/payment/cash_on_delivery.tpl', $data);
    }
  }

  public function confirm() {
    if ($this->session->data['payment_method']['code'] == 'cash_on_delivery') {
      $this->load->language('payment/cash_on_delivery');

      $this->load->model('checkout/order');

      $comment = $this->language->get('text_instruction') . "\n\n";
      $comment .= $this->config->get('cash_on_delivery_bank' . $this->config->get('config_language_id')) . "\n\n";

      $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('cash_on_delivery_order_status_id'), $comment, true);
    }
  }
}
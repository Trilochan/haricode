<?php

class ControllerAccountAjaxRegister extends Controller {
    private $error = array();

    public function index() {
        if ($this->customer->isLogged()) {
            $this->response->redirect($this->url->link('account/account', '', 'SSL'));
        }

        $this->load->language('account/register');

        $this->load->model('account/customer');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $data = array(
                'firstname'  => '',
                'lastname'   => '',
                'email'      => $this->request->post['register_email'],
                'telephone'  => '',
                'fax'        => '',
                'password'   => $this->request->post['register_password'],
                'company'    => '',
                'address_1'  => '',
                'address_2'  => '',
                'city'       => '',
                'postcode'   => '',
                'country_id' => '',
                'zone_id'    => '',
                'key'        => $this->generateKey()
            );

            $this->model_account_customer->addCustomer($data);

            // Clear any previous login attempts for unregistered accounts.
            $this->model_account_customer->deleteLoginAttempts($this->request->post['register_email']);

            $this->customer->login($this->request->post['register_email'], $this->request->post['register_password']);

            unset($this->session->data['guest']);

            // Add to activity log
            $this->load->model('account/activity');

            $activity_data = array(
                'customer_id' => $this->customer->getId(),
                'name'        => ''
            );

            $this->model_account_activity->addActivity('register', $activity_data);

            echo json_encode(array('status' => true));
        } else {
            echo json_encode(array(
                'status' => false,
                'msg'    => $this->error
            ));
        }
    }

    public function validate() {

        if ((utf8_strlen($this->request->post['register_email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['register_email'])) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['register_email'])) {
            $this->error['warning'] = $this->language->get('error_exists');
        }


        if ((utf8_strlen($this->request->post['register_password']) < 4) || (utf8_strlen($this->request->post['register_password']) > 20)) {
            $this->error['password'] = $this->language->get('error_password');
        }

//        if ($this->request->post['register_password'] != $this->request->post['register_password2']) {
//            $this->error['compire'] = '������ � ������ � ������ ���� �� ���������';
//        }

        return !$this->error;
    }

    private function generateKey() {
        $length   = rand(5, 20);
        $chars    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $numChars = strlen($chars);
        $string   = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }

        return md5($string);
    }

    public function confirm() {
        $this->load->language('account/register');

        $this->load->model('account/customer');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate_confirm()) {
            $data = array(
                'firstname'  => $this->request->post['firstname'],
                'telephone'  => $this->request->post['telephone']
            );

            $this->model_account_customer->confirm($data);

            echo json_encode(array('status' => true));
        } else {
            echo json_encode(array(
                'status' => false,
                'msg'    => $this->error
            ));
        }
    }

    public function validate_confirm() {

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        if(!$this->model_account_customer->getConfirm($this->request->post['email'], $this->request->post['key'])) {
            $this->error['confirm'] = $this->language->get('error_confirm');
        }

        return !$this->error;
    }
}

?>

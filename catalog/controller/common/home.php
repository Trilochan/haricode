<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->url->link('common/home'), 'canonical');
		}

		if (isset($this->request->get['account']) && $this->request->get['account'] == 'login') {

			if ($this->customer->isLogged()) {
				$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

				$this->response->redirect($this->url->link('account/account', '', 'SSL'));
			}

			$data['show_login_form'] = true;
		} else {
			$data['show_login_form'] = false;
		}

		$you_tube = $this->config->get('config_youtube');
		$you_tube = explode("/", $you_tube);
		$data['you_tube_id'] = array_pop($you_tube);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}
}
<?php

class ControllerCommonHeader extends Controller {
  public function index() {
    $this->document->addStyle('catalog/view/javascript/jquery/slick/slick.css');
    $this->document->addStyle('catalog/view/javascript/jquery/slick/slick-theme.css');
    $this->document->addScript('catalog/view/javascript/jquery/slick/slick.min.js');

    $this->document->addStyle('catalog/view/javascript/jquery/smoke/css/smoke.min.css');
    $this->document->addScript('catalog/view/javascript/jquery/smoke/js/smoke.min.js');
    $this->document->addScript('catalog/view/javascript/jquery/smoke/lang/ru.js');

    // $this->document->addStyle('catalog/view/javascript/jquery/ionrange/css/ion.rangeSlider.css');
    // $this->document->addStyle('catalog/view/javascript/jquery/ionrange/css/ion.rangeSlider.skinNice.css');
    // $this->document->addStyle('catalog/view/javascript/jquery/ionrange/css/normalize.css');

    // $this->document->addStyle('catalog/view/javascript/jquery/ionrange/js/ion-rangeSlider/ion.rangeSlider.min.js');

    $this->document->addScript('catalog/view/javascript/jquery/jquery.maskedinput.min.js');

    $data['title'] = $this->document->getTitle();

    $data['ulogin_form_marker'] = $this->load->controller('module/ulogin');

    $url = $this->url->link('module/ulogin/login', '', 'SSL');
    $route = isset($this->request->get['route']) ? $this->request->get['route'] : '';
    $data['redirect_uri'] = urlencode($url.  '&backurl=' . $route);
    $data['callback'] = 'uloginCallback';


    if ($this->request->server['HTTPS']) {
      $server = $this->config->get('config_ssl');
    } else {
      $server = $this->config->get('config_url');
    }

    if ($this->language->get('code') == 'uk') {
      $data['language_code'] = 'uk';
    } else if ($this->language->get('code') == 'ru') {
      $data['language_code'] = 'ru';
    }

    $data['base']        = $server;
    $data['description'] = $this->document->getDescription();
    $data['keywords']    = $this->document->getKeywords();
    $data['links']       = $this->document->getLinks();
    $data['styles']      = $this->document->getStyles();
    $data['scripts']     = $this->document->getScripts();
    $data['lang']        = $this->language->get('code');
    $data['direction']   = $this->language->get('direction');

    if ($this->config->get('config_google_analytics_status')) {
      $data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');
    } else {
      $data['google_analytics'] = '';
    }

    $data['name'] = $this->config->get('config_name');

    if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
      $data['icon'] = $server . 'image/' . $this->config->get('config_icon');
    } else {
      $data['icon'] = '';
    }

    if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
      $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
    } else {
      $data['logo'] = '';
    }

    $this->load->language('common/header');
    $this->load->language('account/login');

    $data['text_home']   = $this->language->get('text_home');
    $data['text_logged'] = $this->language->get('text_logged');
    $data['text_greet'] = $this->language->get('text_greet');

    $data['text_forgotten']    = $this->language->get('text_forgotten');
    $data['text_login']    = $this->language->get('text_login');
    $data['text_logout']   = $this->language->get('text_logout');
    $data['text_category'] = $this->language->get('text_category');
    $data['text_contact']  = $this->language->get('text_contact');
    $data['text_all']      = $this->language->get('text_all');
    $data['text_register']      = $this->language->get('text_register');
    $data['text_register_success']      = $this->language->get('text_register_success');
    $data['text_callback']      = $this->language->get('text_callback');

    $data['your_city'] = $this->language->get('your_city');
    $data['your_city_yes'] = $this->language->get('your_city_yes');
    $data['your_city_no'] = $this->language->get('your_city_no');
    $data['select_your_city'] = $this->language->get('select_your_city');
    $data['your_city_kiev'] = $this->language->get('your_city_kiev');
    $data['your_city_charkov'] = $this->language->get('your_city_charkov');
    $data['your_city_lviv'] = $this->language->get('your_city_lviv');
    $data['your_city_dnepr'] = $this->language->get('your_city_dnepr');
    $data['select_your_city_other'] = $this->language->get('select_your_city_other');
    $data['your_city_placeholder'] = $this->language->get('your_city_placeholder');
    $data['your_city_info'] = $this->language->get('your_city_info');
    $data['text_compare'] = $this->language->get('text_compare');
    $data['text_wishlist'] = $this->language->get('text_wishlist');
    $data['text_login_enter'] = $this->language->get('text_login_enter');
    $data['text_register_submit'] = $this->language->get('text_register_submit');
    $data['text_login_socials'] = $this->language->get('text_login_socials');

    $this->load->model('catalog/information');

    $data['informations'] = array();

    foreach ($this->model_catalog_information->getInformations() as $result) {
      if ($result['top']) {
        $data['informations'][] = array(
            'title' => $result['title'],
            'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
        );
      }
    }

    $data['home']      = $this->url->link('common/home');
    $data['logged']    = $this->customer->isLogged();
    $data['forgotten'] = $this->url->link('account/forgotten', '', 'SSL');
    $data['account']   = $this->url->link('account/account', '', 'SSL');
    $data['logout']    = $this->url->link('account/logout', '', 'SSL');
    $data['contact']   = $this->url->link('information/contact');
    $data['telephone'] = explode(',',$this->config->get('config_telephone'));

    $data['compare_total_count'] = (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0);
    $data['wishlist_total_count'] = (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0);

    $status = true;

    if (isset($this->request->server['HTTP_USER_AGENT'])) {
      $robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

      foreach ($robots as $robot) {
        if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
          $status = false;

          break;
        }
      }
    }

    // Menu
    $this->load->model('catalog/category');

    $this->load->model('catalog/product');

    $this->load->model('tool/image');

    $data['categories'] = array();

    $categories = $this->model_catalog_category->getCategories(0);

    foreach ($categories as $category) {
      if ($category['top']) {
        // Level 2
        $children_data = array();

        $children = $this->model_catalog_category->getCategories($category['category_id']);

        foreach ($children as $child) {
          // Level 3
          $level_3_data = array();

          $level_3_categories = $this->model_catalog_category->getCategories($child['category_id']);

          foreach ($level_3_categories as $level_3_category) {
            $level_3_filter_data = array(
                'filter_category_id'  => $level_3_category['category_id'],
                'filter_sub_category' => true
            );

            $level_3_data[] = array(
                'name' => $level_3_category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($level_3_filter_data) . ')' : ''),
                'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $level_3_category['category_id'])
            );
          }
          // End level 3

          $filter_data = array(
              'filter_category_id'  => $child['category_id'],
              'filter_sub_category' => true
          );

          if($child['image']) {
            $image = $this->model_tool_image->resize($child['image'], 50, 50);
          } else {
            $image = $this->model_tool_image->resize('placeholder.png', 50, 50);
          }

          $children_data[] = array(
              'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
              'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
              'thumb' => $image,
              'children' => $level_3_data
          );
        }

        // Level 1
        $data['categories'][] = array(
            'name'     => $category['name'],
            'children' => $children_data,
            'column'   => $category['column'] ? $category['column'] : 1,
            'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
        );
      }
    }

    $data['language'] = $this->load->controller('common/language');
    $data['currency'] = $this->load->controller('common/currency');
    $data['search']   = $this->load->controller('common/search');
    $data['cart']     = $this->load->controller('common/cart');

    // For page specific css
    if (isset($this->request->get['route'])) {
      if (isset($this->request->get['product_id'])) {
        $class = '-' . $this->request->get['product_id'];
      } elseif (isset($this->request->get['path'])) {
        $class = '-' . $this->request->get['path'];
      } elseif (isset($this->request->get['manufacturer_id'])) {
        $class = '-' . $this->request->get['manufacturer_id'];
      } else {
        $class = '';
      }

      $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
    } else {
      $data['class'] = 'common-home';
    }

    @$data['route'] = $this->request->get['route'];

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
    } else {
      return $this->load->view('default/template/common/header.tpl', $data);
    }
  }
}
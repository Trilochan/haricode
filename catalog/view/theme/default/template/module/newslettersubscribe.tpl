<div id="subscribe" class="container-fluid" style="padding: 0px; width: 175px; margin: 0px;">
    <div class="subscribe-section">
        <!-- <h3><?php echo $heading_title; ?></h3> -->
    </div>
    <div class="subscribe-section" style="padding: 0px">
        <form class="form-inline" name="subscribe">
            <div class="form-group" style="float: left">
                <label class="sr-only" for="exampleInputEmail3">Email address</label>
                <input style="width: 140px" type="email" id="subscribe_email" name="subscribe_email" class="form-control subscribe my-top-banner-mail-input" placeholder="<?php echo $entry_email; ?>">
            </div>
            <?php if ($option_unsubscribe) { ?>
                <button type="button" class="my-subscribe-button" id="subscribe-btn" onclick="email_subscribe()"></button>
                <button type="button" class="my-subscribe-button" id="subscribe-btn" onclick="email_unsubscribe()"></button>
            <?php } else { ?>
                <button type="button" class="my-subscribe-button" id="subscribe-btn" onclick="email_subscribe()"></button>
            <?php } ?>
            <p class="text-danger col-sm-12" id="subscribe_result"></p>
        </form>
    </div>
</div><!-- /#subscribe -->

<script language="javascript">
    function email_subscribe() {
        $.ajax({
            type: 'post',
            url: 'index.php?route=module/newslettersubscribe/subscribe',
            dataType: 'html',
            data: $("form[name=subscribe]").serialize(),
            success: function (html) {
                eval(html);
            }
        });
    }
    function email_unsubscribe() {
        $.ajax({
            type: 'post',
            url: 'index.php?route=module/newslettersubscribe/unsubscribe',
            dataType: 'html',
            data: $("form[name=subscribe]").serialize(),
            success: function (html) {
                eval(html);
            }
        });
    }
</script>

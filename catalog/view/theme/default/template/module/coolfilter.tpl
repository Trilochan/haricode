<?php if ($coolfilters) { ?>
  <noindex>
    <div class="box coolfilter">
      <!-- <div class="box-heading"><?php echo $heading_title; ?></div> -->
      <div class="box-content">
        <?php foreach ($coolfilters as $coolfilter) { ?>
          <?php if (isset($coolfilter['coolfilters'])) { ?>
            <?php if ($coolfilter['style_id'] == 'list') { ?>
              <div class="coolfilter-item coolfilter-item-list">
                <b><?php echo $coolfilter['name']; ?></b>
                <i class="inform" data-toggle="tooltip" data-placement="top" title="<?php echo $coolfilter['description']; ?>"></i>
                <ul>
                  <?php foreach ($coolfilter['coolfilters'] as $coolfilter_value) { ?>
                    <?php if ($coolfilter_value['count'] || !$count_enabled) { ?>
                      <li><a href="<?php echo $coolfilter_value['href']; ?>" <?php if ($coolfilter_value['active']) { ?>class="coolfilter_active"<?php } ?> data-key="<?php echo $coolfilter_value['key']; ?>"
                             data-value="<?php echo $coolfilter_value['value']; ?>"><?php echo $coolfilter_value['name']; ?></a> <?php echo $coolfilter_value['view_count']; ?></li>
                    <?php } else { ?>
                      <li><?php echo $coolfilter_value['name']; ?> <?php echo $coolfilter_value['view_count']; ?></li>
                    <?php } ?>
                  <?php } ?>
                </ul>
              </div>
            <?php } ?>
            <?php if ($coolfilter['style_id'] == 'checkbox') { ?>
              <div class="coolfilter-item coolfilter-item-checkbox">
                <b><?php echo $coolfilter['name']; ?></b>
                <i class="inform" data-toggle="tooltip" data-placement="top" title="<?php echo $coolfilter['description']; ?>"></i>
                <ul>
                  <?php foreach ($coolfilter['coolfilters'] as $coolfilter_value) { ?>
                    <?php if ($coolfilter_value['count'] || !$count_enabled) { ?>
                      <li class="active_my">
                        <div class="inputChackWrap clearfix"><input type="checkbox" <?php if ($coolfilter_value['active']) { ?>checked="checked"<?php } ?>>
                          <a href="<?php echo $coolfilter_value['href']; ?>" <?php if ($coolfilter_value['active']) { ?>class="coolfilter_active"<?php } ?> data-key="<?php echo $coolfilter_value['key']; ?>"
                             data-value="<?php echo $coolfilter_value['value']; ?>"><?php echo $coolfilter_value['name']; ?></a> <span><?php echo $coolfilter_value['view_count']; ?></span></div>
                      </li>

                    <?php } else { ?>
                      <li class="disable_my">
                        <div class="inputChackWrap clearfix"><input type="checkbox" disabled="disabled"></div>
                        <span><?php echo $coolfilter_value['name']; ?> <?php echo $coolfilter_value['view_count']; ?></span></li>
                    <?php } ?>
                  <?php } ?>
                </ul>
              </div>
            <?php } ?>

            <?php if ($coolfilter['style_id'] == 'select') { ?>
              <div class="coolfilter-item coolfilter-item-select">
                <div class="coolfilter-item-select-head"><?php echo $coolfilter['name']; ?>
                  <div class="coolfilter-item-select-button"></div>
                </div>
                <div class="coolfilter-item-select-list">
                  <ul>
                    <?php foreach ($coolfilter['coolfilters'] as $coolfilter_value) { ?>
                      <?php if ($coolfilter_value['count'] || !$count_enabled) { ?>
                        <li><input type="checkbox" <?php if ($coolfilter_value['active']) { ?>checked="checked"<?php } ?>><a href="<?php echo $coolfilter_value['href']; ?>" <?php if ($coolfilter_value['active']) { ?>class="coolfilter_active"<?php } ?>
                                                                                                                             data-key="<?php echo $coolfilter_value['key']; ?>" data-value="<?php echo $coolfilter_value['value']; ?>"><?php echo $coolfilter_value['name']; ?></a> <?php echo $coolfilter_value['view_count']; ?>
                        </li>
                      <?php } else { ?>
                        <li><input type="checkbox" disabled="disabled"><?php echo $coolfilter_value['name']; ?> <?php echo $coolfilter_value['view_count']; ?></li>
                      <?php } ?>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            <?php } ?>

            <?php if ($coolfilter['style_id'] == 'image') { ?>
              <div class="coolfilter-item coolfilter-item-image">
                <div class="coolfilter-item-image-head"><?php echo $coolfilter['name']; ?></div>
                <?php foreach ($coolfilter['coolfilters'] as $coolfilter_value) { ?>
                  <?php if ($coolfilter_value['count'] || !$count_enabled) { ?>
                    <a href="<?php echo $coolfilter_value['href']; ?>" <?php if ($coolfilter_value['active']) { ?>class="coolfilter_active"<?php } ?> data-key="<?php echo $coolfilter_value['key']; ?>"
                       data-value="<?php echo $coolfilter_value['value']; ?>"><img src="<?php echo $coolfilter_value['image']; ?>" alt="<?php echo $coolfilter_value['name']; ?><?php echo $coolfilter_value['view_count']; ?>"
                                                                                   title="<?php echo $coolfilter_value['name']; ?><?php echo $coolfilter_value['view_count']; ?>"></a>
                  <?php } else { ?>
                    <img src="<?php echo $coolfilter_value['image']; ?>" alt="<?php echo $coolfilter_value['name']; ?><?php echo $coolfilter_value['view_count']; ?>"
                         title="<?php echo $coolfilter_value['name']; ?><?php echo $coolfilter_value['view_count']; ?>">
                  <?php } ?>
                <?php } ?>
              </div>
            <?php } ?>

            <?php if ($coolfilter['style_id'] == 'slider') { ?>


              <div class="coolfilter-item coolfilter-item-slider" id="<?php echo($coolfilter['type_index']); ?>">
                <b><?php echo $coolfilter['name']; ?></b>
                <i class="inform" data-toggle="tooltip" data-placement="top" title="<?php echo $coolfilter['description']; ?>"></i>

                <div class="coolfilter-item-slider-body">
                  <input type="text" id="<?php echo($coolfilter['type_index']); ?>" style="display: none;" data-key="<?php echo($coolfilter['type_index'][0]); ?>" data-value="<?php echo $coolfilter['coolfilters'][0]['value'] . ',' . $coolfilter['coolfilters'][1]['value']; ?>"/>

                  <div id="<?php echo($coolfilter['type_index']); ?>slider-range" data-key="<?php echo($coolfilter['type_index'][0]); ?>" class="slider-range coolfilter_active"></div>
                </div>


                <script>
                  function initSlider<?php echo($coolfilter['type_index']); ?>() {
                    if (/\W<?php echo($coolfilter['type_index'][0]); ?>:[\d\.]+,[\d\.]+/.test(location.href)) {
                      var myRe = /\W<?php echo($coolfilter['type_index'][0]); ?>:([\d\.]+),([\d\.]+)/;
                      var <?php echo($coolfilter['type_index']); ?>coolfilterValue = myRe.exec(location.href);
                      startValue = <?php echo($coolfilter['type_index']); ?>coolfilterValue[1];
                      endValue = <?php echo($coolfilter['type_index']); ?>coolfilterValue[2];
                      $("#<?php echo($coolfilter['type_index']); ?>").attr('data-value', startValue + ',' + endValue);
                      $("#<?php echo($coolfilter['type_index']); ?>slider-range").attr('data-value', startValue + ',' + endValue);
                      $('#<?php echo($coolfilter['type_index']); ?> #<?php echo($coolfilter['type_index']); ?>_from').val(startValue);
                      $('#<?php echo($coolfilter['type_index']); ?> #<?php echo($coolfilter['type_index']); ?>_to').val(endValue);
                    } else {
                      startValue = <?php echo $coolfilter['coolfilters'][0]['value']; ?>;
                      endValue = <?php echo $coolfilter['coolfilters'][1]['value']; ?>;
                    }

                    min = <?php echo $coolfilter['coolfilters'][0]['value']; ?>;
                    max = <?php echo $coolfilter['coolfilters'][1]['value']; ?>;
                    values: [startValue, endValue];

                    $("#<?php echo($coolfilter['type_index']); ?> .slider-range").ionRangeSlider({
                      min: min,
                      max: max,
                      type: "double",
                      grid: true,
                      postfix: '<?php //echo $currency_symbol_right; ?>',
                      from: startValue,
                      to: endValue,
                      onFinish: function (date) {


                        value = date.from + "," + date.to;
                        $('#<?php echo($coolfilter['type_index']); ?> div#<?php echo($coolfilter['type_index']); ?>slider-range').attr('data-value', value);
                        $('#<?php echo($coolfilter['type_index']); ?> #<?php echo($coolfilter['type_index']); ?>_to').html(date.to);
                        $('#<?php echo($coolfilter['type_index']); ?> #<?php echo($coolfilter['type_index']); ?>_from').html(date.from);

                        //$("#<?php //echo($coolfilter['type_index']); ?>// input").addClass("coolfilter_active");
                        apply();
                      },

                      onStart: function (obj) {

                      }
                    });
                  }
                  initSlider<?php echo($coolfilter['type_index']); ?>();
                </script>
              </div>
            <?php } ?>
          <?php } ?>
        <?php } ?>

        <!--<a onclick="apply();"; id="coolfilter_apply_button" class="button"><span><?php echo $text_apply; ?></span></a>-->

      </div>
      <div class="bottom">&nbsp;</div>
    </div>
  </noindex>
<?php } ?>

<script>
  $(document).ready(function () {

    if ($(".coolfilter_active").not("#<?php echo($coolfilter['type_index']); ?>").length > 0) {

      var arr = {};
      $(".coolfilter-item").not("#<?php echo($coolfilter['type_index']); ?>").each(function (i, coolfilter_item) {

        var urls = {};
        $(coolfilter_item).find(".coolfilter_active").each(function (i, coolfilter_active) {
          value = $(coolfilter_active).parent().find('a').first().outerHTML;
          urls[+i] = value;
        });

        name = $(coolfilter_item).find('b').text();
        arr[+i] = {
          'name': name,
          'urls': urls
        };

      });


      var selected = '<div class="coolfilter-item coolfilter-item-checkbox coolfilter-selected" id="coolfilter-selected"><div>Выбранные параметры:</div>';
      $.each(arr, function (index, val) {

        if (!$.isEmptyObject(val.urls)) {
          selected += '<div class="coolfilter-selected-group"><b>' + val.name + '</b><ul>';
          urls = '';
          $.each(val.urls, function (index, val) {
            selected += '<li>' + val + '</li>';
            ;
          })
          selected += '</div></ul>';
        }
        ;

      });
      selected += '<br><a onclick="resetcoolfilter();"><?php echo $text_reset_coolfilter; ?></a><br>';
      selected += '</div>';

      $(".coolfilter .box-heading").after(selected);
      $(".coolfilter-selected ul li a").each(function () {
        $(this).removeClass('coolfilter_active');
        $(this).html('<span style="color:red">&times; </span>' + $(this).html());
      });

    }
  });


</script>

<script>
  function apply() {

    $(".coolfilter").addClass('disable');


    var coolfilter = '';
    var arr = {};
    $(".coolfilter_active").each(function (i) {
      var key = $(this).attr("data-key");
      var value = $(this).attr("data-value");
      if (arr[key] === undefined) {
        arr[key] = '';
        arr[key] += value;
      } else {
        arr[key] += ',' + value;
      }

    });

    $.each(arr, function (index, val) {
      coolfilter += index + ':' + val + ';';
    });
    coolfilter = coolfilter.substr(0, coolfilter.length - 1);
    setUrl(coolfilter);
  }
  ;

  function setUrl(coolfilter) {
    var href = location.href;

    var exp = /&page=(.*?)(&|$)/g;
    href = href.replace(exp, "");
    var exp = /&coolfilter=(.*?)(&|$)/g;
    href = href.replace(exp, "");
    href = href.replace(exp, "$2") + '&coolfilter=' + coolfilter;

    window.history.pushState(null, null, href);

    $.ajax({
      url: href,
      type: 'GET',
      success: function (data) {
        var products = $(data).find('#products').html();
        var pagination = $(data).find('#pagination').html();
        var filter = $(data).find('.coolfilter').html();
        if (products !== 'undefined') {
          $('#products').html(products);
          $('#pagination').html(pagination);
        }
        $('.coolfilter').html(filter);

        if (localStorage.getItem('display') == 'list') {
          $('#list-view').trigger('click');
        } else {
          $('#grid-view').trigger('click');
        }

        initSliderprice();
        initSliderlength();
        initSliderwidth();
        initSliderheight();


        filterClick();
        filtersPicket();
        $(".coolfilter").removeClass('disable');
      }
    });

  }

  /*
   function addButtonReset() {
   var href = location.href;
   if (/(\?|&)coolfilter=(.*?)/.test(href)) {
   $(".coolfilter-selected").append('<br><a onclick="resetcoolfilter();"><?php echo $text_reset_coolfilter; ?></a><br>');
   }
   }

   addButtonReset();
   */
  function resetcoolfilter() {
    setUrl('')
  }

  $(".coolfilter-item-select-head").click(function () {
    $(".coolfilter-item-select-list").not($(this).next(".coolfilter-item-select-list")).hide();
    $(this).next(".coolfilter-item-select-list").toggle();
    return false;
  });

  $(document).click(function (e) {
    var $target = $(e.target);
    if (!$target.is("a") && !$target.is("input:checkbox")) {
      $(".coolfilter-item-select-list").hide();
    }
  });

  function filterClick() {
    $(".coolfilter-item a").on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass("coolfilter_active");
      var checkbox = $(this).siblings("input:checkbox");
      if (checkbox.is(':checked')) {
        checkbox.attr('checked', false);
        $(this).parent().find('.inputChackWrap').css('background', 'transparent');
      } else {
        checkbox.attr('checked', true);
      }
      apply();
    });

    $(".coolfilter-item input[type=checkbox], .coolfilter-item-select input:checkbox").on('click', function () {
      $(this).siblings("a").toggleClass("coolfilter_active");
      apply();
    });
  }
  filterClick();

  function filtersPicket() {
    var filterPicket = [];
    $('.coolfilter a.coolfilter_active').each(function (index, element) {
      var categoryName = $(element).parents('.coolfilter-item').find('b:first').text();
      var filterName = $(element).text();
      var filterValue = $(element).attr('data-value');
      var filterKey = $(element).attr('data-key');

      if (!filterPicket[categoryName]) {
        filterPicket[categoryName] = [];
      }

      filterPicket[categoryName][index] = {
        'name': filterName,
        'value': filterValue,
        'key': filterKey
      };
    });

    var html = "";
    for (var category in filterPicket) {
      html += '<span class="filter-category">' + category + ':</span>';
      for (var value in filterPicket[category]) {
        html += '<span class="filter-name">' + filterPicket[category][value].name + '<a href="javascript: void(null);" onclick="deleteFilter(\'' + filterPicket[category][value].value + '\', \'' + filterPicket[category][value].key + '\');"><i class="fa fa-times"></i></a></span>';
      }
    }
    if (html !== "") {
      html += '<a href="javascript: void(null);" onclick="resetcoolfilter();">Сбросить все фильтры</a>';
    }

    $('.category .filter-top').html(html);
  }

  $(function () {
    filtersPicket();
  });

  function deleteFilter(value, key) {
    $('.coolfilter a.coolfilter_active[data-value=' + value + '][data-key=' + key + ']').trigger('click');
  }

</script>
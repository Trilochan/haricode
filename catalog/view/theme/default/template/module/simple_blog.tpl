<h3><?php echo $heading_title; ?>
  <?php if ($article_link) { ?>
    <a href="<?php echo $article_link; ?>" class="pull-right"><?php echo $text_show_all; ?></a>
  <?php } ?>
</h3>
<div class="simple-blog">
  <div class="row simple-blog-carousel">
    <?php if ($articles) { ?>
      <?php foreach ($articles as $article) { ?>
        <div class="col-sm-3 <?php echo $article['class']; ?>">
          <a href="<?php echo $article['href']; ?>">
            <img src="<?php echo $article['thumb']; ?>" class="img-responsive img-thumbnail center-block"/>
          </a>
          <h4 class="text-center">
            <a href="<?php echo $article['href']; ?>"><?php echo $article['article_title']; ?></a>
          </h4>
          <span class="date-added"><?php echo $article['date_added']; ?><br/></span>

          <p>
            <?php echo $article['description']; ?>
          </p>
          <a href="<?php echo $article['href']; ?>"><?php echo $text_detail; ?></a>
        </div>
      <?php } ?>
    <?php } ?>
  </div>
</div>

<script>
  $('.simple-blog-carousel').slick({
    accessibility: true,
    adaptiveHeight: true,
    arrows: true,
    dots: false,
    draggable: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    swipe: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
</script>
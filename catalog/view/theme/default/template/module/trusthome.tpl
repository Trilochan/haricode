<div class="row">
<div class="col-md-3"></div>
<div class="col-md-9">
	<?php foreach ($trusts as $trust) { ?>
	<div style="float: left;">
		<div data-toggle="tooltip" data-placement="bottom" data-html="true" title="<?php echo $trust['trust_text'];?>">
			<img src="<?php echo $trust['image'];?>" alt="" title="<?php echo $trust['trust_name'];?>">
		</div>
		<div>
			<?php echo $trust['trust_name'];?>
		</div>
	</div>
	<?php } ?>
</div>
</div>
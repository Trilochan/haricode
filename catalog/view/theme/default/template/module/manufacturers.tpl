<div id="manufacturers<?php echo $module; ?>" class="slick-carousel">
  <?php foreach ($manufacturers as $manufacturer) { ?>
    <div class="item text-center">
      <a href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>" class="img-responsive center-block"/></a>
      <h4><a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a></h4>
    </div>
  <?php } ?>
</div>
<script type="text/javascript"><!--
  $('#manufacturers<?php echo $module; ?>').slick({
    accessibility: true,
    adaptiveHeight: true,
    arrows: true,
    dots: false,
    draggable: true,
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    swipe: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });
  --></script>
'use strict'; 

var gulp = require('gulp'),
	prefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    livereload = require('gulp-livereload'),
    rename = require('gulp-rename');
 

var path = {
    build: {
        css: '../stylesheet/'
    },
    src: {
        style: '../stylesheet/sass/**/*.scss'
    },
    watch: {
        style: '../stylesheet/sass/**/*.scss'
    }
};

//style
gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(prefixer({
            browsers: ['last 3 versions'],
            cascade: true
        })) 
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest(path.build.css))
        .pipe(livereload());
});

//build all
gulp.task('build', [
    'style:build'
]);


//watch
gulp.task('watch', function(){
    livereload.listen();
    gulp.watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
});


//default
gulp.task('default', ['build', 'watch']);
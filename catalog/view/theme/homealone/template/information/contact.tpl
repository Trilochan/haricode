<?php echo $header; ?>
<script>
$('.my-header-menu-absolute').hide();
</script>
<div style="background: #eee;">
<div class="my-conteiner">
  <div style="background: #fff" class="row">
    <div style="padding: 0 15px;background: #fff" id="content"><?php echo $content_top; ?>
      <div style="height: 336px" id="my-map"></div>
      <div>
        <p class="fa fa-map-marker my-contact-marker"></p>
      </div>
      <h1 class="my-contact-heading"><?php echo $heading_title; ?></h1>
      <div class="col-md-12 col-sm-12">
        <div class="col-md-3 col-sm-3">
            <div class="my-contact-head-list">
              <span class="fa fa-map-marker"></span>
            </div>
            <p class="my-contact-heading-list"><?php echo $head_office; ?></p>
            <p class="my-contact-address"><?php echo $address; ?></p>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="my-contact-head-list">
            <span class="fa fa-mobile"></span>
          </div>
          <p class="my-contact-heading-list"><?php echo $text_telephone; ?></p>
          <ul class="my-contact-tele-ul">
            <?php foreach($telephone as $tele) { ?>
            <li><?php echo $tele; ?></li>
            <?php } ?>
          </ul>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="my-contact-head-list">
            <span class="fa fa-calendar-o"></span>
          </div>
          <p class="my-contact-heading-list"><?php echo $work_time; ?></p>
          <p style="text-align: center"><?php echo $open; ?></p>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="my-contact-head-list">
            <span class="fa fa-envelope"></span>
          </div>
          <p class="my-contact-heading-list">Наш e-mail</p>
          <p style="text-align: center">
            <?php echo $shop_email; ?>
          </p>
        </div>
      </div>
      <div style="float: left;width: 100%">
        <p class="fa fa-envelope my-contact-marker"></p>
      </div>
      <h1 class="my-contact-heading"><?php echo $text_contact; ?></h1>
      <div class="col-md-offset-3 col-md-6">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
          <fieldset style="border: none">
            <div class="col-sm-6">
            <div class="form-group required">

                <input style="float: left" placeholder="<?php echo $your_name; ?>" type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="my-contact-input" />
                <?php if ($error_name) { ?>
                <div class="text-danger"><?php echo $error_name; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="col-sm-6">
            <div class="form-group required">

                <input style="float: right" placeholder="Email" type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="my-contact-input" />
                <?php if ($error_email) { ?>
                <div class="text-danger"><?php echo $error_email; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <div class="col-sm-12">
                <textarea placeholder="<?php echo $your_massage; ?>" name="enquiry" rows="10" id="input-enquiry" class="my-contact-textarea"><?php echo $enquiry; ?></textarea>
                <?php if ($error_enquiry) { ?>
                <div class="text-danger"><?php echo $error_enquiry; ?></div>
                <?php } ?>
              </div>
            </div>
            <?php if ($site_key) { ?>
            <div class="form-group">
              <div class="col-sm-12">
                <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                <?php if ($error_captcha) { ?>
                <div class="text-danger"><?php echo $error_captcha; ?></div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
          </fieldset>
          <div class="buttons">
              <input class="my-contact-submit" type="submit" value="<?php echo $button_submit; ?>" />
          </div>
        </form>
      </div>
      <!--<h3><?php echo $text_location; ?></h3>
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <?php if ($image) { ?>
            <div class="col-sm-3"><img src="<?php echo $image; ?>" alt="<?php echo $store; ?>" title="<?php echo $store; ?>" class="img-thumbnail" /></div>
            <?php } ?>
            <div class="col-sm-3"><strong><?php echo $store; ?></strong><br />
              <address>
              <?php echo $address; ?>
              </address>
              <?php if ($geocode) { ?>
              <a href="https://maps.google.com/maps?q=<?php echo urlencode($geocode); ?>&hl=en&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
              <?php } ?>
            </div>
            <div class="col-sm-3"><strong><?php echo $text_telephone; ?></strong><br>
              <?php echo $telephone; ?><br />
              <br />
              <?php if ($fax) { ?>
              <strong><?php echo $text_fax; ?></strong><br>
              <?php echo $fax; ?>
              <?php } ?>
            </div>
            <div class="col-sm-3">
              <?php if ($open) { ?>
              <strong><?php echo $text_open; ?></strong><br />
              <?php echo $open; ?><br />
              <br />
              <?php } ?>
              <?php if ($comment) { ?>
              <strong><?php echo $text_comment; ?></strong><br />
              <?php echo $comment; ?>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <?php if ($locations) { ?>
      <h3><?php echo $text_store; ?></h3>
      <div class="panel-group" id="accordion">
        <?php foreach ($locations as $location) { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><a href="#collapse-location<?php echo $location['location_id']; ?>" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $location['name']; ?> <i class="fa fa-caret-down"></i></a></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-location<?php echo $location['location_id']; ?>">
            <div class="panel-body">
              <div class="row">
                <?php if ($location['image']) { ?>
                <div class="col-sm-3"><img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" title="<?php echo $location['name']; ?>" class="img-thumbnail" /></div>
                <?php } ?>
                <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br />
                  <address>
                  <?php echo $location['address']; ?>
                  </address>
                  <?php if ($location['geocode']) { ?>
                  <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=en&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                  <?php } ?>
                </div>
                <div class="col-sm-3"> <strong><?php echo $text_telephone; ?></strong><br>
                  <?php echo $location['telephone']; ?><br />
                  <br />
                  <?php if ($location['fax']) { ?>
                  <strong><?php echo $text_fax; ?></strong><br>
                  <?php echo $location['fax']; ?>
                  <?php } ?>
                </div>
                <div class="col-sm-3">
                  <?php if ($location['open']) { ?>
                  <strong><?php echo $text_open; ?></strong><br />
                  <?php echo $location['open']; ?><br />
                  <br />
                  <?php } ?>
                  <?php if ($location['comment']) { ?>
                  <strong><?php echo $text_comment; ?></strong><br />
                  <?php echo $location['comment']; ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <h3><?php echo $text_contact; ?></h3>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
            <div class="col-sm-10">
              <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
              <?php if ($error_enquiry) { ?>
              <div class="text-danger"><?php echo $error_enquiry; ?></div>
              <?php } ?>
            </div>
          </div>
          <?php if ($site_key) { ?>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                <?php if ($error_captcha) { ?>
                  <div class="text-danger"><?php echo $error_captcha; ?></div>
                <?php } ?>
              </div>
            </div>
          <?php } ?>
        </fieldset>
        <div class="buttons">
          <div class="pull-right">
            <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
          </div>
        </div>
      </form>-->
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?>
  </div>
</div>
  </div>
<?php echo $footer; ?>
<script>
  function initMap() {
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('my-map'), {
      center: {lat: <?php echo $geocode_lat; ?>, lng: <?php echo $geocode_lng; ?>},
      scrollwheel: false,
      zoom: 16
    }); 
  }
  initMap();
</script>

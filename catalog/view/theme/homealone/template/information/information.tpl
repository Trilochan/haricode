<?php echo $header; ?>
<div style="background: #eee;">
  <div class="my-conteiner">
    
    <ul class="my-breadcrumb">
      <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
        <?php if (count($breadcrumbs) != ($key + 1)) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
        <?php } else { ?>
          <li><a><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
        <?php } ?>
      <?php } ?>
    </ul>
    <div class=" clearfix" style="background: #fff;">
      <!-- <div class="leftColumn"><?php echo $column_left; ?></div> -->
      <div id="content" class="oneColumn"><?php echo $content_top; ?>
        <h1><?php echo $heading_title; ?></h1>
      <?php echo $description; ?><?php echo $content_bottom; ?></div>
      <div class="rightColumn">
        <?php echo $column_right; ?>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
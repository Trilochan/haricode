<?php echo $header; ?>
<div style="background: #eee">
<?php echo $content_top; ?>
<div style="background: #eee">
<div class="my-conteiner clearfix">
    <div class="leftColumn">
      <?php echo $column_left; ?>

  <!-- VK plugin -->
  <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
  <div class="fb-page" style="width: 100%;" data-href="<?php echo $facebook_config; ?>"  data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $facebook_config; ?>"><a href="<?php echo $facebook_config; ?>">Facebook</a></blockquote></div></div>
  <div id="vk_groups"></div>
  <script type="text/javascript">
  VK.Widgets.Group("vk_groups", {mode: 0, width: "auto", height: "270", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, <?php echo $vk_config_id; ?>);
  </script>
  <!-- VK plugin END -->
  <!-- FaceBook plugin -->

  <script>
  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  </script>
  
  <!-- FaceBook plugin END -->

      <script src="https://apis.google.com/js/platform.js"></script>
      <div class="g-ytsubscribe my" data-channelid="<?php echo $you_tube_id; ?>" data-layout="full" data-count="default"></div>
    </div>
    <!-- .leftColumn -->
    <div class="mainColumn">

      <?php echo $content_bottom; ?>

    </div>
    <div class="rightColumn">
      <?php echo $column_right; ?>
      <!--<div class="my-home-special-block">
        <img src="catalog/view/theme/homealone/image/special1.jpg">
      </div>
      <div class="my-home-special-block">
        <img src="catalog/view/theme/homealone/image/special2.jpg">
      </div>
      <div class="my-home-special-block">
        <img src="catalog/view/theme/homealone/image/special3.jpg">
      </div>-->
    </div>
</div>
</div>
</div>
<?php echo $footer; ?>
<?php if ($show_login_form) { ?>
<script>
$('#my-login-modal').modal('show');
</script>
<?php } ?>
<footer style="margin: 0px;padding: 0px">
  <div class="my-footer-top"></div>
  <div class="footer-color-block">
    <div class="my-conteiner">
      <div class="row" style="margin: 0px">
        <div class="footer-bottom" style="float: left;width: 100%">
          <div class="footerLeftColumn">
            <?php if ($language_code == 'uk') { ?>
                <?php if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/index.php?route=common/home') { ?>
                  <img  class="footer_logo" style="padding-left: 43px; width: 260px;" src="<?php echo ('http://' . $_SERVER['SERVER_NAME'] . '/image/catalog/logo/odin-doma-logo-uk.svg') ?>"/>
                <?php } else { ?>
                  <a href="<?php echo $home; ?>">
                    <img class="footer_logo" style="padding-left: 43px; width: 260px;" src="<?php echo ('http://' . $_SERVER['SERVER_NAME'] . '/image/catalog/logo/odin-doma-logo-uk.svg') ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
                  </a>
                <?php } ?>
           <?php } else {  ?>
              <?php if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/index.php?route=common/home') { ?>
                <img class="footer_logo" style="padding-left: 43px; width: 260px;"  src="<?php echo ('http://' . $_SERVER['SERVER_NAME'] . '/image/catalog/logo/odin-doma-logo-ru.svg') ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
              <?php } else { ?>
                <a href="<?php echo $home; ?>">
                  <img class="footer_logo" style="padding-left: 43px; width: 260px;" src="<?php echo ('http://' . $_SERVER['SERVER_NAME'] . '/image/catalog/odin-doma-logo-ru.svg') ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
                </a>
              <?php } ?>  
            <?php } ?>  


            
            <div class="addres">
              <img src="/catalog/view/theme/homealone/image/call-support.png" alt="">
              <p class="footer-block-text2"><?php echo $work_time; ?></p>
            </div>
            <div class="footerKorzinaText">
              <img src="/catalog/view/theme/homealone/image/buyCart-red.png" alt="">
              <p><?php echo $order_time; ?></p>
            </div>
            <div class="phoneNumber">
              <i class="fa fa-phone"></i>
              <div class="phone">
                <?php foreach($telephone as $tele) { ?>
                <p class="footer-block-text"><?php echo $tele; ?></p>
                <?php } ?>
              </div>
            </div>
            <div class="mail">
              <i class="fa fa-envelope-o"></i>
              <p class="footer-block-text text-email"><?php echo $shop_email; ?></p>
            </div>
          </div>
          <div class="centerColum">
            <h3 class="my-footer-heading"><?php echo $text_information; ?></h3>
              <a class="my-footer-text2" href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a>
              <?php foreach ($informations as $information) { ?>
                <?php if($information['bottom_infoblock']) { ?>
                  <a class="my-footer-text2" href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                <?php } ?>
              <?php } ?>
          </div>
          <div class="footerRightColumn">
            <h3 class="my-footer-heading footerAkciyaText ml55"><?php echo $text_gift_for_all; ?></h3>
            <div class="footer-button-block">
              <div class="my-footer-input-block" id="subscribe">
                <form class="form-inline" name="subscribe">
                  <p class="text-danger col-sm-12" id="subscribe_result"></p>
                  <input class="my-footer-input" type="text" placeholder="Ваш E-mail" id="subscribe_email" name="subscribe_email" onkeydown="if (event.keyCode == 13) {document.getElementById('subscribe-btn').click();return false;}">
                  <input class="my-footer-submit" type="submit" value="<?php echo $text_gift_take_naw; ?>" id="subscribe-btn" onclick="email_subscribe(); return false;">
                </form>
                <script language="javascript">
                  function email_subscribe() {
                    $.ajax({
                      type: 'post',
                      url: 'index.php?route=module/newslettersubscribe/subscribe',
                      dataType: 'html',
                      data: $("form[name=subscribe]").serialize(),
                      success: function (html) {
                        eval(html);
                      }
                    });
                  }
                  function email_unsubscribe() {
                    $.ajax({
                      type: 'post',
                      url: 'index.php?route=module/newslettersubscribe/unsubscribe',
                      dataType: 'html',
                      data: $("form[name=subscribe]").serialize(),
                      success: function (html) {
                        eval(html);
                      }
                    });
                  }
                </script>
              </div>
              <?php if($socials){?>
                <?php foreach($socials as $social){?>
                  <a href="<?php echo $social['href'];?>" target="_blank">
                    <img src="<?php echo $social['thumb'];?>" alt="<?php echo $social['name'];?>" title="<?php echo $social['name'];?>" />
                  </a>
                <?php } ?>
              <?php } ?> 
            </div>
            
            <img class="imagePayment" src="catalog/view/theme/homealone/image/payImage.png" alt="payment">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bottom-site">
    <div class="my-conteiner">
      <?php foreach ($informations as $information) { ?>
        <?php if($information['bottom']) { ?>
          <a class="my-top-bottom-link" href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?><span></span></a>
        <?php } ?>
      <?php } ?>
      <a class="my-top-bottom-link" href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a>
      <a href="http://im.webpr.ua"><p class="webpr-text"><span></span><?php echo $text_developer; ?></p></a>
    </div>
  </div>
</footer>
<script>
var siteUrl = getUrlParam('route');
if(siteUrl === 'product/product' || siteUrl === 'product/category' || siteUrl === 'product/search' || siteUrl === 'simple_blog/article/view'){
// $('.my-product-slider-url').css('height','300px');
$('.my-product-slider-url').slick({
slidesToShow: 6,
slidesToScroll: 1,
draggable: true,
arrows: true,
swipeToSlide: true,
responsive: [
{
breakpoint: 1360,
settings: {
slidesToShow: 5,
slidesToScroll: 1,
}
},
{
breakpoint: 1200,
settings: {
slidesToShow: 4,
slidesToScroll: 1,
}
},
]
});
}
$('.newMy').slick({
slidesToShow: 4,
slidesToScroll: 1,
draggable: true,
arrows: true,
swipeToSlide: true,
responsive: [
{
breakpoint: 1440,
settings: {
slidesToShow: 3,
slidesToScroll: 1,
arrows: true,
}
},
{
breakpoint: 1050,
settings: {
slidesToShow: 2,
slidesToScroll: 1,
arrows: true,
}
},
]
});
</script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAukSHxXlVib9JoYAIggWMqfww3HFabF3s<?php if (isset($language_code)) echo "&language=" . $language_code; ?>"></script>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
</body></html>
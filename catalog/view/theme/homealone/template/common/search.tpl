<div id="search" class="my-header-search">
  <p class="my-header-search-text"><?php echo $text_find_product; ?></p>
  <input type="text" name="search" value="<?php echo $search; ?>" class="my-header-search-input" />
    <button type="button" class="my-header-search-button"><i class="fa fa-search"></i></button>
</div>
<?php if (count($languages) > 1) { ?>
<div class="my-language-block">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
  <?php foreach ($languages as $language) { ?>
  <?php if ($language['code'] == $code) { ?>
  <a class="for-select-len" href="<?php echo $language['code']; ?>"><?php echo $language['name']; ?></a>
  <?php } else { ?>
  <a class="for-select-len-2" href="<?php echo $language['code']; ?>"><?php echo $language['name']; ?> </a>
  <?php } ?>
  <?php } ?>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect_route" value="<?php echo $redirect_route; ?>" />
  <input type="hidden" name="redirect_query" value="<?php echo $redirect_query; ?>" />
  <input type="hidden" name="redirect_ssl" value="<?php echo $redirect_ssl; ?>" />
</form>
</div>
<?php } ?>

<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
  <!--<![endif]-->
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon" />
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/homealone/stylesheet/mystyle.css" rel="stylesheet">
    <link href="catalog/view/theme/homealone/stylesheet/style.min.css" rel="stylesheet">
    <!-- <link href="catalog/view/theme/homealone/stylesheet/smartfilter.css" rel="stylesheet"> -->
    <!-- <link href="catalog/view/theme/homealone/stylesheet/search-input.css" rel="stylesheet"> -->
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/underscore.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/jquery.hoverIntent.minified.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/AjaxModalLoginRegister.js" type="text/javascript"></script>
    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU"></script>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/myscript.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/quick_buy.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/callback.js" type="text/javascript"></script>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php echo $google_analytics; ?>
  </head>
  <body class="<?php echo $class; ?>" id="mediaPx">
    <!-- <div id="my-reg-modal" class="modal fade" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-reg-block2">
          <span data-toggle="modal" data-target="#my-reg-modal" class="fa fa-times my-close-reg"></span>
          <h3 class="modal-reg-heading"><?php echo $text_register; ?></h3>
          <form id="reg-main">
            <div style="padding: 0px 114px">
              <label class="my-reg-label">Email</label>
              <input class="my-reg-input" name="register_email" type="text" placeholder="exemple@mail.com">
              <label class="my-reg-label">Полное имя</label>
              <input class="my-reg-input" name="firstname" type="text" placeholder="Лиса Орлова">
              <label class="my-reg-label">Пароль</label>
              <input class="my-reg-input" name="register_password" type="text" placeholder="*********">
              <label class="my-reg-label">Подтвердите пароль</label>
              <input class="my-reg-input" name="register_password2" type="text" placeholder="*********">
              <p class="text-danger"></p>
            </div>
          </form>
          <p onclick="ajaxRegister.submit('#reg-main')" class="my-reg-submit">зарегистрироватся</p>
        </div>
      </div>
    </div> -->
    <div id="my-callback-modal" class="modal fade" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-reg-block2" style="padding-bottom: 10px;">
          <span data-toggle="modal" data-target="#my-callback-modal" class="fa fa-times my-close-reg callBackClose"></span>
          <h3 class="modal-reg-heading"><?php echo $text_callback; ?></h3>
          <form id="callback-main">
            <div style="padding: 0px 114px">
              <label class="my-reg-label">Ваше имя</label>
              <input class="my-reg-input" name="name" type="text" placeholder="Как можно к Вам обращаться?">
              <label class="my-reg-label">Номер телефона</label>
              <input class="my-reg-input" name="telephone" type="text" placeholder="+38 (___) ___-__-__">
              <p class="text-danger"></p>
              <p style="color: green" id="callback-text-success" class="text-success"></p>
            </div>
          </form>
          <p onclick="callback.submit('#callback-main')" class="my-reg-submit">Перезвоните мне</p>
        </div>
      </div>
    </div>
    <div id="my-login-modal" class="modal fade" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-reg-block">
          <div style="float: left;width: 50%">
            <h3 class="modal-reg-heading"><?php echo $text_login; ?></h3>
            <form id="login-main">
              <div style="padding: 0px 36px">
                <label class="my-reg-label">Email</label>
                <input class="my-reg-input" name="email" type="text" placeholder="e-mail">
                <label class="my-reg-label">Пароль</label>
                <input class="my-reg-input" name="password" type="password" placeholder="*********">
                <button class="btn btn-default showpassword" type="button"><span class="fa fa-eye-slash"></span></button>
                <p class="text-danger"></p>
              </div>
            </form>
            <p onclick="ajaxLogin.submit('#login-main')" class="my-reg-submit"><?php echo $text_login_enter; ?></p>
            <p class="passForgWhy"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></p>
          </div>
          <div style="float: left;width: 50%">
            <span data-toggle="modal" data-target="#my-login-modal" class="fa fa-times my-close-reg"></span>
            <h3 class="modal-reg-heading"><?php echo $text_register; ?></h3>
            <form id="reg-main">
              <div style="padding: 0px 36px">
                <label for="register_email" class="my-reg-label">Email</label>
                <input class="my-reg-input" name="register_email" type="text" placeholder="e-mail">
                <label for="register_password" class="my-reg-label">Пароль</label>
                <input class="my-reg-input" name="register_password" type="password" placeholder="*********">
                <button class="btn btn-default showpassword" type="button"><span class="fa fa-eye-slash"></span></button>
                <p style="color: red" class="text-danger-reg"></p>
              </div>
            </form>
            <p onclick="ajaxRegister.submit('#reg-main')" class="my-reg-submit"><?php echo $text_register_submit; ?></p>
          </div>
          <div class="clearfix"></div>
          <div class="text-center">
          <p class="socialInfoText"><?php echo $text_login_socials; ?></p>
            <!-- <div><?php //echo $ulogin_form_marker ?></div> -->
            <div id="uLogin" data-ulogin="display=buttons;fields=first_name,last_name,email;optional=phone,city,country,nickname,sex,photo_big,bdate,photo;redirect_uri=<?php echo $redirect_uri; ?>;callback=uloginCallback">
              <div class="socialIconEnter">
                <span class="my-footer-vk my-footer-button" data-uloginbutton="vkontakte"><img src="catalog/view/theme/homealone/image/vk.png" alt=""></span>
                <span class="my-footer-facebook my-footer-button" data-uloginbutton="facebook"><img src="catalog/view/theme/homealone/image/fb.png" alt=""></span>
                <span class="my-footer-google my-footer-button" data-uloginbutton="google"><img src="catalog/view/theme/homealone/image/m-gp.png" alt=""></span>
              </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    <div id="register_success" class="modal fade" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-reg-block2" style="padding-bottom: 10px;">
          <span data-toggle="modal" data-target="#register_success" class="fa fa-times my-close-reg"></span>
          <h3 class="modal-reg-heading"><?php echo $text_register_success; ?></h3>
        </div>
      </div>
    </div>
    <header>
      <div style="background: #e6e6e6">
        <div class="my-conteiner">
          <div class="row" style="margin: 0px">
            <div class="my-top-header">
              <div class="topHeaderlinks">
                <?php foreach ($informations as $information) { ?>
                <a class="my-top-header-link" href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                <?php } ?>
                <a class="my-top-header-link" href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a>
              </div>
              <?php if ($logged) { ?>
              <p class="my-top-header-text"><?php echo $text_greet; ?> <a href="<?php echo $account; ?>"><?php echo $text_logged; ?></a></p>
              <?php } else { ?>
              <p class="my-top-header-text"><?php echo $text_greet; ?> <a href="javascript: void(null);" data-toggle="modal" data-target="#my-login-modal"><?php echo $text_login_enter; ?></a></p>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <div style="background: #f7f7f7">
        <div class="my-conteiner">
          <div class="my-header">
            <div class="row" style="margin: 0px">
              <div id="logo" class="logo">
              <?php if ($language_code == 'uk') { ?>
                <?php if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/index.php?route=common/home') { ?>
                <img class="my-logo-img" src="<?php echo ('http://' . $_SERVER['SERVER_NAME'] . '/image/catalog/logo/odin-doma-logo-uk.svg') ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
                <?php } else { ?>
                <a href="<?php echo $home; ?>">
                  <img class="my-logo-img" src="<?php echo ('http://' . $_SERVER['SERVER_NAME'] . '/image/catalog/logo/odin-doma-logo-uk.svg') ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
                </a>
                <?php } ?>
           <?php } else {  ?>
              <?php if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/index.php?route=common/home') { ?>
                <img class="my-logo-img" src="<?php echo ('http://' . $_SERVER['SERVER_NAME'] . '/image/catalog/logo/odin-doma-logo-ru.svg') ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
                <?php } else { ?>
                <a href="<?php echo $home; ?>">
                  <img class="my-logo-img" src="<?php echo ('http://' . $_SERVER['SERVER_NAME'] . '/image/catalog/logo/odin-doma-logo-ru.svg') ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
                </a>
                <?php } ?> 
            <?php } ?>  



              
              </div>
              <!-- <div class="header1440">
                <div class="city">
                  <p class="my-header-geo"><button class="my-geo-city-button"><span onclick="showGeo();" id="my-geo-city-button">Ваш город:</span>
                  </button><a onclick="showGeo();" id="this-my-geo"></a></p>
                </div>
              </div> -->
              <div class="city">
                <p class="my-header-geo" onclick="showGeo();"><button class="my-geo-city-button"><span id="my-geo-city-button"><?php echo $your_city; ?></span></button>
                <a id="this-my-geo"></a></p>
                <div class="my-geo-quest">
                  <p class="my-time-geo"></p>
                  <span class="my-geo-yes"><?php echo $your_city_yes; ?></span>
                  <span class="my-geo-no"><?php echo $your_city_no; ?></span>
                </div>
              </div>
              
              <div class="cartHeader">
                <?php echo $cart; ?>
              </div>
              <div class="contentHeader">
                <div class="orderLanguage">
                  <?php echo $language; ?>
                  <button data-toggle="modal" data-target="#my-callback-modal" class="my-header-call-back"><?php echo $text_callback; ?></button>
                </div>
                <div class="desireCompare">
                  <a href="index.php?route=product/compare" class="my-header-compire"><span class="fa fa-balance-scale"></span><?php echo $text_compare; ?> <span id="compare-total-count">(<?php echo $compare_total_count; ?>)</span></a>
                  <a href="index.php?route=account/wishlist" class="my-header-wish" onclick="wishlist.info(); return false;"><span class="fa fa-heart"></span><?php echo $text_wishlist; ?> <span id="wishlist-total-count">(<?php echo $wishlist_total_count; ?>)</span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="my-header-line"></div>
        <div class="my-conteiner" style="position: relative">
          <div class="row" style="margin: 0px;">
            <div style="float: left;width: 100%">
              <?php echo $search; ?>
              <div class="my-header-phone-block">
                <?php foreach($telephone as $tele) { ?>
                <p class="my-header-phone"><?php echo $tele; ?></p>
                <?php } ?>
                <!--<p class="my-header-phone">063 123 45 67</p>
                <p class="my-header-phone">063 123 45 67<span class="my-header-phone-line"></span></p>
                <p class="my-header-phone">063 123 45 67<span class="my-header-phone-line"></span></p>-->
                <p class="my-header-phone-circle"><span class="fa fa-phone"></span></p>
              </div>
            </div>
          </div>
          <div class="my-header-menu-absolute">
            <div class="my-header-menu">
              <p class="my-header-menu-heanding">Каталог</p>
              <ul class="my-header-drop" <?php if(!empty($route) && $route != 'common/home') { echo 'style="display:none;"'; } ?>>
                <?php foreach ($categories as $category) { ?>
                <li class="my-header-drop-li">
                  <a href="<?php echo $category['href']; ?>">
                    <?php echo $category['name']; ?>
                  </a>
                  <?php if($category['children']) { ?>
                  <i class="fa fa-chevron-right my-cat-right-arrow"></i>
                  <ul class="my-header-drop2">
                    <?php foreach ($category['children'] as $children) { ?>
                    <li class="my-header-drop-li2">
                      <a href="<?php echo $children['href']; ?>"><?php echo $children['name']; ?></a>
                      <?php if ($children['children']) { ?>
                      <i class="fa fa-chevron-right my-cat-right-arrow"></i>
                      <ul class="my-header-drop3">
                        <?php foreach ($children['children'] as $children3) { ?>
                        <li class="my-header-drop-li3"><a href="<?php echo $children3['href']; ?>"><?php echo $children3['name']; ?></a></li>
                        <?php } ?>
                      </ul>
                      <?php } ?>
                    </li>
                    <?php } ?>
                  </ul>
                  <?php } ?>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
    <script type="text/html" id="geo-list">
    <div class="my-geo-main-block">
      <button onclick="$('.my-geo-quest').toggle(200);" class="my-close-geo"><span class="fa fa-times"></span></button>
      <p class="my-geo-main-heading"><?php echo $select_your_city; ?></p>
      <ul class="my-geo-list-ul">
        <li><?php echo $your_city_kiev; ?></li>
        <li><?php echo $your_city_charkov; ?></li>
        <li><?php echo $your_city_lviv; ?></li>
        <li><?php echo $your_city_dnepr; ?></li>
      </ul>
      <label class="my-geo-main-label"><?php echo $select_your_city_other; ?></label>
      <input class="my-geo-main-input" type="text" placeholder="<?php echo $your_city_placeholder; ?>">
      <div class="my-geo-live-block"></div>
      <p class="my-geo-main-bottom-text">
        <?php echo $your_city_info; ?>
      </p>
    </div>
    </script>
    <script>
    $(document).ready(function(){
    $(document).delegate('.my-geo-main-input','keyup', function(e) {
    if ($(this).val().length > 1){
    getCity.getNew($(this).val());
    }
    if(e.keyCode == 40) {
    $('.test-focus:first-child').focus();
    }
    });
    $(document).delegate('.test-focus','keyup', function(e) {
    if(e.keyCode == 40) {
    console.log($('.test-focus').next('a'));
    $('.test-focus').next('.test-focus').focus();
    }
    });
    if($('.my-banner-img').length < 1) {
    $('.my-header-menu',this).hoverIntent({
    sensitivity: 7,
    interval: 200,
    over: function () {
    $('.my-header-drop',this).slideDown(400);
    },
    timeout: 0,
    out: function () {
    $('.my-header-drop', this).slideUp(400);
    }
    });
    }
    })
    </script>
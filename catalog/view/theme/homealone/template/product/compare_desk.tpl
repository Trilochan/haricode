<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>

      <?php if (!empty($products_all)) { ?>

          <div class="tab-content">
            <?php foreach ($products_all as $key => $products) { ?>
              <h3><?=$category_name[$key]?></h3>
              <div class="" >
                  <div class="row">
                    <div class="col-sm-12">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <td colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $text_product; ?></strong></td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><?php echo $text_name; ?></td>
                            <?php foreach ($products as $product) { ?>
                            <td><a href="<?php echo $product['href']; ?>"><strong><?php echo $product['name']; ?></strong></a></td>
                            <?php } ?>
                          </tr>
                          <tr>
                            <td><?php echo $text_image; ?></td>
                            <?php foreach ($products as $product) { ?>
                            <td class="text-center"><?php if ($product['thumb']) { ?>
                              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
                              <?php } ?></td>
                            <?php } ?>
                          </tr>
                  
                        <tr>
                          <td></td>
                          <?php foreach ($products as $product) { ?>
                          <td>
                            <a href="<?= $product['remove']; ?>" class="btn btn-danger btn-block"><?php echo $button_remove; ?></a></td>
                          <?php } ?>
                        </tr>
                      </table>
                      <a href="<?=$url_compare."&tab=".$key; ?>" <?=(count($products) <= 1)? 'disabled="disabled"' : '';?> class="btn btn-success"><?=$button_compare; ?></a>
                    </div>
                  </div>
                </div>
                <hr />
              <?php } ?>
            </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>"  class="btn btn-default"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>

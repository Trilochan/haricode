<div class="" id="mfilter-content-container">
  <div class="titleOption">
    <h2 class="my-cat-name <?= $products ? '' : 'no_filter' ?>"><?php echo $heading_title; ?></h2>
    <div class="my-sort-block">
      <span class="my-sort-text">Выводить : </span>
      <select id="input-sort" class="form-control" onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="productsCategory" id="products">
    <?php foreach ($products as $product) { ?>
    <div class="product-list oneSlid">
      <div class="my-slider-product-block">
        <?php if ($stickers) { ?>
          <?php if ($product['sticker_rating']) { ?>
            <img class="sticker" src="image/catalog/stickers/top.png" style="z-index: <?php echo $product['sticker_rating']['z_index']; ?>">
          <?php } ?>
          <?php if ($product['sticker_new']) { ?>
            <img class="sticker" src="image/catalog/stickers/new.png" style="z-index: <?php echo $product['sticker_new']['z_index']; ?>">
          <?php } ?>
          <?php if ($product['sticker_special']) { ?>
            <img class="sticker" src="image/catalog/stickers/sale.png" style="z-index: <?php echo $product['sticker_special']['z_index']; ?>">
          <?php } ?>
          <?php if ($product['sticker_vip']) { ?>
            <img class="sticker" src="image/catalog/stickers/vip.png" style="z-index: <?php echo $product['sticker_vip']['z_index']; ?>">
          <?php } ?>
        <?php } ?>
        <a href="<?php echo $product['href']; ?>">
          <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="my-slider-product-img"/>
        </a>
        <a href="<?php echo $product['href']; ?>"><p class="my-slider-product-name"><?php echo $product['name']; ?></p></a>
        <div class="my-slider-product-button-block">
        <a class="ratingReview" href="<?php echo $product['href']; ?>">
          <p class="my-catalog-product-ratang">
            <?php for ($i = 1; $i <= 5; $i++) { ?>
            <?php if ($product['rating'] < $i) { ?>
            <span class="my-cat-star-no"></span>
            <?php } else { ?>
            <span class="my-cat-star-yes"></span>
            <?php } ?>
            <?php } ?>
          </p>
          <span class="my-slider-product-review">(<?php echo $product['reviews']; ?>)</span>
          </a>
          <div class="cartDesireCompare">
            <button class="my-slider-wishlist" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span class="fa fa-heart"></span></button>
            <button class="my-slider-compare" onclick="compare.add('<?php echo $product['product_id']; ?>');"><span class="fa fa-balance-scale"></span></button>
          </div>
        </div>
        <div class="my-slider-product-button-block2">
        <?php if ($product['price'] && !$product['is_complect'] && (int)$product['price'] > 0) { ?>
          <?php if (!$product['special']) { ?>
          <p class="my-slider-product-price"><span class=""><?php echo $product['price']; ?></span></p>
          <?php } else { ?>
          <p class="my-slider-product-price">
		  <span class="my-slider-product-nospecial"><?php echo $product['price']; ?></span>
            <span class="my-slider-product-special"><?php echo $product['special']; ?></span>
          </p>
          <?php } ?>
          <button class="my-slider-product-buy" onclick="cart.add('<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?><span class="my-buy-cart"></span></button>
          <?php } else { ?>
          <a href="<?php echo $product['href']; ?>">
            <button class="my-slider-product-buy"><?php echo $text_details; ?> <i class="fa fa-angle-double-right"></i></button>
          </a>
        <?php } ?>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  <div class="row clear" id="pagination">
    <?php if ($next_page) { ?>
    <div class="col-md-12 col-sm-12">
      <div class="col-md-4 col-sm-4">

      </div>
      <div class="next-page col-md-3 col-sm-3">
        <button  id="get_next_page" onclick="pagination.next(<?php echo $next_page; ?>)" class="center-block my-load-pagi"></button>
        <button  id="get_next_page" onclick="pagination.next(<?php echo $next_page; ?>)" class="center-block my-smart-pagi"><?php echo $button_next_page; ?></button>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php echo $pagination; ?>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
</div>

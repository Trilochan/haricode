          <div class="btn-group">
              <button type="button" <?=($compare_counter)? '' : 'disabled="disabled"' ;?> class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <img src="image/vesi-small.png">
                <span class="counter"><?=$compare_counter?></span>&nbsp;&nbsp;
                  <span class="compare-txt"><?= $text_compare_button; ?></span>&nbsp;&nbsp; 
                <span class="caret"></span>
              </button>
            <?php if(count($compare_title) > 0) { ?>
            <ul class="dropdown-menu" role="menu" style="margin: 0;">
            <?php
              foreach ($compare_title as $key => $value) { ?>
                  <li><a href='<?=($value[0] > 1)? $value[1] : $url_compare_desk?>'><?=$key?> : <?=$value[0]?></a></li>
                
              <?php } ?>

            </ul>
            <?php } ?>
          </div>

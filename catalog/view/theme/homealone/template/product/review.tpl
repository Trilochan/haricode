<?php if ($reviews) { ?>
<div class="reviewWrap" style="">
  <?php foreach ($reviews as $review) { ?>
  <div class="reviewBlock">
    <p class="my-review-head-text"><?php echo $review['author']; ?><span><?php echo $review['date_added']; ?></span></p>
    <p class="my-review-count" style="display: block;height: 20px">
      <?php for ($i = 1; $i <= 5; $i++) { ?>
      <?php if ($review['rating'] < $i) { ?>
      <span class="my-cat-star-no"></span>
      <?php } else { ?>
      <span class="my-cat-star-yes"></span>
      <?php } ?>
      <?php } ?>
    </p>
    <p class="my-review-text">
      <?php echo $review['text']; ?>
    </p>
  </div>
  <?php } ?>
  <div class="text-right"><?php echo $pagination; ?></div>
</div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<script type="text/javascript">
$('#form-review').show();
</script>
<?php } ?>

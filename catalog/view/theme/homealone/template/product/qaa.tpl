<?php if ($qaas) { ?>
  <?php foreach ($qaas as $qaa) { ?>
    <table class="table table-striped table-bordered">
      <tr>
        <td style="width: 50%;"><strong><?php echo $qaa['author']; ?></strong></td>
        <td class="text-right"><?php echo $qaa['date_added']; ?></td>
      </tr>
      <tr>
        <td colspan="2"><p><?php echo $qaa['text']; ?></p>
          <table class="table table-striped table-bordered">
            <tr>
              <td style="width: 50%;"><strong><?php echo $text_answer; ?></strong></td>
            </tr>
            <tr>
              <td><p><?php echo $qaa['answer']; ?></p></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  <?php } ?>
  <div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
  <p><?php echo $text_no_qaas; ?></p>
<?php } ?>

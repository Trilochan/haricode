<?php echo $header; ?>
<div class="my-conteiner">
  <ul class="my-breadcrumb">
    <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
      <?php if (count($breadcrumbs) != ($key + 1)) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
      <?php } else { ?>
        <li><a><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
      <?php } ?>
    <?php } ?>
  </ul>
  <div style="padding-top: 40px" class="row">
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if ($products) { ?>
        <?php require_once 'catalog/view/theme/homealone/template/product/product_list.tpl'; ?>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<?php echo $header; ?>
<div style="background: #eee;padding-top: 40px">
  <div class="my-conteiner">
    <ul class="my-breadcrumb hide">
      <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
        <?php if (count($breadcrumbs) != ($key + 1)) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
        <?php } else { ?>
          <li><a><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
        <?php } ?>
      <?php } ?>
    </ul>
  </div>
</div>
  <div class="container">
    <ul class="breadcrumb hide">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>

    <div class="form-group sb_column">
      <div id="content" class="oneColumn">
        <?php echo $content_top; ?>
        <h2 class="sb_head_title"><?php echo $heading_title; ?></h2>
        <?php if ($thumb || $description) { ?>
          <div class="row">
            <?php if ($thumb) { ?>
              <div class="col-sm-2 sb_article_top"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail"/></div>
            <?php } ?>
            <?php if ($description) { ?>
              <div class="col-sm-9 sb_topTitle"><?php echo $description; ?></div>
            <?php } ?>
          </div>
          <hr>
        <?php } ?>

        <?php if ($categories) { ?>
          <h3><?php echo $text_refine; ?></h3>
          <div class="row subcategory-list">
            <?php foreach ($categories as $category) { ?>
              <div class="subcategory text-center">
                <div class="image">
                  <a href="<?php echo $category['href']; ?>"><img class="img-thumbnail center-block" src="<?php echo $category['thumb']; ?>"/></a>
                </div>
                <h4><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
              </div>
            <?php } ?>
          </div>
        <?php } ?>

        <div class="row">
          <?php if ($articles) { ?>
            <?php foreach ($articles as $article) { ?>
              <div class="row sp_oneNews">
                <?php if ($article['image']) { ?>
                  <div class="article-thumbnail-image pull-left">
                    <img src="<?php echo $article['image']; ?>" alt="<?php echo $article['article_title']; ?>"/>
                  </div>
                <?php } ?>

                <div class="article-title">
                  <h2><a class="sb_title" href="<?php echo $article['href']; ?>"><?php echo ucwords($article['article_title']); ?></a></h2>
                </div>

                <div class="date-added"><h4><?php echo $article['date_added']; ?></h4></div>

                <div class="article-description">
                  <?php echo $article['description']; ?>
                </div>

                <a class="sb_more" href="<?php echo $article['href']; ?>"><?php echo $button_continue_reading; ?><i class="fa fa-arrow-circle-o-right"></i></a>
              </div>
            <?php } ?>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left"><?php echo $pagination; ?></div>
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 text-right sb_page_count_info"><?php echo $results; ?></div>
            </div>

          <?php } else { ?>
            <h3 class="text-center"><?php echo $text_no_found; ?></h3>
          <?php } ?>
        </div>

        <?php echo $content_bottom; ?>
      </div>

      <?php echo $column_right; ?>
    </div>
  </div>
<?php echo $footer; ?>
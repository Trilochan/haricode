<?php echo $header; ?>
<div style="background: #eee;padding-top: 40px">
  <div class="my-conteiner">
    <ul class="my-breadcrumb hide">
      <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
        <?php if (count($breadcrumbs) != ($key + 1)) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
        <?php } else { ?>
          <li><a><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
        <?php } ?>
      <?php } ?>
    </ul>
  </div>
</div>
  <div class="my-conteiner" style="padding: 0px 20px 0px 20px">
    <div class="form-group">
      <?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
      <?php } ?>
        <?php echo $content_top; ?>

        <div class="row form-group">
          <?php if (isset($article_info_found)) { ?>
          <div class="article-info">
            <div class="article-title">
              <h2 class="sb_title"><?php echo $article_info['article_title']; ?></h2>
            </div>

            <?php if ($image) { ?>
              <div class="article-thumbnail-image pull-left">
                <img src="<?php echo $image; ?>" alt="<?php echo $article_info['article_title']; ?>"/>
              </div>
            <?php } ?>

            <div class="sb_allText">
              <?php echo html_entity_decode($article_info['description'], ENT_QUOTES, 'UTF-8'); ?>
            </div>

            <?php if ((isset($simple_blog_share_social_site)) && ($simple_blog_share_social_site)) { ?>
              <div class="article-share">
                <!-- ShareThis Button BEGIN -->
                <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
                <script type="text/javascript">stLight.options({publisher: "ur-d825282d-618f-598d-fca6-d67ef9e76731", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
                <span class='st_vkontakte' displayText=''></span>
                <span class='st_facebook' displayText=''></span>
                <span class='st_twitter' displayText=''></span>
                <span class='st_linkedin' displayText=''></span>
                <span class='st_googleplus' displayText=''></span>
                <!-- ShareThis Button END -->
              </div>
              <!--<hr/>-->
            <?php } ?>

            <?php if ($products) { ?>
              <?php require "catalog/view/theme/homealone/template/module/product_slider.tpl"; ?>
            <?php } ?>

            <?php if ($categories) { ?>
              <h3><?php echo $text_categories; ?></h3>
              <div id="categories" class="slick-carousel">
                <?php foreach ($categories as $category) { ?>
                  <div class="item text-center">
                    <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" class="img-responsive center-block"/></a>
                    <h4><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
                  </div>
                <?php } ?>
              </div>
            <?php } ?>

            <?php if ($manufacturers) { ?>
              <h3 class="clear my-product-slider-heading"><?php echo $text_manufacturers; ?></h3>
              <div id="manufacturers<?php echo $module; ?>" class="slick-carousel">
                <?php foreach ($manufacturers as $manufacturer) { ?>
                  <div class="item text-center">
                    <a href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>" class="img-responsive center-block"/></a>
                    <h4><a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a></h4>
                  </div>
                <?php } ?>
              </div>
            <?php } ?>
          </div>

          <?php if (isset($simple_blog_related_articles) && $related_articles) { ?>
            <h3 ><?php echo $text_related_article; ?></h3>
            <div id="related-article" class="slick-carousel">
              <?php foreach ($related_articles as $related_article) { ?>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <h4><a href="<?php echo $related_article['article_href']; ?>"><img src="<?php echo $related_article['image']; ?>" class="img-responsive img-thumbnail center-block"/></a></h4>
                  <div class="text-center">
                    <a href="<?php echo $related_article['article_href']; ?>"><?php echo $related_article['article_title']; ?></a>
                  </div>
                  <p>
                    <?php echo utf8_substr(strip_tags(html_entity_decode($related_article['description'], ENT_QUOTES, 'UTF-8')), 0, 350) . '...'; ?>
                  </p>
                  <p class="text-info">
                    <?php echo $related_article['date_added']; ?><br/>
                    <?php echo $text_comments . $related_article['total_comment']; ?>
                  </p>
                </div>
              <?php } ?>
            </div>
            <hr/>
          <?php } ?>

          <?php if (isset($simple_blog_author_information)) { ?>
            <?php if (isset($author_image)) { ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><?php echo $author_name . " " . $text_author_information; ?></h3>
                </div>

                <div class="panel-body">
                  <div class="author-info">
                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                      <img src="<?php echo $author_image; ?>" alt="<?php echo $article_info['article_title']; ?>" style="border: 1px solid #cccccc; padding: 5px; border-radius: 5px;"/>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
                      <?php echo $author_description; ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          <?php } ?>

          <?php if ($article_info['allow_comment']) { ?>

            <h3 class="panel-title sb_commentTitle"><?php echo $text_related_comment; ?></h3>

            <div id="comments" class="blog-comment-info">
              <form  class="form-horizontal" id="form-comment">
                <div id="comment-list"></div>
                <div id="comment-section"></div>
                <h2 id="review-title">
                  <?php echo $text_write_comment; ?>
                  <i class="fa fa-times del_comment" id="reply-remove" onclick="removeCommentId();"></i>
                </h2>
                <input type="hidden" name="reply_id" value="0" id="blog-reply-id"/>

                <div class="comment-left">

                  <div class="form-group required">
                    <div class="col-sm-6">
                      <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                      <input type="text" name="comment_name" value="" id="input-name" class="form-control"/>
                    </div>
                  </div>
                  <div class="form-group required">
                    <div class="col-sm-6">
                      <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                      <textarea name="comment_text" rows="5" id="input-review" class="form-control"></textarea>

                      <div class="help-block hide"><?php echo $text_note; ?></div>
                    </div>
                  </div>
                  <?php if ($site_key) { ?>
                    <div class="form-group">
                      <div class="col-sm-6">
                        <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                      </div>
                    </div>
                  <?php } ?>
                  <div class="buttons clearfix col-sm-6">
                    <div class="pull-right ">
                      <button type="button" id="button-comment" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary my-product-post-review buttonPadding"><?php echo $button_submit; ?></button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          <?php } ?>
        </div>
      <?php } else { ?>
        <h3 class="text-center"><?php echo $text_no_found; ?></h3>
      <?php } ?>

      <?php echo $content_bottom; ?>
    </div>

    <?php echo $column_right; ?>
  </div>

  <script type="text/javascript">
    function removeCommentId() {
      $("#blog-reply-id").val(0);
      $("#reply-remove").css('display', 'none');
    }
  </script>

  <script type="text/javascript">
    $('#comment-list .pagination a').delegate('click', function () {
      $('#comment-list').fadeOut('slow');

      $('#comment-list').load(this.href);

      $('#comment-list').fadeIn('slow');

      return false;
    });

    $('#comment-list').load('index.php?route=simple_blog/article/comment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>');

  </script>

  <script type="text/javascript">
    $('#button-comment').bind('click', function () {
      $.ajax({
        type: 'POST',
        url: 'index.php?route=simple_blog/article/writeComment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>',
        dataType: 'json',
        data: $("#form-comment").serialize(),
        beforeSend: function () {
          $('#button-comment').button('loading');
        },
        complete: function () {
          $('#button-comment').button('reset');
        },
        success: function (data) {
          $('.alert').remove();

          if (data['error']) {
            $('#review-title').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + data['error'] + '</div>');
          }

          if (data['success']) {
            $('#review-title').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + data['success'] + '</div>');

            $('input[name=\'name\']').val('');
            $('textarea[name=\'text\']').val('');
            $('input[name=\'captcha\']').val('');
            $("#blog-reply-id").val(0);
            $("#reply-remove").css('display', 'none');

            $('#comment-list').load('index.php?route=simple_blog/article/comment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>');
          }
        }
      });
    });

    $('.slick-carousel').slick({
      accessibility: true,
      adaptiveHeight: true,
      arrows: true,
      dots: false,
      draggable: true,
      infinite: true,
      slidesToShow: 6,
      slidesToScroll: 1,
      swipe: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    });
  </script>


<?php echo $footer; ?>
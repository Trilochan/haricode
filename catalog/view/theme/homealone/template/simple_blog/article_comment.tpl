<?php if ($comments) { ?>
  <?php foreach ($comments as $comment) { ?>

    <table class="table table-striped table-bordered">
      <tr>
        <td style="width: 50%;"><strong><?php echo $comment['author']; ?></strong></td>
        <td class="text-right"><?php echo $comment['date_added']; ?></td>
      </tr>
      <tr>
        <td colspan="2"><p><?php echo $comment['comment']; ?></p>
          <a href="<?php echo $page_link; ?>#review-title" class="answer_it"  style="cursor: pointer;"><?php echo $text_reply_comment; ?></a>

          <table class="table table-striped table-bordered">

            <?php if ($comment['comment_reply']) { ?>
              <?php foreach ($comment['comment_reply'] as $comment_reply) { ?>
                <tr>
                  <td style="width: 50%;"><strong><?php echo ucwords($comment_reply['author']); ?></strong></td>
                  <td class="text-right"><?php echo $comment_reply['date_added']; ?></td>
                </tr>
                <tr>
                  <td colspan="2"><p><?php echo $comment_reply['comment']; ?></p></td>
                </tr>
              <?php } ?>
            <?php } ?>
          </table>

        </td>
      </tr>
    </table>
  <?php } ?>

  <div class="form-group">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left"><?php echo $pagination; ?></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right"><?php echo $results; ?></div>
  </div>

<?php } else { ?>
  <h5><?php echo $text_no_blog; ?></h5>
<?php } ?>

<script type="text/javascript">
  function setArticleId(article_id) {
    $("#blog-reply-id").val(article_id);
    $("#reply-remove").css('display', 'inline');
  }
</script>
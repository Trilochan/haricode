<div id="carousel<?php echo $module; ?>" class="slick-carousel homeCarousel clear">
  <?php foreach ($banners as $banner) { ?>
    <div class="item text-center">
      <?php if ($banner['link']) { ?>
        <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive"/></a>
      <?php } else { ?>
        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive"/>
      <?php } ?>
    </div>
  <?php } ?>
</div>
<script type="text/javascript"><!--
  $('#carousel<?php echo $module; ?>').slick({
    accessibility: true,
    adaptiveHeight: true,
    arrows: true,
    dots: false,
    draggable: true,
    swipeToSlide: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    swipe: true,
    responsive: [
      {
        breakpoint: 1427,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 1050,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });
  --></script>
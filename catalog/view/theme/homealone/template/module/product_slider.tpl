<h3 class="my-product-slider-heading">
  <?php
  if (isset($text_related)) {
    echo $text_related;
  } elseif (isset($text_related_product)) {
    echo $text_related_product;
  } else {
    echo $heading_title;
  }
  ?>
</h3>
<?php
//if($route_slider == 'product/product'){

//}
?>
  <div class="my-product-slider-url <?php echo (isset($route_home) && $route_home ? 'newMy' :''); ?>">
  <?php foreach ($products as $product) { ?>
    <div class="oneSlid">
    <div class="my-slider-product-block">
      <?php if ($stickers) { ?>
        <?php if ($product['sticker_rating']) { ?>
          <img class="sticker" src="image/catalog/stickers/top.png" style="z-index: <?php echo $product['sticker_rating']['z_index']; ?>">
        <?php } ?>
        <?php if ($product['sticker_new']) { ?>
          <img class="sticker" src="image/catalog/stickers/new.png" style="z-index: <?php echo $product['sticker_new']['z_index']; ?>">
        <?php } ?>
        <?php if ($product['sticker_special']) { ?>
          <img class="sticker" src="image/catalog/stickers/sale.png" style="z-index: <?php echo $product['sticker_special']['z_index']; ?>">
        <?php } ?>
        <?php if ($product['sticker_vip']) { ?>
          <img class="sticker" src="image/catalog/stickers/vip.png" style="z-index: <?php echo $product['sticker_vip']['z_index']; ?>">
        <?php } ?>
      <?php } ?>
      <a href="<?php echo $product['href']; ?>">
        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="my-slider-product-img"/>
      </a>
      <a href="<?php echo $product['href']; ?>"><p class="my-slider-product-name"><?php echo $product['name']; ?></p></a>
      <div class="my-slider-product-button-block">
        <a class="ratingReview" href="<?php echo $product['href']; ?>">
          <p class="my-catalog-product-ratang">
            <?php for ($i = 1; $i <= 5; $i++) { ?>
            <?php if ($product['rating'] < $i) { ?>
            <span class="my-cat-star-no"></span>
            <?php } else { ?>
            <span class="my-cat-star-yes"></span>
            <?php } ?>
            <?php } ?>
          </p>
          <span class="my-slider-product-review">(<?php echo $product['reviews']; ?>)</span>
        </a>
        <div class="cartDesireCompare">
          <button class="my-slider-wishlist" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span class="fa fa-heart"></span></button>
          <button class="my-slider-compare" onclick="compare.add('<?php echo $product['product_id']; ?>');"><span class="fa fa-balance-scale"></span></button>
        </div>
      </div>

      <div class="my-slider-product-button-block2">
        <?php if ($product['price'] && !$product['is_complect'] && (int)$product['price'] > 0) { ?>
          <?php if (!$product['special']) { ?>
          <p class="my-slider-product-price"><span class=""><?php echo $product['price']; ?></span></p>
          <?php } else { ?>
          <p class="my-slider-product-price">
            <span class="my-slider-product-nospecial"><?php echo $product['price']; ?></span>
            <span class="my-slider-product-special"><?php echo $product['special']; ?></span>
          </p>
          <?php } ?>
          <button class="my-slider-product-buy " onclick="cart.add('<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?><span class="my-buy-cart"></span></button>
        <?php } else { ?>
          <a href="<?php echo $product['href']; ?>">
            <button class="my-slider-product-buy buy_complect"><?php echo $text_details; ?> </button>
          </a>
        <?php } ?>
      </div>

    </div>
    </div>
  <?php } ?>
  </div>

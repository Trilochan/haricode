<div id="banner<?php echo $module; ?>" class="owl-carousel">
  <?php foreach ($banners as $banner) { ?>
  <div class="my-home-special-block">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="" />
    <?php } ?>
  </div>
  <?php } ?>
</div>
<script type="text/javascript"><!--
//$('#banner<?php echo $module; ?>').owlCarousel({
//	items: 6,
//	autoPlay: 3000,
//	singleItem: true,
//	navigation: false,
//	pagination: false,
//	transitionStyle: 'fade'
//});
--></script>

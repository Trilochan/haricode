<div class="my-conteiner clearfix">
<div class="trust">
	<?php foreach ($trusts as $trust) { ?>
	<div style="one_trust">
		<div data-toggle="tooltip" data-placement="bottom" data-html="true" title="<?php echo $trust['trust_text'];?>">
			<img src="<?php echo $trust['image'];?>" alt="" title="<?php //echo $trust['trust_name'];?>">
		</div>
		<div class="text">
			<?php echo $trust['trust_name'];?>
		</div>
	</div>
	<?php } ?>
</div>
</div>
<script>
	  $('.trust').slick({
	    slidesToShow: 7,
		slidesToScroll: 1,
		draggable: true,
		arrows: true,
		swipeToSlide: true,
	  });
</script>
<script src="catalog/view/javascript/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script><link href="catalog/view/javascript/jquery.mCustomScrollbar.css" rel="stylesheet" media="screen"/>


<div class="filter-general-wrapper">
  
  <div class="filter-single-wrapper list-choosen-filters-block">
    <h2 class="title-price-filter">
      <span class="count-products"></span> товаров
    </h2>
      
    <ul id="list-filters" class="list-unstyled">
    
      <?php if($all_values){?>

        <?php foreach($all_values as $key => $values){ ?>
        
          <li class="list-filter-title"><span class="list-filter-title-txt"><?php echo $key; ?>:</span>
            <ul class="list-unstyled">
              <?php foreach($values as $value){?>
                <li class="list-filter-item" data-name="<?php echo $value['filter_type']; ?>" 
                value="<?php echo $value['value']; ?>"><?php echo $value['name'];?>
                <i class="fa fa-times"></i></li>
              
              <?php } ?>
            
            </ul>
            
          </li>
        
        
        <?php } ?>
      
      <?php } ?>
      
      
      
    </ul>
    <div class="panel-footer reset-panel reset-block" style="display:none">
        <button type="button" id="button-filter" class="btn btn-primary reset-btn">Сбросить<i class="fa fa-times"></i></li></button>
      </div>
  </div>
  <script>
    $(document).ready(function () {
      choosen.update();
    });
  </script>


  <!-- FILTER-PRICE -->
  <div class="price-wrapper">
  <div class="title-filters-wrapper">
  </div>
  <div class="price-filter-wrapper">
    <h2 class="title-price-filter title-price">Цена <span class="arrow-open pull-right">﹀</span><span class="arrow-close pull-right">〉</span></h2>
    
  <div class="min-max-wrapper">
    <div id="min-price">
      От
      <input class="<?=($min_price!=0)?'active_filter':''?>" type="text" readonly name="min_price" data-name="min_price" id="price-slider-lower" />
    </div>
    <div id="max-price">
      до
      <input class="<?=($max_price!=0)?'active_filter':''?>" type="text" readonly name="max_price" data-name="max_price" id="price-slider-upper" />
      грн
    </div>    
  </div>
  <div id="price-slider"></div>
  <div class="min-txt"><?php echo (int)$price_limits['min_price']; ?></div>
  <div class="max-txt"><?php echo (int)$price_limits['max_price']; ?></div>
  </div>
</div>
  
  <!-- FILTER-PRICE END -->
  <div class="category-filter">
    
    <?php foreach ($filter_groups as $filter_group) { ?>
   <div class="filter-single-wrapper">
   <h2 class="title-price-filter title-category"><?php echo $filter_group['name']; ?> <span class="arrow-open pull-right">﹀</span><span class="arrow-close pull-right">〉</span></h2>

   
   
     <div class="checkbox scroll-man-wrapper" style="max-height: 274px; height: auto; padding-bottom: 0;">
      <?php foreach ($filter_group['filter'] as $filter) { ?>

        <label for="decoration<?php echo $filter['filter_id']; ?>">
          <input type="checkbox" name="filter[]" data-name="filter" value="<?php echo $filter['filter_id']; ?>" id="decoration<?php echo $filter['filter_id']; ?>"  class="mtfilt <?=(in_array($filter['filter_id'], $filter_category))?' active_filter':''?>"  <?=(in_array($filter['filter_id'], $filter_category))?'checked="checked"':''?>  name="decoration" >
            <span class="checkbox-material">
        <span class="check"> </span>
      </span>
          <div class="title"><?php echo $filter['name']; ?></div>
          
          
          <? if(!empty($filter['childs'])) { ?>
           <span class="first-inner">
            <span class="vertical-line"></span>
            
            <? foreach($filter['childs'] as $child) { ?>
            <label for="decoration-inner<?=$child['filter_id']; ?>">
              <input type="checkbox" name="filter[]" data-name="filter" value="<?php echo $child['filter_id']; ?>"  id="decoration-inner<?=$child['filter_id']; ?>" name="decoration-inner" class="mtfilt <?=(in_array($child['filter_id'], $filter_category))?' active_filter':''?>"  <?=(in_array($child['filter_id'], $filter_category))?'checked="checked"':''?> >
            <span class="checkbox-material">
         <span class="check"> </span>
      </span>
              <div class="title"><?=$child['name']?></div>
            </label>
            <? } ?>           
            </span>          
            <? } ?>
        </label>
    <?php } ?> 
    </div>  
    </div>
    <?php } ?>
</div>
<!-- </div> -->
<!-- SINGLE-FILTER-WRAPPER END -->




  
  <?php if(isset($stock_statuses)){?>
    <div class="filter-single-wrapper availability">
      <h2 class="title-price-filter title-single">Наличие <span class="arrow-open pull-right">﹀</span><span class="arrow-close pull-right">〉</span></h2>
      
      <div class="sample1">

          <?php foreach($stock_statuses as $status){?>
            <div class="radio radio-success">
              <label>
                
                <input type="radio" data-name="stock" value="<?php echo $status['stock_status_id'];?>" name="stock_status_id" <?=($stock_id && $stock_id == $status['stock_status_id'])?'checked="checked" class="active_filter"':''?> >
                  <div class="circle"></div>
                  <span class="check"></span>
                  <div class="title"><?php echo $status['name'];?></div>
              </label>
            </div>
          <?php } ?>
      
      </div>
    </div>
  <?php } ?>
  
  <?php if($manufacturers){?>
    <div class="filter-single-wrapper manufacturers-filter-block">
        <h2 class="title-price-filter title-manuf">Производитель <span class="arrow-open pull-right">﹀</span><span class="arrow-close pull-right">〉</span></h2>
        
        <div class="scroll-man-wrapper">
          <?php foreach($manufacturers as $manuf){?>
            <div class="checkbox">
              <label>
                <input data-name="manuf" class="manfilt <?=(in_array($manuf['manufacturer_id'], $filter_manufacturer))?'active_filter':''?>" type="checkbox" name="manuf[]" value="<?php echo $manuf['manufacturer_id'];?>" <?=(in_array($manuf['manufacturer_id'], $filter_manufacturer))?'checked="checked"':''?> />
                <span class="checkbox-material">
                  <span class="check"> </span>
                </span>
                <div class="title">
                  <?php echo $manuf['name'];?>
                </div>
              </label>
      
            </div>
          <?php } ?>
        </div>
    </div>
  <?php } ?>
</div>

<script type="text/javascript"><!--

  function initSlider() {
    var slider = document.getElementById('price-slider');
    
    if(getParameterByName('min_price')){
      var min_price = getParameterByName('min_price');
    }else{
      var min_price = '<?=($price_limits['min_price'])?$price_limits['min_price']:0; ?>';
    }
    
    
    if(getParameterByName('max_price')){
      var max_price = getParameterByName('max_price');
    }else{
      var max_price = '<?=($price_limits['max_price'])?$price_limits['max_price']:0; ?>';
    }
    
    
    if(max_price != 0 && max_price != min_price){
      var step = 1;
    }else{
      var step = 0;
      slider.setAttribute('disabled', true);
    }
    
  
  
    noUiSlider.create(slider, {
      connect: true,
      behaviour: 'tap',
      step: step,
      start: [min_price, max_price],
      range: {
        'min': <?=($price_limits['min_price'])?$price_limits['min_price']:0; ?>,
        'max': <?=($price_limits['max_price'])?$price_limits['max_price']:0; ?>
      },
      format: wNumb({
      decimals: 0
      })
    });
        
    var lowerValue = document.getElementById('price-slider-lower');
    var upperValue = document.getElementById('price-slider-upper');
        
    slider.noUiSlider.on('update', function( values, handle ) {
          
      lowerValue.value = values[0];
      upperValue.value = values[1];
      lowerValue.classList.add("active_filter");
      upperValue.classList.add("active_filter");
      
      
          
    });
    
    slider.noUiSlider.on('set', function( values, handle ) {
      apply();
      
    });
  }
  
$(function(){
  initSlider();
})
  
  

$(document).on('ready readyAgain', function() {
  
    //#######################################
  $('.mtfilt, .manfilt').unbind('click');
  $('.mtfilt, .manfilt').bind('click', function() {
    if($(this).hasClass('active_filter')){
      $(this).removeClass('active_filter');
    }else{
      $(this).addClass('active_filter');
    }
    apply();

  });
  $('input[name=\'stock_status_id\']').unbind('click');
  $('input[name=\'stock_status_id\']').bind('click', function() {
    
    $('input[name=\'stock_status_id\']').removeClass('active_filter'); // Сбрасываем все radio в начальное состояние
    
    if($(this).hasClass('active_filter')){
      $(this).removeClass('active_filter');
    }else{
      $(this).addClass('active_filter');
    }
    $(this).attr('checked', 'checked');
    apply();
    //#######################################

  });
  
  /********************** BUTTON-RESET **************************/
  var locationWithoutFilters = '<?php echo $link_for_cansel; ?>'.replace(/&amp;/, "&");
  $('#button-filter').on('click', function(bf) {
    window.location = locationWithoutFilters;
  });
  /********************** BUTTON-RESET END **************************/

});

//$(document).trigger('readyAgain');
//--></script> 
<script>

var objT = {};
/************************** CHOOSEN-FILTER-BLOCK *******************************/
var choosen = {};

choosen.listFilters = [];

choosen.update = function () {
  this.listFilters = [];

  $(".active_filter").each(function (index, elem) {
    if ($(elem).attr("data-name") != "min_price" 
      && $(elem).attr("data-name") != "max_price"){
      choosen.listFilters.push($(elem));
    };
  });
  if (this.listFilters.length > 0) {
    if ($(".list-filters, .reset-block").css("display") == "none") {
      $(".list-filters, .reset-block").show();
    };
  }else {
    $(".list-filters, .reset-block").hide();
  };

  $(".count-products").text($("#count_products").val());
  $("#list-filters .list-filter-item").on("click", function () {
    //############################
    if ($(this).attr("data-name") == "stock") {
      $('.filter-single-wrapper input[data-name="stock"]').removeAttr('checked');
      apply();
      return;
    }
    //############################
      var keyAttr = $(this).attr("data-name");
      var valueAttr = $(this).attr("value");
      $('input[data-name="' + keyAttr + '"]')
        .filter('input[value="' + valueAttr + '"]').click();
  });
};
/************************** CHOOSEN-FILTER-BLOCK END *******************************/

/********************* Manufacturers-filter SCROLL *************************/
var manufsOpenInit = function () {
  $(".scroll-man-wrapper").mCustomScrollbar({
    theme:"inset-dark"
  });
  var manHeight = 0;
  $(".manufacturers-filter-block .checkbox").each(function (index, checkElem) {
    manHeight += (parseInt($(checkElem).outerHeight()) + 7);
  });
  manHeight += 15;
  if(manHeight < 274) {
    $(".manufacturers-filter-block").css("height", manHeight + 55 + "px");
  }
  /************** OPEN-CLOSE BLOCKS INITIALIZER ****************/
  $(".title-price").on("click", function () {
      $(".price-wrapper").toggleClass("close-filter");
  });
  $(".title-category").on("click", function () {
      $(".category-filter").toggleClass("close-filter");
  });
  $(".title-single").on("click", function () {
      $(this).closest(".filter-single-wrapper").toggleClass("close-filter");
  });
  $(".title-manuf").on("click", function () {
      $(".manufacturers-filter-block").toggleClass("close-filter");
  });
  /************** OPEN-CLOSE BLOCKS INITIALIZER END ****************/
};
/********************* Manufacturers-filter SCROLL END *************************/

$(document).ready(function () {
  /********* ВЫЗОВ функции choosen.update() **********/
          choosen.update();

          manufsOpenInit();
});

function rebuild() {
    initSlider();
    //initMenu();
    manufsOpenInit();
    /********* ВЫЗОВ функции choosen.update() **********/
          choosen.update();

    $('#list-view').click(function() {
    $('#content .product-layout > .clearfix').remove();

    $('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');

    localStorage.setItem('display', 'list');
  });

  // Product Grid
  $('#grid-view').click(function() {
    $('#content .product-layout > .clearfix').remove();

    // What a shame bootstrap does not take into account dynamically loaded columns
    cols = $('#column-right, #column-left').length;

    /*if (cols == 2) {
      $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
    } else if (cols == 1) {
      $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
    } else {
      $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
    }*/
    $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-3 col-md-4 col-sm-4 col-xs-12');
    

     localStorage.setItem('display', 'grid');
  });

  if (localStorage.getItem('display') == 'list') {
    $('#list-view').trigger('click');
  } else {
    $('#grid-view').trigger('click');
  }
  

}

  function apply() { // Функция собирает все активные элементы фильтра и формирует запрос 
        var smartfilter = '';
        var arr = {};
        $(".active_filter").each(function (i) {
            var key = $(this).attr("data-name");
            var value = $(this).val();

              

              if (arr[key] === undefined) {
                arr[key] = '';
                arr[key] += value;
              } else {
                arr[key] += ',' + value;
              }
        
        });
        //############################
        var active_stock = false;
        $('.filter-single-wrapper input[data-name="stock"]').map(function(){
            if($(this).attr('checked') == 'checked') active_stock = true;
        });

        $.each(arr, function (index, val) {
          if((index == 'min_price') && (val == <?php echo (int)$price_limits['min_price']; ?>)) ;
          else if((index == 'max_price') && (val == <?php echo (int)$price_limits['max_price']; ?>)) ;
          else if((index == 'stock') && (!active_stock)) ;
            else smartfilter += index + '=' + val + '&';
        });
        //############################
        smartfilter = smartfilter.substr(0, smartfilter.length - 1);
    
        setUrl(smartfilter); // Отправляем GET запрос
    }
   function setUrl(smartfilter) { 
      //############################
      contentOpacity(true);
      //############################ 
      if(smartfilter != ''){  
        href = '<?php echo $action; ?>&'+smartfilter; // Собираем новый URL 
        
      }else{
        href = '<?php echo $action;?>';
      }
      
      pageurl = href.replace(/&amp;/, "&");

      $.ajax({ // Отправляем запрос к контроллеру для фильтрации
        url: pageurl,
        type: 'GET',
        success: function(data,textStatus,  jqxhr){
          
          newlink = jqxhr.getResponseHeader('X-link');
        
          pageurl = newlink;
      
          if(pageurl != window.location){
            window.history.pushState({path:pageurl},'',pageurl);  
          }
          
          htmls = $.parseHTML(data);
          
         
          $('.mainColumn').html($(htmls).find('.mainColumn').html());
            
          $('.leftColumn').html($(htmls).find('.leftColumn').html());
          
          $('.my-breadcrumb').html($(htmls).find('.my-breadcrumb').html());
            
          document.title=$(htmls).find('#categ_header').text();
          
          $('.seo').html($(htmls).filter('.seo').html()); 
          
          $(document).trigger('readyAgain');
          
          
          rebuild();  
          //############################
          // запрос на деактивацию неподходящих фильтров
          setActiveFilsets(); 
          contentOpacity(false);        
          //############################
        }       
      });
      
      
      
      
      
    }
    
    
    function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }



      //##############################################
    $(document).ready(function(){
      setActiveFilsets();
    });


    function setActiveFilsets()
    {
      // сбор URL для GET запроса
      var smartfilter = getActiveFilters();
      var url = '<?php echo $action; ?>&'+smartfilter+'&activefilters=get';
      // запрос на номера активных фильтров
      $.getJSON(url,
            function(data){
              if(data != 'false'){
                // деактивация всех фильтров кроме min_price и max_price
                $(".mtfilt, .manfilt, .sample1 input").map(function() {
                    $(this).parent().css('opacity', '0.4');
                    $(this).attr('disabled', 'disabled');
                });

                // активация соответствующих фильтров
                  $.each(data[0], function (key, value) {
                  $('.mtfilt').parent().find('input[value="' + value + '"]').parent().css('opacity', '1');
                  $('.mtfilt').parent().find('input[value="' + value + '"]').removeAttr('disabled');
                  });
                  $.each(data[1], function (key, value) {
                  $('.manfilt').parent().find('input[value="' + value + '"]').parent().css('opacity', '1');
                  $('.manfilt').parent().find('input[value="' + value + '"]').removeAttr('disabled');
                  });
                  $.each(data[2], function (key, value) {
                  $('.sample1').parent().find('input[value="' + value + '"]').parent().css('opacity', '1');
                  $('.sample1').parent().find('input[value="' + value + '"]').removeAttr('disabled');
                  });
            }
        });
    }


    function getActiveFilters()
    {
      var smartfilter = '';
          var arr = {};
          $(".active_filter").each(function (i) {
              var key = $(this).attr("data-name");
              var value = $(this).val();
          if (arr[key] === undefined) {
            arr[key] = '';
            arr[key] += value;
          } else {
            arr[key] += ',' + value;
          }       
          });
          $.each(arr, function (index, val) {
            if((index == 'min_price') && (val == <?php echo (int)$price_limits['min_price']; ?>)) ;
            else if((index == 'max_price') && (val == <?php echo (int)$price_limits['max_price']; ?>)) ;
              else smartfilter += index + '=' + val + '&';
          });
          return smartfilter.substr(0, smartfilter.length - 1);
    }


    function contentOpacity(view)
    {
      if(view) $('.my-conteiner').parent().css('opacity', '0.4');
      else $('.my-conteiner').parent().css('opacity', '1');
    }
    //###############################################

 </script>
 
 

<div class="my-conteiner">
  <div class="my-home-banner-block-block">
    <button class="banner-batton-left"><span class="fa fa-chevron-left"></span></button>
    <button class="banner-batton-right"><span class="fa fa-chevron-right"></span></button>
    <div id="slideshow<?php echo $module; ?>" class="my-home-banner-block" style="opacity: 1;">
      <?php foreach ($banners as $banner) { ?>
      <div class="item" style="position: relative;">
        <?php if ($banner['link']) { ?>
        <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="my-banner-img" /></a>
        <?php } else { ?>
        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="my-banner-img" />
        <?php } ?>
        <div class="my-banner-bottom"></div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
$('#slideshow<?php echo $module; ?>').slick({
  accessibility: true,
  adaptiveHeight: true,
  autoplay: true,
  arrows: true,
  dots: false,
  draggable: true,
  infinite: true,
  pauseOnHover: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  swipe: true,
  prevArrow: $ ('.banner-batton-left'),
  nextArrow: $ ('.banner-batton-right'),
});
--></script>
  <span data-toggle="modal" data-target="#trust-article" class="fa fa-times my-close-reg"></span>
  <h3 class="modal-reg-heading"><?php echo $title; ?></h3>
<div class="modal-body">
  <?php echo $description; ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_close; ?></button>
</div>
<?php if ($articles) { ?>
<h3 class="my-home-news-heading"><?php echo $heading_title; ?></h3>
<a class="allNews hoverRose" href="<?php echo $article_link; ?>"><?php echo $text_all_news; ?></a>
<?php foreach ($articles as $article) { ?>
<?php @$i++; ?>
<div class="my-home-news-block">
    <div <?php if($i > 1) { echo 'class="my-home-news-two"'; } else { echo 'class="my-home-news-main"'; } ?>>
      <a href="<?php echo $article['href']; ?>"><h3><?php echo $article['article_title']; ?></h3></a>
      <p>
        <?php echo $article['description']; ?>
      </p>
      <a class="more hoverRose" href="<?php echo $article['href']; ?>"><?php echo $text_detail; ?></a>
      <span class="my-home-news-date"><?php echo $article['date_added']; ?></span>
    </div>
</div>
<?php } ?>
<?php } ?>

<script>
  $('.simple-blog-carousel').slick({
    accessibility: true,
    adaptiveHeight: true,
    arrows: true,
    dots: false,
    draggable: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    swipe: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
</script>
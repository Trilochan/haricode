<h3><?php echo $heading_title; ?></h3>
<div class="row">
    <?php foreach ($categories as $category) { ?>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
            <div class="image">
                <img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive center-block"/>
            </div>
            <div class="category-name"> <?php echo $category['name']; ?></div>
            <div class="bottom">
                <a href="<?php echo $category['href']; ?>" class="details">Подробнее</a>
            </div>
        </div>
    <?php } ?>
</div>

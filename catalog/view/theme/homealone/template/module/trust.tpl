<?php foreach ($trusts as $trust) { ?>
<div class="delivery guaranty">
  <?php if (isset($trust['article_href'])) { ?>
    <p style="cursor: pointer; cursor: hand;" class="my-product-delivery-heading" onclick="trust.show(<?php echo $trust['related_article']; ?>);return false;"><?php echo $trust['trust_name'];?></p>
  <?php } else { ?>
    <p class="my-product-delivery-heading"><?php echo $trust['trust_name'];?></p>
  <?php } ?>
  <p class="my-product-delivery-text"><span>&gt;</span><?php echo $trust['trust_text1'];?></p>
  <p class="my-product-delivery-text"><span>&gt;</span><?php echo $trust['trust_text2'];?></p>
  <p class="my-product-delivery-text"><span>&gt;</span><?php echo $trust['trust_text3'];?></p>
</div>
<?php } ?>

<div id="trust-article" class="modal fade" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    </div>
  </div>
</div>

<script type="text/javascript">
var trust = {
  'show': function(articleId) {
            $('#trust-article .modal-content').load('index.php?route=module/trust/article&id=' + articleId, function() {});

            $('#trust-article').modal('show');
          }
  }
</script>
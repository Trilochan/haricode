<?php echo $header; ?>
<div style="background: #eee;padding-top: 40px">
<div class="my-conteiner">
  <div class="row oneColumn">
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <img style="display: block;margin: auto" src="catalog/view/theme/homealone/image/not_found.gif">
      <!--<div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>-->
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<?php echo $footer; ?>
<script>
var siteUrl = getUrlParam('route');
if(!(siteUrl === 'product/product' || siteUrl === 'product/category')){
	$('.my-product-slider-url').slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      draggable: true,
      arrows: true,
      responsive: [
        {
          breakpoint: 1360,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          }
        },
      ]
    });
}
</script>
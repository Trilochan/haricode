
<?php if (!isset($redirect)) { ?>
  <table class="table my-table">
    <thead>
    <tr class="my-cart-table-head">
      <td class="text-left"><?php echo $column_name; ?></td>
      <td class="text-center">Цена за ед.</td>
      <td class="text-center"><?php echo $column_quantity; ?></td>
      <td class="text-center"><?php echo $column_price; ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($products as $product) { ?>
      <tr>
        <td class="text-left for-dashed">

          <div class="caption">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="my-cart-product-image"/>
            <span class="my-cart-product-model">Код: <?php echo $product['model']; ?></span>
            <p class="my-cart-product-name"><?php echo $product['name']; ?></p>
            <?php if($product['option']) { ?>
            <?php foreach($option as $options) { ?>
            <p><span><?php echo $options['name']; ?></span><?php echo $options['value']; ?></p>
            <?php } ?>
            <?php } ?>

          </div>
        </td>
        <td style="vertical-align: inherit" class="text-center for-dashed"><?php echo $product['price']; ?></td>
        <td style="vertical-align: inherit" class="text-center number for-dashed">
          <i class="fa fa-minus" id="minus" onclick="cart.quantity.minus('<?php echo $product['key']; ?>', $(this).parent().find('input').val()); reload();"></i>
          <input type="text" size="3" value="<?php echo $product['quantity']; ?>" onchange="cart.quantity.update('<?php echo $product['key']; ?>', $(this).val()); reload();"/>
          <i class="fa fa-plus" id="plus" onclick="cart.quantity.plus('<?php echo $product['key']; ?>', $(this).parent().find('input').val()); reload();"></i>
        </td>
        <td style="vertical-align: inherit" class="text-center product-price for-dashed">
          <?php echo $product['total']; ?>
          <i class="fa fa-times" onclick="cart.remove('<?php echo $product['key']; ?>'); reload();"></i>
        </td>
      </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <?php foreach ($totals as $total) { ?>
      <tr>
        <td colspan="3" class="text-left bottom-table-text"><?php echo $total['title']; ?>:</td>
        <td style="text-align: center;" class="text-right bottom-table-text"><?php echo $total['text']; ?></td>
      </tr>
    <?php } ?>
    </tfoot>
  </table>


<!--<?php if (!$payment) { ?>
  <a href="/" class="pull-left proceed">Продолжить покупки</a>
  <input type="button" value="<?php echo $button_save; ?>" id="button-save" data-loading-text="<?php echo $text_loading; ?>" class="pull-right button-buy"/>
<?php } ?>-->
  <?php echo $payment; ?>
<?php } else { ?>
  <script type="text/javascript"><!--
    location = '<?php echo $redirect; ?>';
    //--></script>
<?php } ?>

<div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="errorLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="errorLabel"><?php echo $text_validate_error_header; ?></h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>



<?php if ($error_warning) { ?>
    <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
    <?php foreach ($payment_methods as $payment_method) { ?>
            <label class="my-delivery-label">
                <?php if ($payment_method['code'] == $code) { ?>
                    <?php $code = $payment_method['code']; ?>
                    <input class="my-delivery-input" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked"/>
                    <?php if(isset($payment_method['thumb'])) { ?>
                        <img src="<?php echo $payment_method['thumb']; ?>" />
                    <?php } ?>
                <?php } else { ?>
                    <input class="my-delivery-input" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>"/>
                    <?php if(isset($payment_method['thumb'])) { ?>
                        <img src="<?php echo $payment_method['thumb']; ?>" />
                    <?php } ?>
                <?php } ?>
                <span class="my-delivery-span">
                    <?php echo $payment_method['title']; ?>
                    <?php if ($payment_method['terms']) { ?>
                        (<?php echo $payment_method['terms']; ?>)
                    <?php } ?>
                </span>
            </label>
    <?php } ?>
<?php } ?>
<?php if ($text_agree) { ?>
    <div class="buttons">
        <div class="pull-right"><?php echo $text_agree; ?>
            <?php if ($agree) { ?>
                <input type="checkbox" name="agree" value="1" checked="checked"/>
            <?php } else { ?>
                <input type="checkbox" name="agree" value="1"/>
            <?php } ?>
        </div>
    </div>
<?php } ?>

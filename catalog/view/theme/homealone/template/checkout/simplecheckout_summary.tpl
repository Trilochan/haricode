<div class="simplecheckout-block" id="simplecheckout_summary">
    <?php if ($display_header) { ?>
    <div class="checkout-heading panel-heading"><?php echo $text_summary ?></div>
    <?php } ?>
    <table class="simplecheckout-cart">
        <colgroup>
        <col class="image">
        <col class="name">
        <col class="model">
        <col class="price">
        <col class="quantity">
        <col class="total">
        </colgroup>
        <thead>
            <tr>
                <th class="image hide"><?php echo $column_image; ?></th>
                <th class="name"><?php echo $column_name; ?></th>
                <th class="model hide"><?php echo $column_model; ?></th>
                <th class="price"><?php echo $column_price; ?></th>
                <th class="quantity"><?php echo $column_quantity; ?></th>
                <th class="total"><?php echo $column_total; ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
                <td class="image hide">
                    
                </td>
                <td class="name">
                    <div class="opt">
                        <?php if ($product['thumb']) { ?>
                        <a class="pr_img" href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                        <?php } ?>
                        <a class="product_name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        <div class="options">
                            <?php foreach ($product['option'] as $option) { ?>
                            <span class="one_opt"> <?php echo $option['name']; ?>: <?php echo $option['value']; ?></span><br />
                            <?php } ?>
                        </div>
                        <?php if ($product['reward']) { ?>
                            <small><?php echo $product['reward']; ?></small>
                        <?php } ?>
                    </div>
                </td>
                <td class="model hide"><?php echo $product['model']; ?></td>
                <td class="price"><?php echo $product['price']; ?></td>
                <td class="quantity"><?php echo $product['quantity']; ?></td>
                <td class="total"><?php echo $product['total']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($vouchers as $voucher_info) { ?>
            <tr>
                <td class="image"></td>
                <td class="name"><?php echo $voucher_info['description']; ?></td>
                <td class="model"></td>
                <td class="price"><?php echo $voucher_info['amount']; ?></td>
                <td class="quantity">1</td>
                <td class="total"><?php echo $voucher_info['amount']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($totals as $total) { ?>
            <tr class="totalInfo">
                <!-- <div class="simplecheckout-cart-total" id="total_<?php echo $total['code']; ?>"> -->
                <td class="title" colspan="3"><span><?php echo $total['title']; ?>:</span></td>
                <td class="price"><span class="simplecheckout-cart-total-value"><?php echo $total['text']; ?></span></td>
                </div>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    
    <?php if ($summary_comment) { ?>
    <table class="simplecheckout-cart simplecheckout-summary-info">
        <thead>
            <tr>
                <th class="name"><?php echo $text_summary_comment; ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $summary_comment; ?></td>
            </tr>
        </tbody>
    </table>
    <?php } ?>
    <?php if ($summary_payment_address || $summary_shipping_address) { ?>
    <table class="simplecheckout-cart simplecheckout-summary-info">
        <thead>
            <tr>
                <th class="name"><?php echo $text_summary_payment_address; ?></th>
                <?php if ($summary_shipping_address) { ?>
                <th class="name"><?php echo $text_summary_shipping_address; ?></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $summary_payment_address; ?></td>
                <?php if ($summary_shipping_address) { ?>
                <td><?php echo $summary_shipping_address; ?></td>
                <?php } ?>
            </tr>
        </tbody>
    </table>
    <?php } ?>
</div>
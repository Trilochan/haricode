<form class="form-horizontal">
  <?php if ($addresses) { ?>
    <div class="radio">
      <label>
        <input type="radio" name="shipping_address" value="existing" checked="checked"/>
        <?php echo $text_address_existing; ?></label>
    </div>
    <div class="clearfix"></div>
    <div id="shipping-existing">
      <select name="address_id" class="form-control">
        <?php foreach ($addresses as $address) { ?>
          <?php if ($address['address_id'] == $address_id) { ?>
            <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>
              , <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
          <?php } else { ?>
            <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>
              , <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
          <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="radio">
      <label>
        <input type="radio" name="shipping_address" value="new"/>
        <?php echo $text_address_new; ?></label>
    </div>
  <?php } ?>

  <div class="clearfix"></div>

  <div id="shipping-new" style="display: <?php echo($addresses ? 'none' : 'block'); ?>;margin-top: 15px;">
    <div class="form-group required">
      <div class="col-sm-8 col-sm-offset-2">
        <label class="my-cart-label col-md-3" for="input-shipping-firstname"><?php echo $entry_firstname; ?>:</label>
        <input type="text" name="firstname" value="<?php echo $firstname; ?>"  id="input-shipping-firstname" class="my-cart-input col-md-7"/>
      </div>
    </div>
    <div class="form-group required hidden">
      <label class="control-label col-sm-12" for="input-shipping-lastname"><?php echo $entry_lastname; ?></label>

      <div class="col-sm-12">
        <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-shipping-lastname" class="form-control"/>
      </div>
    </div>
    <div class="form-group required hidden">
      <label class="control-label" for="input-shipping-country"><?php echo $entry_country; ?></label>
      <div class="col-sm-12">
        <select name="country_id" id="input-shipping-country" class="form-control">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($countries as $country) { ?>
            <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
            <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
            <?php } ?>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="form-group required hidden">
      <label class="control-label" for="input-shipping-zone"><?php echo $entry_zone; ?></label>

      <div class="col-sm-12">
        <select name="zone_id" id="input-shipping-zone" class="form-control">
        </select>
      </div>
    </div>
    <div class="form-group required">
      <div class="col-sm-8 col-sm-offset-2">
        <label class="my-cart-label col-md-3" for="input-shipping-city"><?php echo $entry_city; ?></label>
        <input type="text" name="city" value="<?php echo $city; ?>" placeholder="" id="input-shipping-city" class="my-cart-input col-md-7"/>
      </div>
    </div>
    <div class="form-group required hidden">
      <label class="control-label" for="input-shipping-postcode"><?php echo $entry_postcode; ?></label>

      <div class="col-sm-12">
        <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-shipping-postcode" class="form-control"/>
      </div>
    </div>
    <div class="form-group required hidden">
      <label class="control-label" for="input-shipping-address-1"><?php echo $entry_address_1; ?></label>

      <div class="col-sm-12">
        <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-shipping-address-1" class="form-control"/>
      </div>
    </div>

    <div class="form-group hidden">
      <label class="control-label" for="input-shipping-company"><?php echo $entry_company; ?></label>

      <div class="col-sm-12">
        <input type="text" name="company" value="" placeholder="<?php echo $entry_company; ?>" id="input-shipping-company" class="form-control"/>
      </div>
    </div>
    <div class="form-group hidden">
      <label class="control-label" for="input-shipping-address-2"><?php echo $entry_address_2; ?></label>

      <div class="col-sm-12">
        <input type="text" name="address_2" value="" placeholder="<?php echo $entry_address_2; ?>" id="input-shipping-address-2" class="form-control"/>
      </div>
    </div>

    <?php foreach ($custom_fields as $custom_field) { ?>
      <?php if ($custom_field['location'] == 'address') { ?>
        <?php if ($custom_field['type'] == 'select') { ?>
          <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
            <label class="control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>

            <div class="col-sm-12">
              <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                  <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        <?php } ?>
        <?php if ($custom_field['type'] == 'radio') { ?>
          <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
            <label class="control-label"><?php echo $custom_field['name']; ?></label>

            <div class="col-sm-12">
              <div id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>">
                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                  <div class="radio">
                    <label>
                      <input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                      <?php echo $custom_field_value['name']; ?></label>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php } ?>
        <?php if ($custom_field['type'] == 'checkbox') { ?>
          <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
            <label class="control-label"><?php echo $custom_field['name']; ?></label>

            <div class="col-sm-12">
              <div id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>">
                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                      <?php echo $custom_field_value['name']; ?></label>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php } ?>
        <?php if ($custom_field['type'] == 'text') { ?>
          <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
            <label class="control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>

            <div class="col-sm-12">
              <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                     value="<?php echo isset($shipping_address_custom_field[$custom_field['custom_field_id']]) && $shipping_address_custom_field[$custom_field['custom_field_id']] != "" ? $shipping_address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>"
                     id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"/>
            </div>
          </div>
        <?php } ?>
        <?php if ($custom_field['type'] == 'textarea') { ?>
          <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
            <label class="control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>

            <div class="col-sm-12">
              <textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                        class="form-control"><?php echo isset($shipping_address_custom_field[$custom_field['custom_field_id']]) && $shipping_address_custom_field[$custom_field['custom_field_id']] != "" ? $shipping_address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']; ?></textarea>
            </div>
          </div>
        <?php } ?>
        <?php if ($custom_field['type'] == 'file') { ?>
          <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
            <label class="control-label"><?php echo $custom_field['name']; ?></label>

            <div class="col-sm-12">
              <button type="button" id="button-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"/>
            </div>
          </div>
        <?php } ?>
        <?php if ($custom_field['type'] == 'date') { ?>
          <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
            <label class="control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>

            <div class="col-sm-12">
              <div class="input-group date">
                <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"/>
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
            </div>
          </div>
        <?php } ?>
        <?php if ($custom_field['type'] == 'time') { ?>
          <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
            <label class="control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>

            <div class="col-sm-12">
              <div class="input-group time">
                <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"/>
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
            </div>
          </div>
        <?php } ?>
        <?php if ($custom_field['type'] == 'datetime') { ?>
          <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
            <label class="control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>

            <div class="col-sm-12">
              <div class="input-group datetime">
                <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"/>
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
            </div>
          </div>
        <?php } ?>
      <?php } ?>
    <?php } ?>
  </div>
</form>
<script type="text/javascript">

  $(function () {

    if(localStorage.getItem('geo') !== null){
      $('#input-shipping-city').val(localStorage.getItem('geo'));
    }

    <?php if($shipping_code['code'] != 'novaposhta.novaposhta') { ?>
    $('#input-shipping-custom-field2').val('false');
    $('#input-shipping-custom-field2').parent().parent().addClass('hidden');
    <?php } else { ?>
    $('#input-shipping-custom-field2').parent().parent().removeClass('hidden');

    var city = $('input#input-shipping-city');
    var street = $('input#input-shipping-custom-field2');

    var city_id = $(city).attr("id");
    var city_class = $(city).attr("class");
    var city_name = $(city).attr("name");
    var city_val = $(city).val();

    var street_id = $(street).attr("id");
    var street_class = $(street).attr("class");
    var street_name = $(street).attr("name");
    var street_val = $(street).val();

    $(city).replaceWith('<select id="' + city_id + '" class="' + city_class + '" name="' + city_name + '"><option value=""> --- Выберите --- </option></select>');
    $(street).replaceWith('<select id="' + street_id + '" class="' + street_class + '" name="' + street_name + '"><option value=""> --- Выберите --- </option></select>');

    var city = $('select#input-shipping-city');
    var street = $('select#input-shipping-custom-field2');

    $(city).attr("disabled", "true");
    $(street).attr("disabled", "true");

    $.ajax({
      url: 'index.php?route=checkout/simplifiedcheckout/getNovaPoshtaCities',
      dataType: 'json',
      type: "GET",
      cache: true,
      success: function (json_city) {
        $.map(json_city.data, function (item_city) {
          var selected = "";
          if (city_val === item_city.DescriptionRu) {
            selected = "selected";
          }
          $(city).append('<option value="' + item_city.DescriptionRu + '" data-ref="' + item_city.Ref + '" ' + selected + ' >' + item_city.DescriptionRu + '</option>');
        });
        $(city).removeAttr("disabled");
        if (city_val !== "") {
          var ref = $(city).find(':selected').attr('data-ref');
          $.ajax({
            url: 'index.php?route=checkout/simplifiedcheckout/getNovaPoshtaWarehouses&city_ref=' + ref,
            dataType: 'json',
            type: "GET",
            success: function (json_warehouse) {
              $(street).html('<option value=""> --- Выберите --- </option>');
              $.map(json_warehouse.data, function (item_warehouse) {
                var selected = "";
                console.log(street_val);
                console.log(item_warehouse.DescriptionRu);
                if (street_val === item_warehouse.DescriptionRu) {
                  selected = "selected";
                }
                $(street).append('<option data-ref="' + item_warehouse.Ref + '" value=\'' + item_warehouse.DescriptionRu + '\' ' + selected + ' >' + item_warehouse.DescriptionRu + '</option>');
              });
              $(street).removeAttr("disabled");
            }
          });
        }
      }
    });

    $(city).change(function (obj) {
      $(street).attr("disabled", "true");
      var value = $(this).val();
      if (value.length !== 0) {
        var ref = $(this).find(':selected').attr('data-ref');

        $.ajax({
          url: 'index.php?route=checkout/simplifiedcheckout/getNovaPoshtaWarehouses&city_ref=' + ref,
          dataType: 'json',
          type: "GET",
          success: function (json_warehouse) {
            $(street).html('<option value=""> --- Выберите --- </option>');
            $.map(json_warehouse.data, function (item_warehouse) {
              $(street).append('<option data-ref="' + item_warehouse.Ref + '" value=\'' + item_warehouse.DescriptionRu + '\'>' + item_warehouse.DescriptionRu + '</option>');
            });
            $(street).removeAttr("disabled");
          }
        });
      }
    });
    <?php }?>
  });


  $('input[name=\'shipping_address\']').on('change', function () {
    if (this.value == 'new') {
      $('#shipping-existing').hide();
      $('#shipping-new').show();
      $('#checkout_confirm').load('index.php?route=checkout/simplifiedconfirm');
    } else {
      $('#shipping-existing').show();
      $('#shipping-new').hide();
      $('#checkout_confirm').load('index.php?route=checkout/simplifiedconfirm&payment_instruction');
    }
  });

  $('#shipping_address .form-group[data-sort]').detach().each(function () {
    if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#shipping_address .form-group').length) {
      $('#shipping_address .form-group').eq($(this).attr('data-sort')).before(this);
    }

    if ($(this).attr('data-sort') > $('#shipping_address .form-group').length) {
      $('#shipping_address .form-group:last').after(this);
    }

    if ($(this).attr('data-sort') < -$('#shipping_address .form-group').length) {
      $('#shipping_address .form-group:first').before(this);
    }
  });

  $('#shipping_address button[id^=\'button-shipping-custom-field\']').on('click', function () {
    var node = this;

    $('#form-upload').remove();

    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

    $('#form-upload input[name=\'file\']').trigger('click');

    timer = setInterval(function () {
      if ($('#form-upload input[name=\'file\']').val() != '') {
        clearInterval(timer);

        $.ajax({
          url: 'index.php?route=tool/upload',
          type: 'post',
          dataType: 'json',
          data: new FormData($('#form-upload')[0]),
          cache: false,
          contentType: false,
          processData: false,
          beforeSend: function () {
            $(node).button('loading');
          },
          complete: function () {
            $(node).button('reset');
          },
          success: function (json) {
            $(node).parent().find('.text-danger').remove();

            if (json['error']) {
              $(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
            }

            if (json['success']) {
              alert(json['success']);

              $(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    }, 500);
  });

  $('.date').datetimepicker({
    pickTime: false
  });

  $('.time').datetimepicker({
    pickDate: false
  });

  $('.datetime').datetimepicker({
    pickDate: true,
    pickTime: true
  });

  $('#shipping_address select[name=\'country_id\']').on('change', function () {
    if (this.value) {
      $.ajax({
        url: 'index.php?route=checkout/simplifiedcheckout/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function () {
          $('#shipping_address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
        },
        complete: function () {
          $('.fa-spin').remove();
        },
        success: function (json) {
          $('.fa-spin').remove();

          if (json['postcode_required'] == '1') {
            $('#shipping_address input[name=\'postcode\']').parent().parent().addClass('required');
          } else {
            $('#shipping_address input[name=\'postcode\']').parent().parent().removeClass('required');
          }

          html = '<option value=""><?php echo $text_select; ?></option>';

          if (json['zone'] != '') {
            for (i = 0; i < json['zone'].length; i++) {
              html += '<option value="' + json['zone'][i]['zone_id'] + '"';

              if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                html += ' selected="selected"';
              }

              html += '>' + json['zone'][i]['name'] + '</option>';
            }
          } else {
            html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
          }

          $('#shipping_address select[name=\'zone_id\']').html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  });

  $('#shipping_address select[name=\'country_id\']').trigger('change');
</script>
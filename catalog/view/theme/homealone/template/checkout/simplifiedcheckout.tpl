<?php echo $header; ?>
  <div style="float: left;width: 100%;background: #eee">
  <div class="my-conteiner checkout-page">
    <ul class="my-breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
    <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
    <?php } ?>
      <div style="background: #fff;margin-top: 20px;width: 100%;float: left" id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="col-sm-10 col-sm-offset-1 checkout-products">
          <h1 class="my-cart-heading"><span></span><?php echo $heading_title; ?></h1>

          <div class="table-responsive" id="checkout_confirm"></div>
        </div>
        <!-- <h1><?php echo $heading_title; ?></h1> -->
        <div class="contact-form col-sm-10 col-sm-offset-1">
          <h3 class="my-cart-contact-heading"><span></span><?php echo $text_checkout_shipping_address; ?></h3>

          <div id="shipping_address"></div>
        </div>
        <div class="col-sm-10 col-sm-offset-1 checkout-delivery">
          <div class="delivery col-sm-6">
            <h3 class="my-cart-deliv-heading"><span></span><?php echo $text_checkout_shipping_method; ?><a href="index.php?route=information/information&information_id=7">подробнее</a></h3>
            <div class="variations" id="shipping_methods"></div>
          </div>
          <div class="payment col-sm-6">
            <h3 class="my-cart-pay-heading"><span></span><?php echo $text_checkout_payment_method; ?><a href="index.php?route=information/information&information_id=8">подробнее</a></h3>

            <div class="variations" id="payment_methods"></div>
          </div>
          <div class="col-sm-12">
            <div style="border-bottom: solid 1px #ccc"></div>
          </div>
        </div>
        <?php echo $content_bottom; ?>
        <div id="my-checkout_confirm">
          <input type="button" value="Оформить заказ" id="button-save" data-loading-text="" class="button-buy"/>
        </div>
      </div>
      <?php echo $column_right; ?>

  </div>

</div>
<script type="text/javascript">
  $(document).delegate('#button-save', 'click', function () {
    $.ajax({
      url: 'index.php?route=checkout/simplifiedshipping_address/save',
      type: 'post',
      data: $('#shipping_address input[type=\'text\'], #shipping_address input[type=\'date\'], #shipping_address input[type=\'datetime-local\'], #shipping_address input[type=\'time\'], #shipping_address input[type=\'password\'], #shipping_address input[type=\'checkbox\']:checked, #shipping_address input[type=\'radio\']:checked, #shipping_address textarea, #shipping_address select'),
      dataType: 'json',
      beforeSend: function () {
        $('#button-save').button('loading');
      },
      complete: function () {
        $('#button-save').button('reset');
      },
      success: function (json) {
        console.log(json);
        $('.alert, .text-danger').remove();
        $('div.has-error').removeClass('has-error');

        if (json['error']) {
          if (json['error']['warning']) {
            $('#shipping_address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          }

          for (i in json['error']) {
            var element = $('#input-shipping-' + i.replace('_', '-'));

            if ($(element).parent().hasClass('input-group')) {
              $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
            } else {
              $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
            }
          }

          // Highlight any found errors
          $('.text-danger').parent().parent().addClass('has-error');

          if (json['error'] || !json['payment'] || !json['shipping']) {
            var html = '<p class="text-danger">';
            if (json['error']) {
              for (i in json['error']) {
                html += json['error'][i] + "<br />";
              }
            }
            if (!json['payment']['status']) {
              html += json['payment']['message'] + "<br />";
            }
            if (!json['shipping']['status']) {
              html += json['shipping']['message'] + "<br />";
            }
            html += '</p>';
            $('.modal#error .modal-body').html(html);
            $('.modal#error').modal('show');
          }
        }
        else {
          $.ajax({
            type: 'get',
            url: 'index.php?route=payment/cod/confirm',
            cache: false,
            beforeSend: function() {
              $('#button-confirm').button('loading');
            },
            complete: function() {
              $('#button-confirm').button('reset');
            },
            success: function() {
              location = 'http://localhost/one/index.php?route=checkout/success';
            }
          });
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });
</script>
  <script type="text/javascript">
    $(function () {
      reload();

      $('#shipping_methods input[type=\'radio\']').each(function (i, obj) {
        if ($(obj).prop('checked')) {
          $(obj).trigger('click');
        }
      });
    });

    $(document).delegate('#payment_methods input[type=\'radio\']', 'click', function () {
      $.ajax({
        url: 'index.php?route=checkout/simplifiedpayment_method/save',
        type: 'post',
        data: $('#payment_methods input[type=\'radio\']:checked'),
        dataType: 'json',
        success: function (json) {
          $('.alert, .text-danger').remove();

          if (json['redirect']) {
            location = json['redirect'];
          } else if (json['error']) {
            if (json['error']['warning']) {
              $('#payment_methods').parent().prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
          } else {
            $.ajax({
              url: 'index.php?route=checkout/simplifiedpayment_method',
              dataType: 'html',
              success: function (html) {
                reload();
              },
              error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
              }
            });
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    });

    $(document).delegate('#shipping_methods input[type=\'radio\']', 'click', function () {
      $.ajax({
        url: 'index.php?route=checkout/simplifiedshipping_method/save',
        type: 'post',
        data: $('#shipping_methods input[type=\'radio\']:checked'),
        dataType: 'json',
        success: function (json) {
          $('.alert, .text-danger').remove();

          if (json['error']) {
            if (json['error']['warning']) {
              $('#shipping_methods').parent().prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
          } else {
            reload();
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    });

    $(document).delegate('#shipping_address input, #shipping_address select', 'focusout', function () {
      $.ajax({
        url: 'index.php?route=checkout/simplifiedshipping_address/save',
        type: 'post',
        data: $('#shipping_address form').serialize(),
        dataType: 'json',
        success: function (json) {
          $('.alert, .text-danger').remove();

          if (json['error']) {
            var errors = "";
            for (var i in json['error']) {
              errors += json['error'][i] + "<br />";
            }
            $('#shipping_address').prepend('<div class="alert alert-danger clearfix"><p class="pull-left">' + errors + '</p><button type="button" class="close pull-right" data-dismiss="alert">&times;</button></div>');
          } else {

            if($('#shipping_methods input[type=\'radio\']:checked').length) {
              $('#shipping_methods input[type=\'radio\']').each(function (i, obj) {
                if ($(obj).prop('checked')) {
                  $(obj).trigger('click');
                }
              });
            } else {
              $('#shipping_methods').load('index.php?route=checkout/simplifiedshipping_method', function() {
                $('#checkout_confirm').load('index.php?route=checkout/simplifiedconfirm');});
            }
          }
        }
      });
    });

    function reload() {
      $('#payment_methods').load('index.php?route=checkout/simplifiedpayment_method');
      $('#shipping_methods').load('index.php?route=checkout/simplifiedshipping_method', function() {
        $('#checkout_confirm').load('index.php?route=checkout/simplifiedconfirm');});
      $('#shipping_address').load('index.php?route=checkout/simplifiedshipping_address');
    }

  </script>
<?php echo $footer; ?>
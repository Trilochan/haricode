<?php if ($error_warning) { ?>
    <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>
    <?php foreach ($shipping_methods as $shipping_method) { ?>
        <?php if (!$shipping_method['error']) { ?>
            <?php foreach ($shipping_method['quote'] as $quote) { ?>
                <label class="my-delivery-label">
                    <?php if ($quote['code'] == $code) { ?>
                        <?php $code = $quote['code']; ?>
                        <input class="my-delivery-input" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" checked="checked"/>
                        <?php if (isset($quote['thumb'])) { ?>
                            <img src="<?php echo $quote['thumb']; ?>"/>
                        <?php } ?>
                    <?php } else { ?>
                        <input class="my-delivery-input" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"/>
                        <?php if (isset($quote['thumb'])) { ?>
                            <img src="<?php echo $quote['thumb']; ?>"/>
                        <?php } ?>
                    <?php } ?>
                    <span class="my-delivery-span"><?php echo $quote['title']; ?></span>
                </label>
            <?php } ?>
        <?php } else { ?>
            <div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
        <?php } ?>
    <?php } ?>
<?php } ?>
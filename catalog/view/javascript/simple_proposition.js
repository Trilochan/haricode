var simpleProposition = {
    minus:function(param){
        var $el = $(param).next();

        var key = $el.attr('data-key');
        var count = $el.attr('value');

        if(count > 1) {

            $.ajax({
                url: 'index.php?route=checkout/proposition/edit',
                type: 'post',
                data: 'key=' + key + '&quantity=' + (parseInt(count) - 1),
                dataType: 'json',
                beforeSend: function () {
                    $('#cart > button').button('loading');
                },
                complete: function () {
                    $('#cart > button').button('reset');
                },
                success: function () {
                    $el.attr('value', parseInt(count) - 1);
                    $('#cart > .my-new-modal-block > .modal-dialog').load('index.php?route=common/modal_cart/info #cart > .my-new-modal-block > .modal-dialog >*', function (data) {
                        $('.my-header-cart').html($(data).find('.my-header-cart'));
                        reloadAll();
                    });
                }
            });
        }
    },

    plus:function(param){
        var $el = $(param).prev();

        var key = $el.attr('data-key');
        var count = $el.attr('value');

        if(count < 999) {
            $.ajax({
                url: 'index.php?route=checkout/proposition/edit',
                type: 'post',
                data: 'key=' + key + '&quantity=' + (parseInt(count) + 1),
                dataType: 'json',
                beforeSend: function () {
                    $('#cart > button').button('loading');
                },
                complete: function () {
                    $('#cart > button').button('reset');
                },
                success: function () {
                    $el.attr('value',parseInt(count) + 1);

                    $('#cart > .my-new-modal-block > .modal-dialog').load('index.php?route=common/modal_cart/info #cart > .my-new-modal-block > .modal-dialog >*', function (data) {
                        $('.my-header-cart').html($(data).find('.my-header-cart'));
                        reloadAll();
                    });
                }
            });
        }
    },
    update: function(quantity, key) {
        console.log(key);
        console.log(quantity);

        $.ajax({
            url: 'index.php?route=checkout/proposition/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + parseInt(quantity),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function () {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                $('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function () {
                    initMasks();
                    reloadAll();
                });
            }
        });
    },
    remove: function (key) {
        $.ajax({
            url: 'index.php?route=checkout/proposition/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                $('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function () {
                    initMasks();
                    reloadAll();
                });
            }
        });
    }
}
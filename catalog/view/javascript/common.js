function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

$(document).ready(function() {
  // show or hide password chars in regiter and login form
  $("button.showpassword").click(function () {
    var $input = $(this.previousElementSibling);
    var change = "";
    if ($input.data( "showpassword" ) == undefined || $input.data( "showpassword" ) === "false") {
      $input.data( "showpassword", "true" );
      change = "text";
      showpassword = "true";
      $(this).find('span').first().removeClass("fa-eye-slash").addClass("fa-eye");
    } else {
      $input.data( "showpassword", "false" );
      change = "password";
      showpassword = "false";
      $($(this).find('span')[0]).removeClass("fa-eye").addClass("fa-eye-slash");
    }
    var rep = $("<input type='" + change + "' />")
      .attr("id", $input.attr("id"))
      .attr("name", $input.attr("name"))
      .attr('class', $input.attr('class'))
      .attr('placeholder', $input.attr('placeholder'))
      .val($input.val())
      .data( "showpassword", showpassword)
      .insertBefore($input);
    $input.remove();
    $input = rep;
  });

	// Adding the clear Fix
	cols1 = $('#column-right, #column-left').length;

	//if (cols1 == 2) {
	//	$('#content .product-layout:nth-child(2n+2)').after('<div class="clearfix visible-md visible-sm"></div>');
	//} else if (cols1 == 1) {
	//	$('#content .product-layout:nth-child(3n+3)').after('<div class="clearfix visible-lg"></div>');
	//} else {
	//	$('#content .product-layout:nth-child(4n+4)').after('<div class="clearfix"></div>');
	//}
	
	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();
		
		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});
		
	// Currency
	$('#currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

		$('#currency').submit();
	});

	// Language
	$('.my-language-block #language a').on('click', function(e) {
		e.preventDefault();

		console.log($(this));

		$('#language input[name=\'code\']').attr('value', $(this).attr('href'));

		$('#language').submit();
	});

	/* Search */
	$('#search input[name=\'search\']').parent().find('button').on('click', function() {
		url = $('base').attr('href') + 'index.php?route=product/search';

		var value = $('header input[name=\'search\']').val();

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
	});

	$('#search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('header input[name=\'search\']').parent().find('button').trigger('click');
		}
	});

	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// dropDown effect menu
	var timerId;
	$('#menu li.dropdown').mouseenter(function () {
		var element = $(this);
		if (element.is(':hover')) {
			timerId = setTimeout(function () {
				element.find('.dropdown-menu').slideDown('50');
			}, 500);
		}
	}).mouseleave(function () {
		clearTimeout(timerId)
		$(this).find('.dropdown-menu').slideUp('50');
	});

	// Product List
	$('#list-view').click(function() {
		$('#content .product-layout > .clearfix').remove();

		//$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
		$('#content .row > .product-layout').attr('class', 'product-layout product-list col-xs-12');
		
		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		$('#content .product-layout > .clearfix').remove();

		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		 localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});

	telephoneMask();
});

var telephoneMask = function() {
	$('input[name=\'telephone\']').mask('+38 (999) 999-99-99');
}

// Cart add remove functions
var cart = {
	'add': function(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$.smkAlert({
						text: json['success'],
						type: 'success'
					});

					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
					}, 100);

					$('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function() {telephoneMask();});

					$('#cart > #cart_modal').modal('show');

					if (typeof reloadAll !== 'undefined') {
						reloadAll();
					}
				}
			}
		});
	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},			
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
				}, 100);

				$('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function() {telephoneMask();});

				$('#cart > #cart_modal').modal('show');

				$('input[name="quantity[' + key +']"]').val(Number(quantity));

				if (typeof reloadAll !== 'undefined') {
					reloadAll();
				}
			}
		});
	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},			
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
				}, 100);

				$('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function() {telephoneMask();});

				$('#cart > #cart_modal').modal('show');

				if (typeof reloadAll !== 'undefined') {
					reloadAll();
				}
			}
		});
	},
	'quantity': {
		'plus': function(key, quantity) {
			$.ajax({
				url: 'index.php?route=checkout/cart/update',
				type: 'post',
				data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? Number(quantity) + 1 : 1),
				dataType: 'json',
				beforeSend: function() {
					$('#cart > button').button('loading');
				},
				complete: function() {
					$('#cart > button').button('reset');
				},
				success: function(json) {
					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
					}, 100);

					$('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function() {telephoneMask();});

					$('#cart > #cart_modal').modal('show');

					$('input[name="quantity[' + key +']"]').val(Number(quantity) + 1);

					if (typeof reloadAll !== 'undefined') {
						reloadAll();
					}
				}
			});
		},
		'minus': function(key, quantity) {
			$.ajax({
				url: 'index.php?route=checkout/cart/update',
				type: 'post',
				data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? Number(quantity) - 1 : 1),
				dataType: 'json',
				beforeSend: function() {
					$('#cart > button').button('loading');
				},
				complete: function() {
					$('#cart > button').button('reset');
				},
				success: function(json) {
					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
					}, 100);

					$('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function() {telephoneMask();});

					$('#cart > #cart_modal').modal('show');

					$('input[name="quantity[' + key +']"]').val(Number(quantity) - 1);

					if (typeof reloadAll !== 'undefined') {
						reloadAll();
					}
				}
			});
		},
		'update': function(key, quantity) {
			$.ajax({
				url: 'index.php?route=checkout/cart/update',
				type: 'post',
				data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' || quantity > 1 ? Number(quantity) : 1),
				dataType: 'json',
				beforeSend: function() {
					$('#cart > button').button('loading');
				},
				complete: function() {
					$('#cart > button').button('reset');
				},
				success: function(json) {
					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
					}, 100);

					$('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function() {telephoneMask();});

					$('#cart > #cart_modal').modal('show');

					$('input[name="quantity[' + key +']"]').val(Number(quantity));

					if (typeof reloadAll !== 'undefined') {
						reloadAll();
					}
				}
			});
		}
	},
	'validateInteger': function (evt) {
	    var theEvent = evt || window.event;
	    var key = theEvent.keyCode || theEvent.which;
	    key = String.fromCharCode( key );
	    var regex = /[0-9]/;
	    if (theEvent.keyCode == 13) {
	    	return;
	    }
	    if( !regex.test(key)) {
	        theEvent.returnValue = false;
	        if(theEvent.preventDefault) theEvent.preventDefault();
	    }
	}
}

var fast_order = {
	'add': function(form) {
		$.ajax({
			url: 'index.php?route=module/fast_order',
			type: 'post',
			data: $(form).serialize(),
			dataType: 'json',
			success: function(json) {
				console.log($(form).serialize());
				console.log(json);
				var errors = $(form).find('p.text-danger');
				errors.addClass('hidden');
				errors.html("");
				if(json.status) {
					$('#cart_modal .modal-body').html("");
					$('#cart_modal .modal-footer').html("");
					$('#cart_modal .modal-header h4').remove();
					$('#cart_modal .modal-body').html("<h4>" + json.msg + "</h4>");
					$('#cart-total').html(json.total);
				}
				else {
					errors.removeClass('hidden');
					var message = "";
					for(var key in json.msg) {
						message += json.msg[key] + "<br />";
					}
					errors.html(message);
				}
			}
		});
	}
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$.smkAlert({
						text: json['success'],
						type: 'success'
					});
				}

				if (json['info']) {
					$.smkAlert({
						text: json['info'],
						type: 'info'
					});

					if(json['login_register']) {
						$('#my-login-modal').modal('show');
					}
				}

				$('#wishlist-total span').html(json['total']);
				$('#wishlist-total').attr('title', json['total']);
				$('#wishlist-total-count').html("(" + json['count'] + ")");
			}
		});
	},
	'remove': function() {

	},
    'info': function() {
        $.ajax({
            url: 'index.php?route=account/wishlist/info',
            type: 'post',
            dataType: 'json',
            success: function(json) {
                if (!(json['logged'])) {
                    $('#my-login-modal').modal('show');
                } else if (json['redirect'] && json['logged']) {
                    location = json['redirect'];
                }
            }
        });
    }
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$.smkAlert({
						text: json['success'],
						type: 'success'
					});

					$('#compare-total').html(json['total']);
					$('#compare-total-count').html("(" + json['count'] + ")");
				}
			}
		});
	},
	'remove': function() {

	}
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();
	
			$.extend(this, option);
	
			$(this).attr('autocomplete', 'off');
			
			// Focus
			$(this).on('focus', function() {
				this.request();
			});
			
			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);				
			});
			
			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}				
			});
			
			// Click
			this.click = function(event) {
				event.preventDefault();
	
				value = $(event.target).parent().attr('data-value');
	
				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}
			
			// Show
			this.show = function() {
				var pos = $(this).position();
	
				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});
	
				$(this).siblings('ul.dropdown-menu').show();
			}
			
			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}		
			
			// Request
			this.request = function() {
				clearTimeout(this.timer);
		
				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}
			
			// Response
			this.response = function(json) {
				html = '';
	
				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}
	
					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}
	
					// Get all the ones with a categories
					var category = new Array();
	
					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}
	
							category[json[i]['category']]['item'].push(json[i]);
						}
					}
	
					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
	
						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}
	
				if (html) {
					this.show();
				} else {
					this.hide();
				}
	
				$(this).siblings('ul.dropdown-menu').html(html);
			}
			
			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));	
			
		});
	}
})(window.jQuery);

          $(document).ready(function(){
             // получаем адрес контролера сравнения
              compare_url = $('.compare a').attr('href');
              $('.compare').removeAttr('data-original-title');

            $('button').map(function(){
              // всем button которые имеют событие onclick.compare добавить класс compare_prod и атрибут data-id = id-товара
              if($(this).attr('onclick') != undefined){
                attr = $(this).attr('onclick').substr(0, 7);
                if(attr == 'compare') {
                  var id = $(this).attr('onclick').substr(13).substring(0, $(this).attr('onclick').substr(13).indexOf("');"));
                  $(this).attr('data-id', id);
                  $(this).addClass("compare_prod");
                }
              }
            })

            compare_top();
            // при нажатии на снопку сравнить обновляем данные сравнения на странице
            $('.compare_prod').click(function(){
                  $(this).addClass("btn-warning");
                  setTimeout(function(){
                    compare_top();
                  }, 500);
            });

            
          });

          // функция обновления данных сравнения на странице
          function compare_top(){

            // запрос обновляющий классы для кнопок сравнения согласно присуцтвию товаров в списке сравнения
            $.post(compare_url + "/getStringId" , { string : 'string', }, function(data) {
              str_compare_id = data;
              $('.compare_prod').map(function(){
                  attr = $(this).attr('data-id') + "');";
                  if(str_compare_id[0].indexOf(attr) != -1) {
                    var id = $(this).attr('data-id');
                    var pos = str_compare_id[1].indexOf(id + "=");
                    if(pos != -1){
                        $(this).removeClass("btn-warning");
                        $(this).addClass("btn-success");
                        $(this).attr('data-original-title','Перейти к сравнению');
                        $(this).click(function(){
                          var id = $(this).attr('data-id');
                          var pos = str_compare_id[1].indexOf(id + "=");
                          var tab = str_compare_id[1].substr(pos);
                          pos = tab.indexOf("=");
                          tab = tab.substr(pos + 1);
                          pos = tab.indexOf('"');
                          window.location.href = compare_url + '&tab=' + tab.substr(0, pos);
                        });
                    }
                    else{
                        $(this).addClass("btn-warning");
                        $(this).removeAttr('onclick');
                        $(this).attr('data-original-title','Уже в сравнении');
                    }
                  }
              }); 
            });

            // запрос обновляющий верхнюю кнопку сравнения товаров.
            $.post(compare_url + "/getTitle" , { string : 'string', }, function(data) {
              $('.compare').html(data);  
              $('.compare .btn-group').mouseenter(function(){$('.compare .dropdown-menu').slideDown(0);}).mouseleave(function(){$('.compare .dropdown-menu').slideUp(0);})
              $('.compare button').click(function(){
                window.location.href = compare_url + "&desk=desk";
              })
            });
          }
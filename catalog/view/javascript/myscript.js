$(document).ready(function() {
    // Sizing modal dark background to body heights
    $("#cart_modal").change(function() {
        $('.modal-backdrop').css('height', $(window).height());
    });

    $(".my-slider-product-buy").click(function() {
        $('.modal-backdrop').css('height', $(window).height());
    });

    $('#cart_modal').on('loaded.bs.modal', function() {
        $('.modal-backdrop').css('height', $(window).height());
    });

    //Активация цвета Бордер на доп. картинке продукт.тпл
    $('.my-product-small-img img', this).hover(function() {
        var el = $('.my-product-small-img img', this).context.currentSrc;
        // var data = $(this).data("mfp-src");
        // console.log(data);
        $('.my-product-block-img img.my-product-main-img').css('opacity', '0');
        $('.my-product-small-img img').css('border', 'transparent 2px solid');
        $(this).css('border', '#DDD 2px solid');

        $('.my-product-block-img img.my-product-main-img').prop('src', el);
        $('.my-product-block-img img.my-product-main-img').animate({
            opacity: '1',
        }, 600)
    });
    //GEO LOCATION
    ymaps.ready(function() {

        if (localStorage.getItem('geo') !== null) {
            $('#this-my-geo').text(localStorage.getItem('geo'));
        } else {
            $('.my-time-geo').text(ymaps.geolocation.city + ' ?');
            $('.my-geo-quest').show(200);

            var template = _.template($('#geo-list').html());

            $('.my-geo-yes').click(function() {
                localStorage.setItem('geo', ymaps.geolocation.city);
                setCookie('geo_city', ymaps.geolocation.city);
                document.cookie = "user_geo=" + $(this).text();
                $('#this-my-geo').text(ymaps.geolocation.city);
                $('.my-geo-quest').hide(200);
            });
            $('.my-geo-no').click(function() {
                $('.my-geo-quest').html(template);
            });
        }
    });
    $(document).delegate('.my-geo-list-ul li', 'click', function() {
        localStorage.setItem('geo', $(this).text());
        setCookie('geo_city', $(this).text());
        document.cookie = "user_geo=" + $(this).text();
        $('#this-my-geo').text(localStorage.getItem('geo'));
        $('.my-geo-quest').hide(200);
    })

    $(document).delegate('.my-geo-live-list', 'click', function() {
        localStorage.setItem('geo', $(this).text());
        setCookie('geo_city', $(this).text());
        document.cookie = "user_geo=" + $(this).text();
        $('#this-my-geo').text(localStorage.getItem('geo'));
        $('.my-geo-quest').hide(200);
    })

    //$(document).delegate('#my-geo-city-button','click',function(){
    //    var template = _.template($('#geo-list').html());
    //    $('.my-geo-quest').html(template);
    //    $('.my-geo-quest').show(200);
    //})

    //====================================================================
})

function showGeo() {
    var template = _.template($('#geo-list').html());
    $('.my-geo-quest').html(template);
    $('.my-geo-quest').toggle(200);
}

function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (30 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

$(document).click(function(event) {
    if (!$(event.target).closest('.my-geo-quest').length &&
        !$(event.target).closest('.my-header-geo').length &&
        !$(event.target).closest('.my-geo-no').length) {
        if ($(".my-geo-quest").css("display") != 'none') {
            $('.my-geo-quest').hide(200);
        }
    }
});

function getUrlParam(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function mySlideToggle(select) {
    $(select).slideToggle(400);
}

function myCloseModal(select) {
    $(select).hide(400);
}

function loadSlick(selector) {
    setTimeout(function() {
        $(selector).slick({
            slidesToShow: 8,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 1300,
                settings: {
                    slidesToShow: 7,
                    slidesToScroll: 1,
                }
            }, {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1,
                }
            }, ]
        });
    }, 400)
}

var sellProd = {
    get: function(limit, select, name, temp, category) {
        $.ajax({
            url: 'index.php?route=product/category/getLastProduct',
            type: 'post',
            data: 'limit=' + limit + '&category=' + category,
            dataType: 'json',
            success: function(json) {
                var category_id = sellProd.getUrl('path');
                // console.log(json);
                // console.log(json.sell);
                var template = _.template($(temp).html());
                $(select).append(template({
                    main: json,
                    name: name,
                    categoryId: category_id,
                }));
            }
        })
    },

    getUrl: function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },

    reviewSort: function(count, array) {
        var totalReting = _.pluck(array, 'rating');

        var totalRetingInt = totalReting.map(function(x) {
            return parseInt(x, 10);
        });

        var sum = _.reduce(totalRetingInt, function(memo, num) {
            return (memo + num);
        }, 0) / count;

        var totalSum = Math.round(sum);

        return totalSum;
    }
};

var getCity = {
    get: function(valName) {
        var searchField = valName;
        var myExp = new RegExp(searchField, "i");
        $.ajax({
            url: 'catalog/view/javascript/test.json',
            type: 'get',
            data: valName,

            success: function(data) {
                $('.my-geo-live-block li').remove();
                try {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].title.search(myExp) != -1) {
                            $('.for-geo-remove').remove();
                            $('.my-geo-live-block').append(
                                '<li class="my-geo-live-list"><span class="my-geo-only-region">' + data[i].region + '</span><span class="my-geo-only-city">' + data[i].title + '</span></li>');
                        }
                    }
                } catch (e) {
                    $('.for-geo-remove').remove();
                    $('.my-geo-live-block').append('<p class="my-geo-live-list for-geo-remove">Города с таким именем не найденно</p>');
                }
            }
        })
    },

    close: function() {
        $('.my-geo-quest').hide(200);
    },

    getNew: function(name) {
        $.ajax({
            url: "https://api.novaposhta.ua/v2.0/json/",
            data: {
                "modelName": "Address",
                "calledMethod": "getCities",
                "methodProperties": {
                    "FindByString": name
                },
                "apiKey": "35ac8749097d8f62d404581a596a7849"
            },
            dataType: 'jsonp',
            success: function(json) {
                $('.my-geo-live-block li').remove();
                for (var i = 0; i < json.data.length; i++) {
                    $('.for-geo-remove').remove();
                    $('.my-geo-live-block').append(
                        '<li class="my-geo-live-list">' + json.data[i].DescriptionRu + '</li>');
                }
                if ($('.my-geo-live-block li').length <= 0) {
                    $('.for-geo-remove').remove();
                    $('.my-geo-live-block').after('<p class="my-geo-live-list for-geo-remove">Города с таким именем не найденно</p>');
                }
            }
        });
    }

}

fastOrderProduct = {
    add: function(product_id, telephone, quantity) {
        $.ajax({
            url: 'index.php?route=module/fast_order/addFastOrder',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1) + '&telephone=' + $('#fast-phone').val(),
            dataType: 'json',
            success: function(json) {
                //console.log(json);
            }
        })
    }
}

var myJump = {
    jump: function(h) {
        var top = $(h).offset();
        window.scrollTo(0, top.top);
        $(h).click();
    }
}

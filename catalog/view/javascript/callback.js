var callback = {
    'submit': function(form) {
        var ajax_data = {
            name: $("#callback-main input[name='name']").val(),
            telephone: $("#callback-main input[name='telephone']").val(),
            lecationhref: window.location.href
        }
        form['lecationhref'] = window.location.href;
        $.ajax({
            url: 'index.php?route=module/callback',
            type: 'post',
            data: ajax_data,
            dataType: 'json',
            success: function(json) {
                var errors = $(form).find('p.text-danger');
                errors.addClass('hidden');
                errors.html("");
                if(json.status) {
                    $('.text-success').html('Спасибо что выбрали наш магазин! С вами свяжется менеджер в течении 30 минут.');
                    setTimeout(function(){
                        $('#my-callback-modal').modal('hide');
                        $('.text-success').html(' ');
                    },5000)


                }
                else {
                    errors.removeClass('hidden');
                    var message = "";
                    for(var key in json.msg) {
                        message += json.msg[key] + "<br />";
                    }
                    errors.html(message);
                }
            }
        });
    }
}
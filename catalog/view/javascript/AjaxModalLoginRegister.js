var ajaxLogin = {
    'submit': function(form) {
        $.ajax({
            url: 'index.php?route=account/ajax_login',
            type: 'post',
            data: $(form).serialize(),
            dataType: 'json',
            success: function(json) {
                console.log(json);
                var errors = $(form).find('p.text-danger');
                errors.addClass('hidden');
                errors.html("");
                if(json.status) {
                    location.reload();
                }
                else {
                    errors.removeClass('hidden');
                    errors.html(json.msg.warning);
                }
            }
        });
    }
}

var ajaxRegister = {
    'submit': function(form) {
        $.ajax({
            url: 'index.php?route=account/ajax_register',
            type: 'post',
            data: $(form).serialize(),
            dataType: 'json',
            success: function(json) {
                console.log(json);
                var errors = $('.text-danger-reg');
                errors.addClass('hidden');
                errors.html("");
                if(json.status) {
                    $('#my-login-modal').modal('hide');
                    $('#register_success').modal('show');
                    setTimeout(function () {
                        window.location.reload();
                    }, 400);
                }
                else {
                    errors.removeClass('hidden');
                    var message = "";
                    for(var key in json.msg) {
                        message += json.msg[key] + "<br />";
                    }
                    console.log(errors);
                    errors.html(message);
                }
            }
        });
    }
}

var ajaxConfirm = {
    'submit': function(form) {
        $.ajax({
            url: 'index.php?route=account/ajax_register/confirm',
            type: 'post',
            data: $(form).serialize(),
            dataType: 'json',
            success: function(json) {
                var errors = $(form).find('p.text-danger');
                errors.addClass('hidden');
                errors.html("");
                if(json.status) {
                    $('#register_confirm').modal('hide');
                    $('#register_confirm_success').modal('show');
                }
                else {
                    errors.removeClass('hidden');
                    var message = "";
                    for(var key in json.msg) {
                        message += json.msg[key] + "<br />";
                    }
                    errors.html(message);
                }
            }
        });
    }
}

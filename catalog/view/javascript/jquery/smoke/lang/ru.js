(function($){
    $.fn.smkValidate.Languaje = {
        ru: {
            textEmpty        : 'Обязательное поле',
            textEmail        : 'Не правльно введен E-mail',
            textAlphanumeric : 'Только цифры и/или буквы',
            textNumber       : 'Только цифры',
            textNumberRange  : 'The numerical range must be greater than <b> {@} </b> and less than <b> {@} </b>',
            textDecimal      : 'Только десятичные числа',
            textCurrency     : 'Не верно введена денежная сумма',
            textSelect       : 'Обязательное поле',
            textCheckbox     : 'Обязательное поле',
            textLength       : 'The number of characters is equal to <b> {@} </b>',
            textRange        : 'The number of characters must be greater than <b> {@} </b> and less than <b> {@} </b>',
            textSPassDefault : 'Минимум 4 символа',
            textSPassWeak    : 'Минимум 6 символов',
            textSPassMedium  : 'Минимум 6 символов и число',
            textSPassStrong  : 'Минимум 6 символов, число и заглавная буква'
        }
    };

    $.smkEqualPass.Languaje = {
        ru: {
            // Mensaje de error para el input repassword
            textEqualPass    : 'Пароли не совпадают'
        }
    };

    $.smkDate.Languaje = {
        ru: {
            shortMonthNames : ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            monthNames : ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
        }
    };

}(jQuery));
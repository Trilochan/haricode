var container = ''; //определяется в функции getContainer
var pages = [];

var pagination = {
    'next': function (page) {
        $('#pagination a').each(function () {
            href = $(this).attr('href');
            if (jQuery.inArray(href, pages) == -1) {
                pages.push(href);
            }
        });

        console.log(pages);

        if (page - 1 > pages.length) {
            return;
        }

        w = parseFloat($('#content').css('width'));
        $(container).parent().append('<div class="ajaxblock" style="width:' + w + 'px;height:30px;margin-top:20px;text-align:center;border:none !important;"><img src="image/loader.gif" /></div>');

        $.ajax({
            url: pages[page - 2],
            type: "GET",
            data: '',
            success: function (data) {
                $data = $(data);
                $('.ajaxblock').remove();
                if ($data) {
                    if ($data.find('.product-list').length > 0) {
                        $(container).parent().append($data.find('.product-list').parent().html());
                    } else if ($data.find('.product-grid').length > 0) {
                        $(container).parent().append($data.find('.product-grid').parent().html());
                    }

                    $("#pagination").html($data.find("#pagination").html());
                }

                if (localStorage.getItem('display') == 'list') {
                    $('#list-view').trigger('click');
                } else {
                    $('#grid-view').trigger('click');
                }
            }
        });
    }
};

function getContainer() {
    if ($('.product-list').length > 0) {
        container = '.product-list';
    } else if ($('.product-grid').length > 0) {
        container = '.product-grid';
    } else {
        container = '';
    }
    return container;
}

$(document).ready(function () {
    container = getContainer();
});

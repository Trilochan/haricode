//propositions remove and update functions
var proposition = {
    options: function (id) {
        $('#proposition_product_options_' + id).modal('show');
    },
    add: function (proposition_id) {
        var proposition = {};
        proposition['proposition_id'] = proposition_id;
        proposition['products'] = {};

        $('#proposition_' + proposition_id + ' [data-product-id]').each(function (index, element) {
            proposition['products'][index] = {};
            var product_id = $(element).attr('data-product-id');

            var options = $('#proposition_product_options_' + product_id + ' input[type=\'text\'], #proposition_product_options_' + product_id + ' input[type=\'hidden\'], #proposition_product_options_' + product_id + ' input[type=\'radio\']:checked, #proposition_product_options_' + product_id + ' input[type=\'checkbox\']:checked, #proposition_product_options_' + product_id + ' select, #proposition_product_options_' + product_id + ' textarea').serialize();
            proposition['products'][index]['product_id'] = product_id;
            proposition['products'][index]['options'] = options;
        });

        $.ajax({
            url: 'index.php?route=checkout/proposition/add',
            type: 'post',
            data: proposition,
            dataType: 'json',
            success: function (json) {
                $('.proposition-options .form-group').removeClass('has-error');
                $('.alert, .text-danger').remove();

                if (json['error']) {
                    if (json['error']['option']) {

                        $.smkAlert({
                            text: json['error']['info'],
                            type: 'info',
                            permanent: true,
                        });

                        for (var product_id in json['error']['option']) {
                            var product_name = $('#proposition_' + proposition_id + ' [data-product-id=' + product_id + '] h4').text();
                            var errors = "";
                            for (i in json['error']['option'][product_id]) {
                                var element = $('#proposition_product_options_' + product_id + ' #input-option' + i.replace('_', '-'));

                                if (element.parent().hasClass('input-group')) {
                                    element.parent().after('<div class="text-danger">' + json['error']['option'][product_id][i] + '</div>');
                                } else {
                                    element.after('<div class="text-danger">' + json['error']['option'][product_id][i] + '</div>');
                                }

                                errors += json['error']['option'][product_id][i] + "<br />";
                            }

                            $.smkAlert({
                                text: '<strong>' + product_name + '</strong><br />' + errors,
                                type: 'danger',
                                permanent: true,
                            });

                            // Highlight any found errors
                            $('.text-danger').parent().addClass('has-error');
                        }


                        $.smkAlert({
                            text: json['error']['message'],
                            type: 'warning',
                            permanent: true,
                        });
                    }
                }

                if (json['success']) {// Need to set timeout otherwise it wont update the total
                    $.smkAlert({
                        text: json['success'],
                        type: 'success'
                    });

                    setTimeout(function () {
                        $('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
                    }, 100);

                    $('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function () {
                    });

                    $('#cart > #cart_modal').modal('show');
                }
            }
        });
    },
    remove: function (key) {
        $.ajax({
            url: 'index.php?route=checkout/proposition/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                setTimeout(function () {
                    $('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
                }, 100);

                $('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function () {
                });
            }
        });
    },
    quantity: {
        plus: function (key, quantity) {
            var qty = parseInt(quantity);
            $.ajax({
                url: 'index.php?route=checkout/proposition/edit',
                type: 'post',
                data: 'key=' + key + '&quantity=' + (qty + 1),
                dataType: 'json',
                beforeSend: function () {
                    $('#cart > button').button('loading');
                },
                complete: function () {
                    $('#cart > button').button('reset');
                },
                success: function (json) {
                    setTimeout(function () {
                        $('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
                    }, 100);

                    $('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function () {
                    });
                }
            });
        },
        minus: function (key, quantity) {
            var qty = parseInt(quantity);
            if (quantity != 1) {
                $.ajax({
                    url: 'index.php?route=checkout/proposition/edit',
                    type: 'post',
                    data: 'key=' + key + '&quantity=' + (qty - 1),
                    dataType: 'json',
                    beforeSend: function () {
                        $('#cart > button').button('loading');
                    },
                    complete: function () {
                        $('#cart > button').button('reset');
                    },
                    success: function (json) {
                        setTimeout(function () {
                            $('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
                        }, 100);

                        $('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function () {
                        });
                    }
                });
            }
        },
        update: function (key, quantity) {
            var qty = parseInt(quantity);
            $.ajax({
                url: 'index.php?route=checkout/proposition/edit',
                type: 'post',
                data: 'key=' + key + '&quantity=' + qty,
                dataType: 'json',
                beforeSend: function () {
                    $('#cart > button').button('loading');
                },
                complete: function () {
                    $('#cart > button').button('reset');
                },
                success: function (json) {
                    setTimeout(function () {
                        $('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
                    }, 100);

                    $('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function () {
                    });
                }
            });
        }
    }
};
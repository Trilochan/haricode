<?php
$_['text_add_error'] = 'При добавлении заказа произошла ошибка';
$_['error_telephone'] = 'Пожалуйста введите номер телефона';
$_['text_success'] = 'Ваш заказ успешно оформлен. Наш менеджер свяжется с вами в ближайшее время.';
<?php

// Heading 
$_['heading_title']      	= 'Simple Blog';

$_['text_date_format']		= 'd.m.Y';

$_['text_popular_all']		= 'Популярное';
$_['text_latest_all']		= 'Новости';

$_['text_no_result']		= 'Новостей нет!';

$_['text_comments']		    = 'Коментариев: ';
$_['text_show_all']		    = 'Показать все';
$_['text_detail']		    = 'Подробнее';

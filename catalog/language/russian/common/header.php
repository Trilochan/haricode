<?php
// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = 'Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'Загрузки';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Смотреть Все';
$_['text_logged']        = 'Личный кабинет';
$_['text_contact']       = 'Контакты';
$_['text_callback']      = 'Обратный звонок';
$_['text_greet']         = 'Приветствуем!';
$_['text_register_success'] = 'Спасибо за регистрацию. Теперь Вы можете полноценно совершать покупки в нашем интернет-магазине.';

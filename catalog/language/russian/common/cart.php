<?php
// Text
$_['text_items']    = 'Товаров %s (%s)';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Посмотреть корзину';
$_['text_checkout'] = 'Оформить заказ';
$_['text_recurring']  = 'Платежный профиль';
$_['text_cart_modal_header']  = '<span>%s товар(ов) в корзине</span> на сумму %s';
$_['text_featured']  = 'Рекомендуемые товары';
$_['text_related']  = 'Рекомендуем добавить к заказу';
$_['text_model']  = 'Код: ';
$_['text_cart']  = 'Ваша Корзина ';
$_['text_product']  = 'Товар: ';
$_['text_quantity']  = 'Кол-во: ';
$_['text_price']  = 'Цена: ';

//button
$_['button_cart']  = 'Купить';
$_['button_fast_order']  = 'Купить в 1 клик';
$_['button_remove']  = 'Удалить';

$_['text_discount']               = 'Текущая скидка %s. Для получения <span class="dop_discount">скидки %s</span> <br> добавьте к заказу товар на сумму от %s. Например:';
$_['text_discount_final']         = 'Текущая скидка %s.';
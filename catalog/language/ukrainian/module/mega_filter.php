<?php
$_['heading_title'] = 'Фільтр товарів';
$_['name_price'] = 'Ціна';
$_['name_manufacturers'] = 'Виробники';
$_['name_rating'] = 'Рейтинг';
$_['name_search'] = 'Пошук';
$_['name_stock_status'] = 'Наявність';
$_['name_location'] = 'Location';
$_['name_length'] = 'Довжина';
$_['name_width'] = 'Ширина';
$_['name_height'] = 'Висота';
$_['name_mpn'] = 'MPN';
$_['name_isbn'] = 'ISBN';
$_['name_sku'] = 'SKU';
$_['name_upc'] = 'UPC';
$_['name_ean'] = 'EAN';
$_['name_jan'] = 'JAN';
$_['name_model'] = 'Модель';
$_['text_button_apply'] = 'Застосувати';
$_['text_reset_all'] = 'Скинути';
$_['text_show_more'] = 'Показати ще (% s)';
$_['text_show_less'] = 'Показати менше';
$_['text_display'] = 'Показати';
$_['text_grid'] = 'Сітка';
$_['text_list'] = 'Список';
$_['text_loading'] = 'Завантаження ...';
$_['text_select'] = 'Вибір ...';
$_['text_go_to_top'] = 'Вгору';
$_['text_init_filter'] = 'Ініціалізація фільтра';
$_['text_initializing'] = 'Ініціалізація ...';
?>
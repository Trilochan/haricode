<?php

// Heading 
$_['heading_title']      	= 'Simple Blog';

$_['text_date_format']		= 'd.m.Y';

$_['text_popular_all']		= 'Популярне';
$_['text_latest_all']		= 'Новини';

$_['text_no_result']		= 'Новин немає!';

$_['text_comments']		    = 'Коментарів: ';
$_['text_show_all']		    = 'Показати всі';
$_['text_detail']		    = 'Детальніше';

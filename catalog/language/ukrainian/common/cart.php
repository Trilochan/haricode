<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_items']    = '%s товар(ів) - %s';
$_['text_empty']    = 'Ваш кошик порожній!';
$_['text_cart']     = 'Переглянути кошик';
$_['text_checkout'] = 'Оформити замовлення';
$_['text_recurring']  = 'Профіль оплати';
$_['text_cart_modal_header']  = '<span>%s товар(ів) в кошику</span> на сумму %s';
$_['text_featured']  = 'Рокомендовані товари';
$_['text_related']  = 'Рекомендуемо додаты до замовлення';
$_['text_model']  = 'Код товару: ';
$_['text_cart']  = 'Ваш Кошик ';
$_['text_product']  = 'Товар: ';
$_['text_quantity']  = 'Кількість: ';
$_['text_price']  = 'Ціна: ';

//button
$_['button_cart']  = 'Купувати';
$_['button_fast_order']  = 'Купувати в 1 клік';
$_['button_remove']  = 'Видалити';

$_['text_discount']               = 'Поточна знижка %s. Для отримання <span>знижки %s</span> додайте до замовлення товар на суму від %s.';
$_['text_discount_final']         = 'Поточна знижка %s.';
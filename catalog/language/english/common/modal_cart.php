<?php
// Text
$_['text_items']    = '%s item(s) - %s';
$_['text_empty']    = 'Your shopping cart is empty!';
$_['text_cart']     = 'View Cart';
$_['text_checkout'] = 'Checkout';
$_['text_recurring']  = 'Payment Profile';
$_['text_cart_modal_header']  = '<span>%s item(s) in cart</span> to the amount of %s';
$_['text_featured']  = 'Featured products';
$_['text_related']  = 'Recommend add to the order';
$_['text_model']  = 'Model: ';
$_['text_cart']  = 'Cart: ';
$_['text_product']  = 'Product: ';
$_['text_quantity']  = 'Quantity: ';
$_['text_price']  = 'Price: ';

//button
$_['button_cart']  = 'Add to cart';
$_['button_fast_order']  = 'Buy in 1 click';
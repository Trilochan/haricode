<?php

// Text
$_['text_items']    = '%s item(s) - %s';
$_['text_empty']    = 'Your shopping cart is empty!';
$_['text_cart']     = 'View Cart';
$_['text_checkout'] = 'Checkout';
$_['text_recurring']  = 'Payment Profile';
$_['text_cart_modal_header']  = '<span>%s item(s) in the cart</span> in the amount of %s';
$_['text_featured']  = 'Featured Products';
$_['text_related']  = 'Recommended Add to the order';
$_['text_model']  = 'Model: ';
$_['text_cart']  = 'Cart: ';
$_['text_product']  = 'Products: ';
$_['text_quantity']  = 'Qty: ';
$_['text_price']  = 'Price: ';

//button
$_['button_cart']  = 'Buy';
$_['button_fast_order']  = 'Buy 1 click';
$_['button_remove']  = 'Delete';

<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';

				$_['text_callback_header'] = 'Request a call back';
				$_['text_callback_name'] = 'Your name';
				$_['text_callback_telephone'] = 'Your phone';
				$_['text_callback_submit'] = 'Call me';
				$_['text_callback_success_header'] = 'Application is accepted';
				$_['text_callback_success'] = 'Your application is accepted, please expect our manager in the near future you will be contacted';
				$_['callback_error_name'] = 'The name must be from 1 to 32 characters!';
				$_['callback_error_phone'] = 'The phone must be between 3 and 32 characters!';
				$_['text_callback_email_subject'] = '%s, callback';
				$_['text_callback_email_name'] = 'Name';
				$_['text_callback_email_phone'] = 'Phone';
				$_['text_callback_referer'] = 'Page';
			
<?php
class ControllerModuleViewed extends Controller {
	public function index($setting) {
        $this->document->addStyle('catalog/view/javascript/jquery/slick/slick.css');
        $this->document->addStyle('catalog/view/javascript/jquery/slick/slick-theme.css');
        $this->document->addScript('catalog/view/javascript/jquery/slick/slick.min.js');

        $this->load->language('module/viewed');
        $data['heading_title'] = $this->language->get('heading_title');
		//$data['heading_title'] = $setting['name'];

		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_details'] = $this->language->get('text_details');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/index.php?route=common/home') {
			$data['route_home'] = true;
		} else {
			$data['route_home'] = '';
		}

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

        $products = array();

        if (isset($this->request->cookie['viewed'])) {
            $products = explode(',', $this->request->cookie['viewed']);
        } else if (isset($this->session->data['viewed'])) {
            $products = $this->session->data['viewed'];
        }

        if (isset($this->request->get['route']) && $this->request->get['route'] == 'product/product') {
            $product_id = $this->request->get['product_id'];
            $products = array_diff($products, array($product_id));
            array_unshift($products, $product_id);
            setcookie('viewed', implode(',',$products), time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
        }

		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}

		$products = array_slice($products, 0, (int)$setting['limit']);

		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}

				$is_complect = $this->model_catalog_product->isComplect($product_info['product_id']);


                $data['stickers'] = false;
                $sticker_new = false;
                $sticker_special = false;
                $sticker_rating = false;
                $sticker_vip = false;
                if($this->config->get('stickers_status')) {
                    $data['stickers'] = true;
                    if ($this->config->get('stickers_new_status')) {
                        $date_added = strtotime($product_info['date_added']);
                        $date_now   = strtotime(date("Y-m-d H:i:s"));
                        $days       = (int)date('d', $date_now - $date_added);

                        if ($days < (int)$this->config->get('stickers_new_days')) {
                            $sticker_new = array(
                                'status'  => true,
                                'z_index' => $this->config->get('stickers_new_sort_order'),
                            );
                        } else {
                            $sticker_new = false;
                        }
                    }

                    if ($this->config->get('stickers_special_status')) {
                        if ($special) {
                            $sticker_special = array(
                                'status'  => true,
                                'z_index' => $this->config->get('stickers_special_sort_order'),
                            );
                            true;
                        } else {
                            $sticker_special = false;
                        }
                    }

                    if ($this->config->get('stickers_rating_status')) {
                        switch ($this->config->get('stickers_rating_criteria')) {
                            case 'view':
                                if ($product_info['viewed'] >= (int)$this->config->get('stickers_rating_value')) {
                                    $sticker_rating = array(
                                        'status' => true,
                                        'z_index' => $this->config->get('stickers_rating_sort_order'),
                                    );
                                } else {
                                    $sticker_rating = false;
                                }
                                break;
                            case 'rating':
                                if ($product_info['reviews'] >= (int)$this->config->get('stickers_rating_value')) {
                                    $sticker_rating = array(
                                        'status' => true,
                                        'z_index' => $this->config->get('stickers_rating_sort_order'),
                                    );
                                } else {
                                    $sticker_rating = false;
                                }
                                break;
                            case 'purchase':
                                if ((int)$this->model_catalog_product->getNumberOfSales($product_info['product_id']) >= (int)$this->config->get('stickers_rating_value')) {
                                    $sticker_rating = array(
                                        'status' => true,
                                        'z_index' => $this->config->get('stickers_rating_sort_order'),
                                    );
                                } else {
                                    $sticker_rating = false;
                                }
                                break;
                        }
                    }

                    if ($this->config->get('stickers_vip_status')) {
                        if (in_array($product_info['product_id'], $this->config->get('stickers_vip_products'))) {
                            $sticker_vip = array(
                                'status' => true,
                                'z_index' => $this->config->get('stickers_vip_sort_order'),
                            );
                        } else {
                            $sticker_vip = false;
                        }
                    }
                }
            
				$data['products'][] = array(

                'sticker_rating' => $sticker_rating,
                'sticker_special' => $sticker_special,
                'sticker_new' => $sticker_new,
                'sticker_vip' => $sticker_vip,
            
					'product_id'  => $product_info['product_id'],
					'thumb'       => $image,
					'name'        => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'reviews'     => $product_info['reviews'],
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                    'reviews_count' => sprintf($this->language->get('reviews_count'), $rating),
                    'is_complect' => $is_complect,
				);
			}
		}
		@$data['route_slider'] = $this->request->get['route'];
		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/product_slider.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/product_slider.tpl', $data);
			} else {
				return $this->load->view('default/template/module/product_slider.tpl', $data);
			}
		}
	}
}
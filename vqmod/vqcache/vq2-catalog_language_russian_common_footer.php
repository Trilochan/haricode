<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Обратная связь';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнерская программа';
$_['text_special']      = 'Акции';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Рассылка';

				$_['text_callback_header'] = 'Заказать обратный звонок';
				$_['text_callback_name'] = 'Ваше имя';
				$_['text_callback_telephone'] = 'Ваш телефон';
				$_['text_callback_submit'] = 'Перезвоните мне';
				$_['text_callback_success_header'] = 'Заявка принята';
				$_['text_callback_success'] = 'Ваша заявка принята, пожалуйста, ожидайте, наш менеджер в ближайшее время с Вами свяжется';
				$_['callback_error_name'] = 'Имя должно содержать от 1 до 32 символов!';
				$_['callback_error_phone'] = 'Телефон должен содержать от 3 до 32 символов!';
				$_['text_callback_email_subject'] = '%s, обратный звонок';
				$_['text_callback_email_name'] = 'Имя';
				$_['text_callback_email_phone'] = 'Телефон';
				$_['text_callback_referer'] = 'Страница запроса';
			
$_['text_powered']      = 'Работает на <a href="http://opencart-russia.ru">OpenCart "Русская сборка"</a><br /> %s &copy; %s';

$_['work_time'] = 'График работы Call-центра <br>Будни с 9.00 до 19.00 <br>Суббота с 10.00 до 18.00';
$_['order_time'] = 'Заказы через сайт <br>принимаются круглосуточно';
$_['text_information'] = 'Информация';
$_['text_gift_for_all'] = '100 гривен в подарок каждому подписчику!';
$_['text_gift_take_naw'] = 'Получить сейчас';
$_['text_developer'] = 'разработка и продвижение сайтов';
				$_['text_register_header'] = "Новый пользователь";
				$_['entry_register_email'] = "Ваш E-mail";
				$_['entry_register_password'] = "Ваш пароль";
				$_['text_register_success_header'] = "Регистрация";
				$_['text_register_success'] = "На указанный E-mail было отправлено сообщение с дальнейшими инструкциями.";
				$_['text_button_register'] = "Зарегистрироваться";

				$_['text_login_header'] = "Вход на сайт";
				$_['entry_login_email'] = "Ваш E-mail";
				$_['entry_login_password'] = "Ваш пароль";
				$_['entry_login_remember'] = "Запомнить меня";
				$_['text_button_login'] = "Войти";
				$_['text_login_forgotten_password'] = "Забыли пароль?";
			
<?php echo $header; ?>
<div class="breadCrombs">
  <div class="my-conteiner">
    <ul class="my-breadcrumb">
      <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
        <?php if (count($breadcrumbs) != ($key + 1)) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
        <?php } else { ?>
          <li><a><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
        <?php } ?>
      <?php } ?>
    </ul>
  </div>
  <div class="my-conteiner my-spec-con-prod">
    <div class="row" id="product-order" style="margin: 0px">
      <div class="title_product_text">
        <h3 class="my-product-heading"><?php echo $heading_title; ?></h3>
        <p>
          <span class="my-product-manufact"><?php echo $text_manufacturer; ?> <span><?php echo $manufacturer; ?></span></span>

        </p>

      </div>
      <div class="tovar_true">
        <p class="my-product-model <?=($product_quantity <= 0 )?'productNo':''; ?> <?= ($stock == '2-3 Дня' || $stock == 'Предзаказ')? 'superGreen' : '' ?>" ><?php echo $stock; ?></p>
          <?php if ($sku) { ?>
            <p class="my-product-model_code "><?php echo $text_model; ?>  <?php echo $sku; ?></p>
          <?php } ?>
      </div>
      <div class="my-product-block-img">
        <img class="my-product-main-img" data-mfp-src="<?php echo $thumb; ?>" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
        
        <div class="my-product-small-img">
        <img class="my-product-two-img" data-mfp-src="<?php echo $thumb; ?>" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
        <?php foreach ($images as $image) { ?>
        <img class="my-product-two-img" data-mfp-src="<?php echo $image['thumb']; ?>" data-my="asdf" src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
        <?php } ?>
      </div>
    </div>
    <script>
      $( ".my-product-block-img .my-product-main-img" ).click(function() {
        var s = $( this ).attr('src');
        $('.my-product-small-img img[src="'+s+'"]').click();
      });
    </script>
    <form id="for-fast-order">
    <div class="my-product-block-dis">
      <div class="my-product-block-dis-head">
      <button class="my-product-button-compire <?php if($product_in_compare){echo 'compareYes';}; ?>" onclick="compare.add('<?php echo $product_id; ?>'); return false;"><span class="fa fa-balance-scale "></span><p><?php echo $button_compare; ?></p></button>
        
        
        <button class="my-product-wish-but" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>'); return false;"><span class="fa fa-heart"></span><?php echo $button_wishlist; ?></button>
      </div>
      <div class=" optionsDetailed">
      <p class="my-product-main-rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($rating < $i) { ?>
          <span class="rating-no"></span>
          <?php } else { ?>
          <span class="rating-yes"></span>
          <?php } ?>
          <?php } ?>
        </p>
        <span onclick="myJump.jump('#for-trigger-review')" class="my-product-rating-count"><?php echo $tab_review; ?></span>
          <div id="product">
            
            <div class="form-group clear oh optionsPriceDetailed">

            <?php if(!$complects_status && (int)$price > 0) { ?>
            <div class="leftBlockProduct">
              <input style="display: none;" type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <?php if($discountsName) { ?>
              <?php foreach($discountsName as $discountName) { ?>
              <?php if($discountName['customer_id'] == $_SESSION['customer_id']) { ?>
              <p class="old-your-price"><?php echo $discountName['old_price']; ?><br>
                <span class="my-discount-name">Ваша скидка <?php echo $discountName['firstname']; ?></span>
                <span class="my-discount-price"><?php echo $discountName['discount_price']; ?></span><br>
              </p>
              <p class="new-your-price my-product-price"><?php echo $discountName['price']; ?></p>
              <?php } ?>
              <?php } ?>
              <?php } else { ?>
              <p class="my-product-price">
                <?php echo $price; ?>
              </p>
              <?php } ?>
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="my-product-button-buy"><?php echo $button_cart; ?></button>
              <div style="float: left">

          		  <input id="one-click-phone" placeholder="+38 (___) ___ -__-__" type="phone" name="one-click-phone" />
				  <p onclick="quick_buy.add('#for-fast-order');" name="submit" class="my-product-buyone"><?php echo $text_one_click_buy; ?></p>

			<div class="modal fade" id="quick-buy" tabindex="-1" role="dialog" aria-labelledby="quick-buyLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $text_one_click_buy_close; ?>"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel"><?php echo $text_one_click_buy_success; ?></h4>
			      </div>
	
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $text_one_click_buy_close; ?></button>
			      </div>
			    </div>
			  </div>
			</div>

			
                <span id="for-after-fast"></span>
              </div>
              <div style="float: left;width: 100%;" class="buy-denger">
                <p class="text-danger"></p>
                <p class="text-success"></p>
              </div>
            </div>
            <?php } ?>

            <div class="rightBlockProduct">
              <button class="my-product-credit">Оформить в кредит</button>
            <div class="prodInfoText" style="">
              <?php echo $trusts; ?>
            </div>
            </div>
              
            </div>

          </div>
      </div>
    </div>
    <?php if ($options) { ?>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'image') { ?>
            <div class="optionsColorDetailed form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <h3  class="my-option-heading"><?php echo $option['name']; ?>: </h3>
              <div  id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                  <label style="padding: 0px 12px 0px 0px;" class="my-option-image-label">
                    <input class="my-option-image-input" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" title="<?php echo $option_value['name']; ?>" class="my-option-image" />
                    <?php //echo $option_value['price'] ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
    </form>
    <div class="propositionDetailed">
      <?php if ($proposition_status) { ?>
      <style>
      .fa-equal::before {
      content : "="
      }
      </style>
      <div class="propositionInnerWrap">
        <div class="proposition-slick">
          <?php foreach ($propositions as $proposition_id => $proposition) { ?>
          <div id="proposition_<?php echo $proposition_id; ?>" class="">
            <h3 class="text-center titleProp"><?php echo $proposition['name']; ?></h3>
            <div class="oneItem" data-product-id="<?php echo $main_product['product_id']; ?>">
              <div class="product-thumb transition">
                <?php if ($options) { ?>
                <a class="adventageOption" href="javascript: void(null);" onclick="proposition.options('<?php echo $main_product['product_id']; ?>');" class="proposition-show-options"><i class="fa fa-plus-circle"></i>
                </a>
                <?php } ?>
                <p class="priceDetailed">
                  <span>
                    <?php if (!$main_product['special']) { ?>
                    <?php echo $main_product['price']; ?>
                    <?php } else { ?>
                    <?php echo $main_product['special']; ?>
                  </span>
                  <?php } ?>
                </p>
                <div class="image"><img src="<?php echo $main_product['thumb']; ?>" alt="<?php echo $main_product['name']; ?>" title="<?php echo $main_product['name']; ?>" class="img-responsive" /></div>
                <div class="caption clearfix">
                  <h4><?php echo $main_product['name']; ?></h4>
                  <div class="dopItemDesc">
                    <p><?php echo $main_product['description']; ?></p>
                    <?php if ($main_product['rating']) { ?>
                    <div class="rating">
                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                      <?php if ($main_product['rating'] < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                      <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                      <?php } ?>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <?php if ($main_product['price']) { ?>
                    <p class="price">
                      <?php if (!$main_product['special']) { ?>
                      <?php echo $main_product['price']; ?>
                      <?php } else { ?>
                      <?php echo $main_product['special']; ?>
                      <?php } ?>
                      <?php if ($main_product['tax']) { ?>
                      <span class="price-tax"><?php echo $text_tax; ?><?php echo $main_product['tax']; ?></span>
                      <?php } ?>
                    </p>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="fontProposition">+</div>
            <?php $count = count($proposition['products']); ?>
            <?php foreach ($proposition['products'] as $key => $product) { ?>
            <div class="oneItem" data-product-id="<?php echo $product['product_id']; ?>">
              <div class="product-thumb transition">
                <?php if ($product['options']) { ?>
                <a class="adventageOption" href="javascript: void(null);" onclick="proposition.options('<?php echo $product['product_id']; ?>');" class="proposition-show-options"><i class="fa fa-plus-circle"></i>
                </a>
                <?php } ?>
                <?php if ($product['price_included']) { ?>
                <p class="priceDetailed">
                  <span class="price-new"><?php echo $product['price_included']; ?></span>
                  <span class="price-old"><?php echo $product['price']; ?></span>
                </p>
                <?php } ?>
                <span class="percent"><?php echo $product['percent']; ?></span>
                <div class="image">
                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
                </div>
                <div class="caption clearfix">
                  <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                  <div class="dopItemDesc">
                    <p><?php echo $product['description']; ?></p>
                    <?php if ($product['rating']) { ?>
                    <div class="rating">
                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                      <?php if ($product['rating'] < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                      <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                      <?php } ?>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <?php if ($product['price_included']) { ?>
                    <p class="price">
                      <span class="price-new"><?php echo $product['price_included']; ?></span>
                      <span class="price-old"><?php echo $product['price']; ?></span>
                    </p>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <?php if (($key + 1) < $count) { ?>
            <div class="fontProposition" style="float: left; margin: 1% 10px 0 10px;">+</div>
            <?php } else { ?>
            <!-- <div style="float: left; margin: 7% 10px 0 10px; ">
              <i class="fa fa-equal" style="font-size: 36px;"></i>
            </div> -->
            <?php } ?>
            <?php } ?>
            <div class="deviderPropos"></div>
            <div class="buy-proposition">
              <p class="priceTotal">
                <span class="sloganProp">Вместе дешевле! Скидка:</span>
                <span class="price-old" style="color: #999;text-decoration: line-through;"><?php echo $proposition['total_old']; ?></span>
                <span class="price-new"><?php echo $proposition['total_new']; ?></span>
              </p>
              <div class="button-group">
                <button type="button" class="my-product-button-buy buyProp" onclick="proposition.add('<?php echo $proposition_id; ?>');"><span class="hidden-xs hidden-sm hidden-md hide"></span><?php echo $button_cart; ?></button>
                <span class="afterButton">Комплект</span>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <?php } ?>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
  <ul class="nav nav-tabs optionsAllDetailed" role="tablist">
    <li role="presentation" class="active my-boot-tab"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php echo $tab_all; ?></a></li>
    <li class="my-boot-tab" role="presentation"><a id="for-trigger-review" href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php echo $tab_reviews; ?></a></li>
    <li class="my-boot-tab" role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><?php echo $tab_video; ?></a></li>
  </ul>
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <div class="my-product-descript col-md-6">
      <?php if ($description) { ?>
      <h3><?php echo $tab_description; ?></h3>
      <?php } ?>
        <?php echo $description; ?>
      </div>
      <!-- <div class="my-product-descript-line"></div> -->
      <div class="my-attribut-block col-md-6">
      <?php if ($attribute_groups) { ?>
      <h3><?php echo $tab_attribute; ?></h3>
      <?php } ?>
        <ul class="my-attribut-list">
          <?php foreach ($attribute_groups as $attribute_group) { ?>
          <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
          <li>
            <span class="my-attribut-name"><?php echo $attribute['name']; ?></span>
            <span class="my-attribut-text"><?php echo $attribute['text']; ?></span>
          </li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <div class="reviewAllWrap" id="review"></div>
      <button class="my-product-post-review" onclick="mySlideToggle('#form-review');"><?php echo $text_write; ?></button>
      <div class="col-md-5 col-sm-5">
        <div class="tab-pane" id="tab-review">
          <form class="form-horizontal" id="form-review">
            <div id="review"></div>
            <h2><?php echo $text_write; ?></h2>
            <?php if ($review_guest) { ?>
            <div class="form-group required">
              <div class="col-sm-12">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="name" value="" id="input-name" class="form-control" />
              </div>
            </div>
            <div class="form-group required">
              <div class="col-sm-12">
                <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                <div class="help-block hide"><?php echo $text_note; ?></div>
              </div>
            </div>
            <div class="form-group required">
              <div class="col-sm-12">
                <label class="control-label"><?php echo $entry_rating; ?></label>
                &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                <input type="radio" name="rating" value="1" />
                &nbsp;
                <input type="radio" name="rating" value="2" />
                &nbsp;
                <input type="radio" name="rating" value="3" />
                &nbsp;
                <input type="radio" name="rating" value="4" />
                &nbsp;
                <input type="radio" name="rating" value="5" />
              &nbsp;<?php echo $entry_good; ?></div>
            </div>
            <?php if ($site_key) { ?>
            <div class="form-group">
              <div class="col-sm-12">
                <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
              </div>
            </div>
            <?php } ?>
            <div class="buttons clearfix">
              <div class="pull-right">
                <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary giveFeedback"><?php echo $button_continue; ?></button>
              </div>
            </div>
            <?php } else { ?>
            <?php echo $text_login; ?>
            <?php } ?>
          </form>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages"><?php echo ($video ?: $text_no_video); ?></div>


  </div>

  <!-- start complects -->

       <?php if ($complects_status) { ?>
              <div class="complects clear" id="complect">
                  <?php echo $text_complect_header; ?>
                  <button id="show-complects" class="visible-xs show-complects btn-default">Показать комплекты</button>
                  <div class="checkbox">
                  </div> 
                  <div id="complect_products">
                      <?php foreach ($complect as $product) { ?>
            <?php if($product['special']){
              $def_price = $product['special'];
              }else{
                $def_price = $product['price'];
              }?>
                          <div class="my-slider-product-block" id="product_<?php echo $product['product_id']; ?>" data-c-id="<?php echo $product['product_id']; ?>" data-price-def="<?php echo strip_tags($def_price); ?>">
              <input type="hidden" name="product_id" value="<?php echo $product['product_id'];?>" />
              <input type="hidden" name="option[0]" value="<?php echo $product_id;?>" />
                              <div class="product-thumb transition">
                <?php if($product['options']){ ?>
                <?php foreach($product['options'] as $opt){?>
                  <?php if($opt['required']){?>
                    <a class="complects-show-options" href="javascript: void(null);">
                      <i class="fa fa-plus-circle"></i>
                    </a>
                  <?php } ?>
                <?php } ?>
                <?php } ?>
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="" class="link_img"></a>
                                <h4 class="complect_name"><a class="my-slider-product-name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                  <div class="clearfix">
                                      <div class="price ">
                     <?php if (!$product['special']) { ?>
                                              <span class="price-curent " id="price"><?php echo $product['price']; ?></span>
                                          <?php } else { ?>
                                             <span class="stock_price" title="Акционная цена" id="price" > <?php echo $product['special']; ?></span>
                        <span class="old-price" ><?php echo $product['price']; ?></span>
                      
                                          <?php } ?> 
                    </div>
                                      <div class="quantity ">
                                          <i class="fa fa-minus" onclick="complect.quantity.minus('<?php echo $product['product_id']; ?>')"></i>
                                          <div class="input_wrap"><input type="text" class="form-control" name="quantity" id="quantity" value="1" onchange="complect.quantity.update('<?php echo $product['product_id']; ?>');" /></div>
                                          <i class="fa fa-plus" onclick="complect.quantity.plus('<?php echo $product['product_id']; ?>')"></i>
                                      </div>
                                  </div>

                  <!-- OPTIONS -->
                  <div class="dropdown-caption" > <?php foreach ($product['options'] as $option) { ?>
                    <?php if ($option['type'] == 'select' && $option['required']) { ?>
                      <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                          <option data-option-price="<?php echo $option_value['price']; ?>" data-option-price-prefix="<?php echo $option_value['price_prefix'];?>" value=""><?php echo $text_select; ?></option>
                          <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                              <?php if ($option_value['price']) { ?>
                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                              <?php } ?>
                            </option>
                          <?php } ?>
                        </select>
                      </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'radio' && $option['required']) { ?>
                      <div class="option-image col-sm-6 form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                         <div class="name"><?php echo $option['name']; ?></div>
                         <div class="wrapper-value clearfix">
                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                          <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <div class="radio">
                              <label>
                                <input data-option-price="<?php echo $option_value['price']; ?>" data-option-price-prefix="<?php echo $option_value['price_prefix'];?>" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                <?php echo $option_value['name']; ?>
                                <?php if ($option_value['price']) { ?>
                                  (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                <?php } ?>
                              </label>
                            </div>
                          <?php } ?>
                        </div>
                        </div>
                      </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'checkbox' && $option['required']) { ?>
                      <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"><?php echo $option['name']; ?></label>
                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                          <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <div class="checkbox">
                              <label>
                                <input data-option-price="<?php echo $option_value['price']; ?>" data-option-price-prefix="<?php echo $option_value['price_prefix'];?>" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                <?php echo $option_value['name']; ?>
                                <?php if ($option_value['price']) { ?>
                                  (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                <?php } ?>
                              </label>
                            </div>
                          <?php } ?>
                        </div>
                      </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'image' && $option['required']) { ?>
                      <div class="option-image<?php echo($option['required'] ? ' required' : ''); ?>">
                         <div class="name"><?php echo $option['name']; ?></div>
                          <div class="wrapper-value clearfix">
                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                              <div class="value clearfix tooltip-img">
                              <label>
                              <input data-option-price="<?php echo $option_value['price']; ?>" data-option-price-prefix="<?php echo $option_value['price_prefix'];?>"  data-color-name="<?php echo $option_value['name']; ?>" hidden="hidden" id = "option[<?php echo $option['product_option_id']; ?>]"  type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                <img class="sel_color" title="<?php echo $option_value['name']; ?>" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"/>
                                <span><nobr><?php echo $option_value['name']; ?></nobr></span>
                              </label>
                              </div>
                            <?php } ?>
                          </div>
                      </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'text' && $option['required']) { ?>
                      <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>"
                             id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"/>
                      </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'textarea' && $option['required']) { ?>
                      <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>"
                              class="form-control"><?php echo $option['value']; ?></textarea>
                      </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'file' && $option['required']) { ?>
                      <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label"><?php echo $option['name']; ?></label>
                        <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                        <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>"/>
                      </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'date' && $option['required']) { ?>
                      <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <div class="input-group date">
                          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"/>
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                      </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'datetime' && $option['required']) { ?>
                      <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <div class="input-group datetime">
                          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"/>
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                      </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'time' && $option['required']) { ?>
                      <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <div class="input-group time">
                          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"/>
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                      </div>
                    <?php } ?>
                  <?php } ?>
                   </div> <!-- end dropdown-caption-->

                                  <div class="add-complect">
                                      <input type="checkbox" id="add_complect_<?php echo $product['product_id']; ?>" onclick="complect.add('<?php echo $product['product_id']; ?>')">
                                      <label for="add_complect_<?php echo $product['product_id']; ?>"><span><?php echo $text_add_complect; ?></span>
                                      <span><?php echo $text_product_added; ?></span></label>
                                  </div>
                              </div>
                          </div>
                      <?php } ?>
                  </div>
              </div>
            <?php } ?>

<!-- end of complects -->


</div>
</div>
</div>
<div class="my-conteiner" style="max-width: 1470px">
<div class="spec-prod-cont-back oneColumn">
<?php if ($products) { ?>
<?php include DIR_TEMPLATE . 'homealone/template/module/product_slider.tpl'; ?>
<?php } ?>
<?php echo $content_bottom; ?>
</div>
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
$.ajax({
url: 'index.php?route=product/product/getRecurringDescription',
type: 'post',
data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
dataType: 'json',
beforeSend: function() {
$('#recurring-description').html('');
},
success: function(json) {
$('.alert, .text-danger').remove();
if (json['success']) {
$('#recurring-description').html(json['success']);
}
}
});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
$.ajax({
url: 'index.php?route=checkout/cart/add',
type: 'post',
data: $('#product-order input[type=\'text\'], #product-order input[type=\'hidden\'], #product-order input[type=\'radio\']:checked, #product-order input[type=\'checkbox\']:checked, #product-order select, #product-order textarea'),
dataType: 'json',
beforeSend: function() {
$('#button-cart').button('loading');
},
complete: function() {
$('#button-cart').button('reset');
},
success: function(json) {
$('.alert, .text-danger').remove();
$('.form-group').removeClass('has-error');
if (json['error']) {
if (json['error']['option']) {
for (i in json['error']['option']) {
var element = $('#input-option' + i.replace('_', '-'));
if (element.parent().hasClass('input-group')) {
element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
} else {
element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
}
}
}
if (json['error']['recurring']) {
$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
}
// Highlight any found errors
$('.text-danger').parent().addClass('has-error');
}
if (json['success']) {
$.smkAlert({
text: json['success'],
type: 'success'
});
$('#cart .my-shop-cart-total').html('(' + json['count'] + ')');
$('#cart > #cart_modal .modal-content').load('index.php?route=common/cart/info #cart > #cart_modal .modal-content > *', function() {telephoneMask();});
$('#cart > #cart_modal').modal('show');
}
}
});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
pickTime: false
});
$('.datetime').datetimepicker({
pickDate: true,
pickTime: true
});
$('.time').datetimepicker({
pickDate: false
});
$('button[id^=\'button-upload\']').on('click', function() {
var node = this;
$('#form-upload').remove();
$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
$('#form-upload input[name=\'file\']').trigger('click');
if (typeof timer != 'undefined') {
clearInterval(timer);
}
timer = setInterval(function() {
if ($('#form-upload input[name=\'file\']').val() != '') {
clearInterval(timer);
$.ajax({
url: 'index.php?route=tool/upload',
type: 'post',
dataType: 'json',
data: new FormData($('#form-upload')[0]),
cache: false,
contentType: false,
processData: false,
beforeSend: function() {
$(node).button('loading');
},
complete: function() {
$(node).button('reset');
},
success: function(json) {
$('.text-danger').remove();
if (json['error']) {
$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
}
if (json['success']) {
alert(json['success']);
$(node).parent().find('input').attr('value', json['code']);
}
},
error: function(xhr, ajaxOptions, thrownError) {
alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}
});
}
}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
e.preventDefault();
$('#review').fadeOut('slow');
$('#review').load(this.href);
$('#review').fadeIn('slow');
});
$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
$('#button-review').on('click', function() {
$.ajax({
url: 'index.php?route=product/product/write',
type: 'post',
dataType: 'json',
data: $("#form-review").serialize() + "&product_id=<?php echo $product_id; ?>",
beforeSend: function() {
$('#button-review').button('loading');
},
complete: function() {
$('#button-review').button('reset');
},
success: function(json) {
$('.alert-success, .alert-danger').remove();
if (json['error']) {
$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
}
if (json['success']) {
$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
$('#tab-review input[name=\'name\']').val('');
$('#tab-review textarea[name=\'text\']').val('');
$('#tab-review input[name=\'rating\']:checked').prop('checked', false);
if (window.grecaptcha !== undefined) {
  grecaptcha.reset();
}
}
}
});
});
$(document).ready(function() {
$('.my-product-small-img').magnificPopup({
type:'image',
delegate: 'img',
gallery: {
enabled:true
}
});
});
$('#qaa').delegate('.pagination a', 'click', function (e) {
e.preventDefault();
$('#qaa').fadeOut('slow');
$('#qaa').load(this.href);
$('#qaa').fadeIn('slow');
});
$('#qaa').load('index.php?route=product/product/qaa&product_id=<?php echo $product_id; ?>');
$('#button-qaa').on('click', function () {
$.ajax({
url: 'index.php?route=product/product/writeQaa&product_id=<?php echo $product_id; ?>',
type: 'post',
dataType: 'json',
data: $("#form-qaa").serialize(),
beforeSend: function () {
$('#button-qaa').button('loading');
},
complete: function () {
$('#button-qaa').button('reset');
},
success: function (json) {
$('.alert-success, .alert-danger').remove();
if (json['error']) {
$('#qaa').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
}
if (json['success']) {
$('#qaa').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
$('#tab-qaa input[name=\'name\']').val('');
$('#tab-qaa textarea[name=\'text\']').val('');
}
}
});
});
//--></script>
<?php if ($options) { ?>
<div class="modal proposition-options" id="proposition_product_options_<?php echo $main_product['product_id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?php echo $text_option; ?></h4>
  </div>
  <div class="modal-body">
    <?php foreach ($options as $option) { ?>
    <?php if ($option['type'] == 'select') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
        <option value=""><?php echo $text_select; ?></option>
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
          <?php if ($option_value['price']) { ?>
          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
          <?php } ?>
        </option>
        <?php } ?>
      </select>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'radio') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label"><?php echo $option['name']; ?></label>
      <div id="input-option<?php echo $option['product_option_id']; ?>">
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <div class="radio">
          <label>
            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
            <?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'checkbox') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label"><?php echo $option['name']; ?></label>
      <div id="input-option<?php echo $option['product_option_id']; ?>">
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
            <?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'image') { ?>
    <div class="optionColorModal form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label"><?php echo $option['name']; ?></label>
      <div id="input-option<?php echo $option['product_option_id']; ?>">
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <div class="radio">
          <label>
            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] ?>"
            class="img-thumbnail" title="<?php echo $option_value['name'] ?>" /> <?php echo $option_value['name']; //echo $option_value['price'] ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'text') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>"
      id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'textarea') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>"
      class="form-control"><?php echo $option['value']; ?></textarea>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'file') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label"><?php echo $option['name']; ?></label>
      <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
      <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'date') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <div class="input-group date">
        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
        </span>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'datetime') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <div class="input-group datetime">
        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
        </span>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'time') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <div class="input-group time">
        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
        </span>
      </div>
    </div>
    <?php } ?>
    <?php } ?>
  </div>
</div>
</div>
</div>
<?php } ?>
<?php
foreach ($propositions as $proposition_id => $proposition) {
foreach ($proposition['products'] as $product) {
if ($product['options']) {
?>
<div class="modal proposition-options" id="proposition_product_options_<?php echo $product['product_id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?php echo $text_option; ?></h4>
  </div>
  <div class="modal-body">
    <?php foreach ($product['options'] as $option) { ?>
    <?php if ($option['type'] == 'select') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
        <option value=""><?php echo $text_select; ?></option>
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
          <?php if ($option_value['price']) { ?>
          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
          <?php } ?>
        </option>
        <?php } ?>
      </select>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'radio') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label"><?php echo $option['name']; ?></label>
      <div id="input-option<?php echo $option['product_option_id']; ?>">
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <div class="radio">
          <label>
            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
            <?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'checkbox') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label"><?php echo $option['name']; ?></label>
      <div id="input-option<?php echo $option['product_option_id']; ?>">
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
            <?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'image') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label"><?php echo $option['name']; ?></label>
      <div id="input-option<?php echo $option['product_option_id']; ?>">
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <div class="radio">
          <label>
            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'text') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'textarea') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'file') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label"><?php echo $option['name']; ?></label>
      <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
      <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'date') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <div class="input-group date">
        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
        </span>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'datetime') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <div class="input-group datetime">
        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
        </span>
      </div>
    </div>
    <?php } ?>
    <?php if ($option['type'] == 'time') { ?>
    <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
      <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
      <div class="input-group time">
        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
        <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
        </span>
      </div>
    </div>
    <?php } ?>
    <?php } ?>
  </div>
</div>
</div>
</div>
<?php
}
}
}
?>
<script type="text/javascript">
$(function () {
$('.my-product-small-img').slick({
  slidesToShow: 5,
  slidesToScroll: 5,
});
$('.proposition-slick').slick({
accessibility: true,
arrows: true,
draggable: false,
infinite: true,
pauseOnHover: true,
pauseOnDotsHover: true,
slidesToShow: 1,
slidesToScroll: 1,
swipeToSlide: true,
adaptiveHeight: true
});
$('.proposition-options').on('shown.bs.modal', function() {
$('.date').datetimepicker({
pickTime: false
});
$('.datetime').datetimepicker({
pickDate: true,
pickTime: true
});
$('.time').datetimepicker({
pickDate: false
});
$('button[id^=\'button-upload\']').on('click', function () {
var node = this;
$('#form-upload').remove();
$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
$('#form-upload input[name=\'file\']').trigger('click');
if (typeof timer != 'undefined') {
clearInterval(timer);
}
timer = setInterval(function () {
if ($('#form-upload input[name=\'file\']').val() != '') {
clearInterval(timer);
$.ajax({
url: 'index.php?route=tool/upload',
type: 'post',
dataType: 'json',
data: new FormData($('#form-upload')[0]),
cache: false,
contentType: false,
processData: false,
beforeSend: function () {
$(node).button('loading');
},
complete: function () {
$(node).button('reset');
},
success: function (json) {
$('.text-danger').remove();
if (json['error']) {
$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
}
if (json['success']) {
alert(json['success']);
$(node).parent().find('input').attr('value', json['code']);
}
},
error: function (xhr, ajaxOptions, thrownError) {
alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}
});
}
}, 500);
});
});
});

$('.my-product-slider-url').slick({
slidesToShow: 6,
slidesToScroll: 1,
draggable: true,
arrows: true,
swipeToSlide: true,
responsive: [
{
breakpoint: 1360,
settings: {
slidesToShow: 5,
slidesToScroll: 1,
}
},
{
breakpoint: 1200,
settings: {
slidesToShow: 4,
slidesToScroll: 1,
}
},
]
});

</script>
<?php echo $footer; ?>






<?php if ($complects_status) { ?>
            <script>
              $(function () {
          checkProducts();

        // Выбираем цвета у комплектов
        $('.sel_color').on('click',function(){


          var col_prod_id = $(this).closest('div[id*=\'product_\']').find('input[name=\'product_id\']').val();
          // alert(col_prod_id);
          var color_name = $(this).parent().find('input').attr('data-color-name');
          // alert(color_name);
          var input_color = $('#complect_products div.tooltip-img ').find('input[data-color-name=\''+color_name+'\']');
          // alert(input_color);
          // $(input_color).css("display", "block");
          var input_color_check = $('#complect_products div.tooltip-img').find('input[data-color-name=\''+color_name+'\']:checked');
          // alert(input_color_check);
          if(input_color_check.length !== input_color.length){

            $('#select_color_mod #set_color_complect').attr('data-set-col-comp',color_name);

            $('#select_color_mod .modal-footer input[name=\'prod_id_color\']').val(col_prod_id);

            $('#select_color_mod').modal('show'); 
          }
        });

        // Добавляем в корзину комплекты (без выгодного предложения)
        $("#by_complect").click(function(){
              addTovKomplekt();
          });
                $('#cart_modal').on('hidden.bs.modal', function (e) {
                  checkProducts();
                });
              });

              var complect = {
                add: function (id) { 

                  addLocking();
            var requ = true; // Переменная для проверки отмеченного radio
            var options_array = [];
             $('#complect_products #product_' + id + ' div.option-image').each(function(){
              $(this).find('.warn').remove();
              if($(this).hasClass('required')){
                if($(this).find($('input[type="radio"]:checked')).length == 0){
                  $('<div class="warn">Не выбрано</div>').appendTo(this);
                  requ = false;
                }
              }

              options_array.push({'opt_id':$(this).find($('input[type="radio"]:checked')).attr('name'),'opt_val':$(this).find($('input[type="radio"]:checked')).val()});
             });

            if(requ == true){ // Если radio отмечен то добавляем в localStorage

              var quantity = parseInt($('#complect_products #product_' + id + ' #quantity').val());
              var price = parseInt($('#complect_products #product_' + id + ' .price').text());
              var options = $('#complect_products #product_' + id + ' div.required input[type="radio"]:checked');
              var product_id = <?php echo $product_id; ?>;
              var complect_product_id = id;

              if (localStorage.getItem('product_id_<?php echo $product_id; ?>') === null) {
               var arr = [];
               arr.push({'pid':complect_product_id,'qua':quantity,'options':options_array});
              } else {
               var arr = JSON.parse(localStorage['product_id_<?php echo $product_id; ?>']); 
               var isset;
                 $.each(arr,function(key,val) {
                   if(val.pid == complect_product_id){
                    arr[key] = {"pid":complect_product_id,"qua":quantity,'options' : options_array};
                    isset = key;
                   }
                }) ;
                if(isset === undefined){
                  arr.push({'pid':complect_product_id,'qua':quantity,'options':options_array});
                }
              }
              localStorage.setItem('product_id_<?php echo $product_id; ?>', JSON.stringify(arr));
               $('#complect_products #product_' + id + ' input[type=checkbox]').attr('onclick', 'complect.remove(' + id + ')');
               $('#complect_products #product_' + id).addClass('selected');
              checkProducts();

              // Прячем выбор опций
              var div_options = $('#complect_products #product_' + id).find('.dropdown-caption');
              div_options.css('display', 'none');
              div_options.removeClass('active');
              $('#complect_products #product_' + id).find('i').removeClass('fa-minus-circle');
              $('#complect_products #product_' + id).find('i').addClass('fa-plus-circle');
            }else{

              // Если не выбрана обязательная опция - покажем это
              var div_options = $('#complect_products #product_' + id).find('.dropdown-caption');
              div_options.slideDown(400);
              div_options.addClass('active');
              $('#complect_products #product_' + id).find('i').removeClass('fa-plus-circle');
              $('#complect_products #product_' + id).find('i').addClass('fa-minus-circle');
              $('#complect_products #product_' + id).removeClass('selected');
              $('#add_complect_' + id).prop('checked',false);
              $('#complect_products #product_' + id + ' input[type=checkbox]').attr('onclick', 'complect.add(' + id + ')');
            }
             removeLocking();
                },
                remove: function (id) {
                  addLocking();
          var arr = JSON.parse(localStorage['product_id_<?php echo $product_id; ?>']);
          $.each(arr,function(key,val) {
            if(typeof(val) !== "undefined"){
              if(val.pid == id){
                arr.splice(key,1);
                if(val.options !== null){
                $.each(val.options,function(keyy,valu){ // Перебираем выбранные опции и удаляем отметки
                  $('#complect_products #product_' + val.pid).find('input[value=\''+valu.opt_val+'\']').prop('checked',false);
                });
              }
              }
            }
          });

          localStorage.setItem('product_id_<?php echo $product_id; ?>', JSON.stringify(arr));

          $('#complect_products #product_' + id + ' input[type=checkbox]').attr('onclick', 'complect.add(' + id + ')');
                    $('#complect_products #product_' + id).removeClass('selected');

          checkProducts();
          removeLocking();
                },
                quantity: {
                  plus: function (id) {
                    addLocking();
                    var input_quantity = $('#complect_products #product_' + id + ' #quantity');
                    var quantity = $(input_quantity).val();
                    ++quantity;
                    $(input_quantity).val(quantity);

          if (localStorage.getItem('product_id_<?php echo $product_id; ?>') !== null) {
            var arr = JSON.parse(localStorage['product_id_<?php echo $product_id; ?>']); 
            $.each(arr,function(key,val) {
               if(val.pid == id){
                arr[key] = {"pid":id,"qua":quantity,'options':val.options};
               }
            }) ;
              localStorage.setItem('product_id_<?php echo $product_id; ?>', JSON.stringify(arr));
            foooterBuy()
          }
          removeLocking();
                  },
                  minus: function (id) {
                    addLocking();
                    var input_quantity = $('#complect_products #product_' + id + ' #quantity');
                    var quantity = $(input_quantity).val();
                    if (quantity == 1) {
                      $(input_quantity).val(quantity);
                    } else {
                      --quantity;
                      $(input_quantity).val(quantity);
                    }

          if (localStorage.getItem('product_id_<?php echo $product_id; ?>') !== null) {
            var arr = JSON.parse(localStorage['product_id_<?php echo $product_id; ?>']); 

            $.each(arr,function(key,val) {
               if(val.pid == id){
                arr[key] = {"pid":id,"qua":quantity,'options':val.options};
               }
            }) ;
              localStorage.setItem('product_id_<?php echo $product_id; ?>', JSON.stringify(arr));
            foooterBuy()
          }
           removeLocking();
                  },
                  update: function (id) {
                    addLocking();
                    var input_quantity = $('#complect_products #product_' + id + ' #quantity');
                    var quantity = $(input_quantity).val();
                    if ($(input_quantity).attr('data-product-key')) {
                      var key = $(input_quantity).attr('data-product-key');
                      updateCart(key, quantity);
                    } else {
                      removeLocking();
                    }
                  }
                },
                all: {
                  add: function () {
                    addLocking();
                    $('#complect_products .product-thumb').each(function (index, element) {
                      if (!$(element).find('input[type=checkbox]').prop('checked')) {
                        $(element).find('input[type=checkbox]').prop('checked', true);
                        var quantity = $(element).find('#quantity').val();
                        var product_id = <?php echo $product_id; ?>;
                        var complect_product_id = $(element).parent().attr('id').split('_')[1];
                        $.ajax({
                          url: 'index.php?route=checkout/complect/add',
                          type: 'post',
                          data: {product_id: product_id, complect_product_id: complect_product_id, quantity: quantity},
                          dataType: 'json',
                          success: function (json) {
                            if (json['success']) {
                              $('#cart-total').html(json['total']);
                              $('#count_products').html(json['count_products']);
                              $('#complect_products #product_' + complect_product_id + ' input[type=checkbox]').attr('onclick', 'complect.remove(' + complect_product_id + ')');
                              $('#complect_products #product_' + complect_product_id).addClass('selected');
                            }
                          }
                        });

                        $('#cart > #cart_modal').load('index.php?route=common/cart/info #cart > #cart_modal > *', function (data) {
                          $('#cart-total').html($(data).find('#cart-total').text());
                          $('#count_products').html($(data).find('#count_products').text());
                          $('input[name=telephone]').mask("+38 (099) 999-99-99");
                          checkProducts();
                          checkAll();
                          $('#complect #select_all').attr('onclick', 'complect.all.remove()');
                          removeLocking();
                        });
                      }
                    });
                  },
                  remove: function () {
                    addLocking();
                    $('#complect_products .product-thumb').each(function (index, element) {
                      $(element).find('input[type=checkbox]').prop('checked', false);
                      var key = $(element).find('#quantity').attr('data-product-key');
                      var complect_product_id = $(element).parent().attr('id').split('_')[1];
                      $('#complect_products #product_' + complect_product_id + ' input[type=checkbox]').attr('onclick', 'complect.add(' + complect_product_id + ')');
                      $('#complect_products #product_' + complect_product_id).removeClass('selected');
                      $.ajax({
                        url: 'index.php?route=checkout/cart/remove',
                        type: 'post',
                        data: 'key=' + key,
                        dataType: 'json',
                        beforeSend: function () {
                          $('#cart > button').button('loading');
                        },
                        success: function (json) {
                          $('#cart > button').button('reset');
                          $('#cart-total').html(json['total']);
                          $('#count_products').html(json['count_products']);
                          $('#count_products_mobile').html(json['count_products']);
                          $('#cart > #cart_modal').load('index.php?route=common/cart/info #cart > #cart_modal > *', function () {
                            $('#complect_products #product_' + id + ' input[type=checkbox]').attr('onclick', 'complect.add(' + id + ')');
                            $('#complect_products #product_' + id).removeClass('selected');
                            $('input[name=telephone]').mask("+38 (099) 999-99-99");
                          });
                        }
                      });


                      $('#cart > #cart_modal').load('index.php?route=common/cart/info #cart > #cart_modal > *', function (data) {
                        $('#cart-total').html($(data).find('#cart-total').text());
                        $('#count_products').html($(data).find('#count_products').text());
                        $('input[name=telephone]').mask("+38 (099) 999-99-99");
                        checkProducts();
                        checkAll();
                        $('#complect #select_all').attr('onclick', 'complect.all.add()');
                        removeLocking();
                      });
                    });
                  }
                }
              }


              function checkProducts() {
          if(localStorage.getItem('product_id_<?php echo $product_id; ?>') !== null && JSON.parse(localStorage['product_id_<?php echo $product_id; ?>']).length > 0){
            var arr = JSON.parse(localStorage['product_id_<?php echo $product_id; ?>']);
             $.each(arr,function(key,val) {
              if(val.options !== null){

                $.each(val.options,function(keyy,valu){ // Перебираем выбранные опции
                  $('#complect_products #product_' + val.pid).find('input[value=\''+valu.opt_val+'\']').prop('checked',true);
                  var orig_pr = parseInt($('#complect_products #product_' + val.pid).attr('data-price-def'));
                  regenerateMainPriceComplect(orig_pr,' грн',val.pid); // Эта функция находится в product_option_price.xml
                });
              }
              $('#add_complect_' + val.pid).attr('onclick', 'complect.remove(' + val.pid + ')');
              $('#complect_products #product_' + val.pid).addClass('selected');
              $('#complect_products #product_' + val.pid + ' #quantity' ).val(val.qua);
              $('#add_complect_' + val.pid).prop('checked',true);
            }) ;
          }
          updateCount();
          foooterBuy();
        }

              function updateCount() {
                var count = $('#complect_products .product-thumb input[type=checkbox]:checked').length;
                $('#complect > h2 span#count').text(count);
              }

              function updateCart(key, quantity) {
                $('#cart > #cart_modal').load('index.php?route=common/cart/info #cart > #cart_modal > *', {key: key, quantity: quantity}, function (data) {
                  $('#cart-total').html($(data).find('#cart-total').text());
                  $('#count_products').html($(data).find('#count_products').text());
                  checkProducts();
                  $('input[name=telephone]').mask("+38 (099) 999-99-99");
                  removeLocking();
                });
              }


              function checkAll() {
                var product_count = $('#complect_products .product-thumb').length;
                var buy_product_count = $('#complect_products .product-thumb input[type=checkbox]:checked').length;
                if (product_count == buy_product_count) {
                  $('#complect #select_all').prop("checked", true);
                  $('#complect #select_all').attr('onclick', 'complect.all.remove()');
                } else {
                  $('#complect #select_all').prop("checked", false);
                  $('#complect #select_all').attr('onclick', 'complect.all.add()');
                }
                foooterBuy();
              }



              function addLocking() {
                $('div#complect').addClass('locking-complect');
              }



              function removeLocking() {
                $('div#complect').removeClass('locking-complect');
              }
            </script>
          <?php } ?>



<script>
        $(function() {
            foooterBuy();
            $('#cart_modal').on('hidden.bs.modal', function (e) {
                foooterBuy();
            });
        });

        function foooterBuy() {
      var total = 0;
        if(localStorage.getItem('product_id_<?php echo $product_id; ?>') !== null && JSON.parse(localStorage['product_id_<?php echo $product_id; ?>']).length > 0){
          var arr = JSON.parse(localStorage['product_id_<?php echo $product_id; ?>']);
          $.each(arr,function(key,val) {
            var price = parseInt($('#complect_products #product_'+ val.pid +' .price #price').text());
             total = total + (price * val.qua);
            });
        }else{
          total = 0;
          $('.footer-buy .total').text(total+' грн');
        }
            if(total !== 0) {
                $('.footer-buy .total').text(total);
        $('.footer-buy .total').append(' грн');
                scroll_t = $(this).scrollTop();
                if (scroll_t > 100) {
                    $('.footer-buy').removeClass('hidden');
                } else {
                    $('.footer-buy').addClass('hidden');
                }
            }
            else {
                $('.footer-buy').addClass('hidden');
            }
        }

      $(function() {
        $('.footer-buy button').click(
            function(e) {
              e.preventDefault();
              if($(window).width() < 960) {
                $('#cart_modal_mobile').modal('show');
              } else {
                $('#cart_modal').modal('show');
              }
            }
        );
      });
    </script>

    <div class="footer-buy hidden">
      <span class="total"></span>
   <div id="by_complect" class="btn btn-default footer-button-buy button-buy"><?php echo $button_cart; ?></div>
    </div>
 
<script>

  function setAllColorsComplect(colorName){
    $('#complect_products div.col-lg-3').find('div.tooltip-img input[data-color-name=\''+colorName+'\']').prop('checked',true);
    prod_id = $('#select_color_mod input[name=\'prod_id_color\']').val();
    complect.add(prod_id)
    $('#select_color_mod').modal('hide');
  }

  function addTovKomplekt(){ // Функция для добавления товаров в комплекте
      if (localStorage.getItem('product_id_<?php echo $product_id; ?>') !== null) {
        var arr = JSON.parse(localStorage['product_id_<?php echo $product_id; ?>']);
          $.each(arr,function(key,val) {
            $.ajax({
              url: 'index.php?route=checkout/cart/add',
              type: 'post',
              data: $('#complect_products #product_' + val.pid + ' input[type=\'text\'], #complect_products #product_' + val.pid + ' input[type=\'hidden\'], #complect_products #product_' + val.pid + ' input[type=\'radio\']:checked, #complect_products #product_' + val.pid + ' input[type=\'checkbox\']:checked, #complect_products #product_' + val.pid + ' select, #complect_products #product_' + val.pid + ' textarea').serialize()+'&quantity='+val.qua,
              dataType: 'json',
              success: function(json){
                if(json['success']){
                  var input_quantity = $('#complect_products #product_' + val.pid + ' #quantity');
                  var key = $(input_quantity).attr('data-product-key');
                    $('#cart_mobile > #cart_modal_mobile').load('index.php?route=common/mobile_cart/info #cart_mobile > #cart_modal_mobile > *', {key: key, quantity: val.pid}, function (data) {
                        $('#cart-total').html($(data).find('#cart-total').text());
                        $('#count_products_mobile').html($(data).find('#count_products_mobile').text()); 
                        $('input[name=telephone]').mask("+38 (099) 999-99-99");
                    });

                    $('#cart > #cart_modal').load('index.php?route=common/cart/info #cart > #cart_modal > *', {key: key, quantity: val.pid}, function (data) {
                        $('#cart-total').html($(data).find('#cart-total').text());
                        $('#count_products').html($(data).find('#count_products').text()); 
                        $('input[name=telephone]').mask("+38 (099) 999-99-99");
                    });
                }
              },
            });

            //Убираем галочки и вычитаем колличество
            $('#complect_products #product_' + val.pid + ' input[type=checkbox]').attr('onclick', 'complect.add(' + val.pid + ')');

            $('#complect_products #product_' + val.pid).removeClass('selected');

              $('#add_complect_' + val.pid).prop('checked',false);

            $('.complects-show-options i').removeClass('fa-minus-circle').addClass('fa-plus-circle');

            $('#complect_products #product_' + val.pid).find('.dropdown-caption').css('display', 'none');

            $('#complect_products #product_' + val.pid).find('.dropdown-caption').removeClass('active');

            $.each(val.options,function(keyy,valu){ // Перебираем выбранные опции и удаляем отметки
              $('#complect_products #product_' + val.pid).find('input[value=\''+valu.opt_val+'\']').prop('checked',false);
            });
          });

          // Очищаем всю инфу из локал стореджа
          localStorage.removeItem('product_id_<?php echo $product_id; ?>', JSON.stringify(arr));
          checkProducts();
          if($(window).width() < 960) {
            $('#cart_modal_mobile').modal('show');
          } else {
            $('#cart_modal').modal('show');
          }
      }
  }
</script>



<!-- Модалка с выбором цвета -->

<div class="modal fade" id="select_color_mod" tabindex="-1" role="dialog" aria-labelledby="select_color_mod_label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="select_color_mod">Подтверждение действия</h4>
            </div>
            <div class="modal-body">
                Установить такой же параметр для других товаров комплекта, в которых он доступен?
            </div>
            <div class="modal-footer">
        <input type="hidden" id="set_color_complect" data-set-col-comp = "" />
                <button type="button" class="btn btn-default pull-left complects_button_yes" onclick="setAllColorsComplect($('#set_color_complect').attr('data-set-col-comp'));">Да</button>
                <button type="button" class="btn btn-default complects_button_no" data-dismiss="modal" onclick="complect.add($('#select_color_mod input[name=\'prod_id_color\']').val());$('#select_color_mod').modal('hide');">Нет</button>
        <input type="hidden" name="prod_id_color" value=""/>
            </div>
        </div>
    </div>
</div>

<script>

  $(function () {
    /******************Комплекты*****************/
  // Show options in complects
   $('#complect_products .complects-show-options').click(function () {

    if(!($(this).parent().parent().find('.dropdown-caption').hasClass('active'))){
      
      $(this).parent().parent().find('.dropdown-caption').slideDown(400);
      $(this).parent().parent().find('.dropdown-caption').addClass('active');
      $(this).find('i').removeClass('fa-plus-circle');
      $(this).find('i').addClass('fa-minus-circle');
      
      }else{
      $(this).parent().parent().find('.dropdown-caption').slideUp(400);
      $(this).parent().parent().find('.dropdown-caption').removeClass('active');
      $(this).find('i').removeClass('fa-minus-circle');
      $(this).find('i').addClass('fa-plus-circle');
      };
     

   });
  //hide dropdown when modal opened
  $('.button-buy').on('click', function () {
    $('.dropdown-caption').css('display', 'none');
  });


  // Highlight any found errors
  $('.text-danger').each(function() {
    var element = $(this).parent().parent();

    if (element.hasClass('form-group')) {
      element.addClass('has-error');
    }
  });

    var originalPriceComplect = 0; 
      var currencyComplect = "";
    $('#complect_products div[id*=\'product_\'] input').on('change',function(){
      var id_prop_complect = $(this).closest('div[id*=\'product_\']').attr('data-c-id');
      originalPriceComplect = parseInt($('#product_'+id_prop_complect).attr('data-price-def'));
      regenerateMainPriceComplect(originalPriceComplect,' грн',id_prop_complect);
    });
  });

  /**********************************************/
    function getSum(originalPrice, price, prefix) {
        switch (prefix) {
            case '-':
          return parseFloat(originalPrice) - parseFloat(price);
            case '+':
                return parseFloat(originalPrice) + parseFloat(price);
        }
    }

  /*************Комплекты*****************/
  function regenerateMainPriceComplect(originalPrice, currency,id){
    var sum = 0;
    $('#complect_products div#product_'+id).find('input[type=radio]:checked').each(function(){
      var price = $(this).attr('data-option-price');
            var prefix = $(this).attr('data-option-price-prefix');
            if (price && prefix) {
                sum = getSum(sum, price, prefix);
            }
    });
    chaingePriceComplect(originalPrice, sum, currency,id);
  }

  function chaingePriceComplect(originalPrice, changedPrice, currency,id) {
     if(originalPrice + changedPrice > 0){
    var pr_o =  parseInt($('div[data-c-id='+id+']').find('.oldd-price').text());
    var pr_n = originalPrice + changedPrice;
      $('div[data-c-id='+id+']').find('#price').text(originalPrice + changedPrice + ' ' + currency);
    }else{
    $('div[data-c-id='+id+']').find('#price').text(0 + ' ' + currency);
    }
    }
</script>



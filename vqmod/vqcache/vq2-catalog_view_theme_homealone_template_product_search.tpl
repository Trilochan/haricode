<?php echo $header; ?><?php if( ! empty( $mfilter_json ) ) { echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; } ?>
<div style="background: #eee">
  <div class="breadCrombs">
    <div class="my-conteiner">
      <ul class="my-breadcrumb">
        <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
          <?php if (count($breadcrumbs) != ($key + 1)) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
          <?php } else { ?>
            <li><a><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
          <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div style="background: #eee">
    <div class="my-conteiner">
      <div class="leftColumn">
        <?php if (!$is_category) {
        if (!empty($category_list)) { ?>
        <div class="categories-block">
          <h5 class="categories-block-title"><?php echo $text_category_list; ?></h5>
          <ul class="finded-categories-list">
            <?php foreach ($category_list as $category) { ?>
            <li><a class="category-link" href="<?php echo $category['href']; ?>" ><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php }
        } else if ($in_category) { ?>
        <div class="categories-block">
          <h5 class="categories-block-title"><?php echo $in_category; ?></h5>
        </div>
        <?php } ?>
        <?php echo $column_left; ?>
      </div>
      <div class="mainColumn">
        
        <?php require 'catalog/view/theme/homealone/template/product/product_list.tpl'; ?>
      </div>
      <div class="rightColumn">
        <?php echo $column_right; ?>
      </div>
    </div>
  </div>
  <div style="background: #eee">
    <div class="oneColumn">
        <?php echo $content_bottom; ?>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
url = 'index.php?route=product/search';
var search = $('#content input[name=\'search\']').prop('value');
if (search) {
url += '&search=' + encodeURIComponent(search);
}
var category_id = $('#content select[name=\'category_id\']').prop('value');
if (category_id > 0) {
url += '&category_id=' + encodeURIComponent(category_id);
}
var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');
if (sub_category) {
url += '&sub_category=true';
}
var filter_description = $('#content input[name=\'description\']:checked').prop('value');
if (filter_description) {
url += '&description=true';
}
location = url;
});
$('#content input[name=\'search\']').bind('keydown', function(e) {
if (e.keyCode == 13) {
$('#button-search').trigger('click');
}
});
$('select[name=\'category_id\']').on('change', function() {
if (this.value == '0') {
$('input[name=\'sub_category\']').prop('disabled', true);
} else {
$('input[name=\'sub_category\']').prop('disabled', false);
}
});
$('select[name=\'category_id\']').trigger('change');
--></script>
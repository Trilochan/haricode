<?php echo $header; ?><?php if( ! empty( $mfilter_json ) ) { echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; } ?>
<div class="">
  <!-- bread crombs -->
  <div class="breadCrombs">
    <div class="my-conteiner">
      <ul class="my-breadcrumb">
        <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
          <?php if (count($breadcrumbs) != ($key + 1)) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
          <?php } else { ?>
            <li><a><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
          <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </div>
  <!-- finish bread crombs -->
  <div class="my-conteiner">
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2 class="hide"><?php echo $heading_title; ?></h2>
      <?php if ($products) { ?>
      <div class="oneColumn">
        <?php require_once 'catalog/view/theme/homealone/template/product/product_list.tpl'; ?>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <div><?php echo $description; ?></div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
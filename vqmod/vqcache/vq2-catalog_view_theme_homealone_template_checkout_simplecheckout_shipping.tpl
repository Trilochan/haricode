<div class="simplecheckout-block" id="simplecheckout_shipping" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>
    <?php if ($display_header) { ?>
        <div class="checkout-heading panel-heading"><?php echo $text_checkout_shipping_method ?></div>
    <?php } ?>
    <div class="simplecheckout-warning-block" <?php echo $display_error && $has_error_shipping ? '' : 'style="display:none"' ?>><?php echo $error_shipping ?></div>
    <div class="simplecheckout-block-content">
        <?php if (!empty($shipping_methods)) { ?>
            <?php if ($display_type == 2 ) { ?>
                <?php $current_method = false; ?>
                <select class="dr_select" data-onchange="reloadAll" name="shipping_method">
                    <?php foreach ($shipping_methods as $shipping_method) { ?>
                        <?php if (!empty($shipping_method['title'])) { ?>
                        <optgroup label="<?php echo $shipping_method['title']; ?>">
                        <?php } ?>
                        <?php if (empty($shipping_method['error'])) { ?>
                            <?php foreach ($shipping_method['quote'] as $quote) { ?>
                                <option value="<?php echo $quote['code']; ?>" <?php echo !empty($quote['dummy']) ? 'disabled="disabled"' : '' ?> <?php echo !empty($quote['dummy']) ? 'data-dummy="true"' : '' ?> <?php if ($quote['code'] == $code) { ?>selected="selected"<?php } ?>><?php echo $quote['title']; ?><?php echo !empty($quote['text']) ? ' - '.$quote['text'] : ''; ?></option>
                                <?php if ($quote['code'] == $code) { $current_method = $quote; } ?>
                            <?php } ?>
                        <?php } else { ?>
                            <option value="<?php echo $shipping_method['code']; ?>" disabled="disabled"><?php echo $shipping_method['error']; ?></option>
                        <?php } ?>
                        <?php if (!empty($shipping_method['title'])) { ?>
                        </optgroup>
                        <?php } ?>
                    <?php } ?>
                </select>
                <?php if ($current_method) { ?>
                    <?php if (!empty($current_method['description'])) { ?>
                        <div class="simplecheckout-methods-description"><?php echo $current_method['description']; ?></div>
                    <?php } ?>
                    <?php if (!empty($rows)) { ?>
                        <?php foreach ($rows as $row) { ?>
                          <?php echo $row ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } else { ?>

                <?php foreach ($shipping_methods as $shipping_method) { ?>
                    <?php if (!empty($shipping_method['title'])) { ?>
                    <p><b><?php echo $shipping_method['title']; ?></b></p>
                    <?php } ?>
                    <?php if (!empty($shipping_method['warning'])) { ?>
                        <div class="simplecheckout-error-text"><?php echo $shipping_method['warning']; ?></div>
                    <?php } ?>
                    <?php if (empty($shipping_method['error'])) { ?>
                        <?php foreach ($shipping_method['quote'] as $quote) { ?>
                            <div class="radio">
                                <label for="<?php echo $quote['code']; ?>">
                                    <input type="radio" data-onchange="reloadAll" name="shipping_method" <?php echo !empty($quote['dummy']) ? 'disabled="disabled"' : '' ?> <?php echo !empty($quote['dummy']) ? 'data-dummy="true"' : '' ?> value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" <?php if ($quote['code'] == $code) { ?>checked="checked"<?php } ?> />
                                    <span class="radio_button"></span>
                                    <?php if (!empty($quote['img'])) { ?>
                                        <img src="<?php echo $quote['img']; ?>" width="60" height="32" border="0" style="display:block;margin:3px;">
                                    <?php } ?>
                                    <?php echo !empty($quote['title']) ? $quote['title'] : ''; ?><?php echo !empty($quote['text']) ? ' - ' . $quote['text'] : ''; ?>
                                </label>
                            </div>
                            <?php if (!empty($quote['description'])) { ?>
                                <div class="form-group">
                                    <label for="<?php echo $quote['code']; ?>"><?php echo $quote['description']; ?></label>
                                </div>
                            <?php } ?>
                            <?php if ($quote['code'] == $code && !empty($rows)) { ?>
                                    <?php foreach ($rows as $row) { ?>
                                      <?php echo $row ?>
                                    <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="simplecheckout-error-text"><?php echo $shipping_method['error']; ?></div>
                    <?php } ?>
                <?php } ?>

            <?php } ?>
            <input type="hidden" name="shipping_method_current" value="<?php echo $code ?>" />
            <input type="hidden" name="shipping_method_checked" value="<?php echo $checked_code ?>" />
        <?php } ?>
        <?php if (empty($shipping_methods) && $address_empty && $display_address_empty) { ?>
            <div class="simplecheckout-warning-text"><?php echo $text_shipping_address; ?></div>
        <?php } ?>
        <?php if (empty($shipping_methods) && !$address_empty) { ?>
            <div class="simplecheckout-warning-text"><?php echo $error_no_shipping; ?></div>
        <?php } ?>
    </div>
</div>
<script>
  $(function () {
    <?php if($shipping_code != 'novaposhta.novaposhta') { ?>
    /****Загрузка городов с API НП по умолчанию*******/
    var city = $('input#customer_your_city');
    var city_id = $(city).attr("id");
    var city_class = $(city).attr("class");
    var city_name = $(city).attr("name");
    var city_val = $(city).val();
    var city_onchange = $(city).attr('data-onchange');

    $(city).replaceWith('<select id="' + city_id + '" class="' + city_class + '" name="' + city_name + '" data-onchange="' + city_onchange + '"><option value=""> --- <?php echo $nova_poshta_select_default; ?> --- </option></select>');


    var city = $('select#customer_your_city');

    $.ajax({
      url: 'index.php?route=checkout/simplecheckout_shipping/getNovaPoshtaCities',
      dataType: 'json',
      type: "GET",
      cache: true,
      success: function (json_city) {
        $.map(json_city.data, function (item_city) {
          var selected = "";
          if (city_val === item_city.DescriptionRu) {
            selected = "selected";
          }
          $(city).append('<option value="' + item_city.DescriptionRu + '" data-ref="' + item_city.Ref + '" ' + selected + ' >' + item_city.DescriptionRu + '</option>');
        });
        $(city).removeAttr("disabled");

      }
    });
    /***************************************/
    <?php } else { ?>
    var city = $('input#customer_your_city');
    var street = $('input#shipping_novaposhta_warehouse');

    var city_id = $(city).attr("id");
    var city_class = $(city).attr("class");
    var city_name = $(city).attr("name");
    var city_val = $(city).val();
    var city_onchange = $(city).attr('data-onchange');

    var street_id = $(street).attr("id");
    var street_class = $(street).attr("class");
    var street_name = $(street).attr("name");
    var street_val = $(street).val();
    var street_onchange = $(street).attr('data-onchange');

    $(city).replaceWith('<select id="' + city_id + '" class="' + city_class + '" name="' + city_name + '" data-onchange="' + city_onchange + '"><option value=""> --- <?php echo $nova_poshta_select_default; ?> --- </option></select>');
    $(street).replaceWith('<select id="' + street_id + '" class="' + street_class + '" name="' + street_name + '" data-onchange="' + street_onchange + '"><option value=""> --- <?php echo $nova_poshta_select_default; ?> --- </option></select>');

    var city = $('select#customer_your_city');
    var street = $('select#shipping_novaposhta_warehouse');

    $(city).attr("disabled", "true");
    $(street).attr("disabled", "true");

    $.ajax({
      url: 'index.php?route=checkout/simplecheckout_shipping/getNovaPoshtaCities',
      dataType: 'json',
      type: "GET",
      cache: true,
      success: function (json_city) {
        $.map(json_city.data, function (item_city) {
          var selected = "";
          if (city_val === item_city.DescriptionRu) {
            selected = "selected";
          }
          $(city).append('<option value="' + item_city.DescriptionRu + '" data-ref="' + item_city.Ref + '" ' + selected + ' >' + item_city.DescriptionRu + '</option>');
        });
        $(city).removeAttr("disabled");
        if (city_val !== "") {
          var ref = $(city).find(':selected').attr('data-ref');
          $.ajax({
            url: 'index.php?route=checkout/simplecheckout_shipping/getNovaPoshtaWarehouses&city_ref=' + ref,
            dataType: 'json',
            type: "GET",
            success: function (json_warehouse) {
              $(street).html('<option value=""> --- <?php echo $nova_poshta_select_default; ?> --- </option>');
              $.map(json_warehouse.data, function (item_warehouse) {
                var selected = "";
                if (street_val === item_warehouse.DescriptionRu) {
                  selected = "selected";
                }
                $(street).append('<option data-ref="' + item_warehouse.Ref + '" value=\'' + item_warehouse.DescriptionRu + '\' ' + selected + ' >' + item_warehouse.DescriptionRu + '</option>');
              });
              $(street).removeAttr("disabled");
            }
          });
        }
      }
    });

    $(city).change(function (obj) {
      $(street).attr("disabled", "true");
      var value = $(this).val();
      if (value.length !== 0) {
        var ref = $(this).find(':selected').attr('data-ref');

        $.ajax({
          url: 'index.php?route=checkout/simplecheckout_shipping/getNovaPoshtaWarehouses&city_ref=' + ref,
          dataType: 'json',
          type: "GET",
          success: function (json_warehouse) {
            $(street).html('<option value=""> --- <?php echo $nova_poshta_select_default; ?> --- </option>');
            $.map(json_warehouse.data, function (item_warehouse) {
              $(street).append('<option data-ref="' + item_warehouse.Ref + '" value=\'' + item_warehouse.DescriptionRu + '\'>' + item_warehouse.DescriptionRu + '</option>');
            });
            $(street).removeAttr("disabled");
          }
        });
      }
    });
    <?php }?>
  });
</script>
            
<?php if (!$ajax && !$popup && !$as_module) { ?>
<?php $simple_page = 'simpleedit'; include $simple_header; ?>
<div class="simple-content">
<?php } ?>
    <?php if (!$ajax || ($ajax && $popup)) { ?>
    <script type="text/javascript">
    (function($) {
    <?php if (!$popup && !$ajax) { ?>
        $(function(){
    <?php } ?>
            if (typeof Simplepage === "function") {
                var simplepage = new Simplepage({
                    additionalParams: "<?php echo $additional_params ?>",
                    additionalPath: "<?php echo $additional_path ?>",
                    mainUrl: "<?php echo $action; ?>",
                    mainContainer: "#simplepage_form",
                    scrollToError: <?php echo $scroll_to_error ? 1 : 0 ?>,
                    javascriptCallback: function() {<?php echo $javascript_callback ?>}
                });

                simplepage.init();
            }
    <?php if (!$popup && !$ajax) { ?>
        });
    <?php } ?>
    })(jQuery || $);
    </script>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="simplepage_form">
        <div class="simpleregister" id="simpleedit">
            <?php if ($error_warning) { ?>
            <div class="warning alert alert-danger"><?php echo $error_warning; ?></div>
            <?php } ?>
            <div class="simpleregister-block-content">
                <?php foreach ($rows as $row) { ?>
                  <?php echo $row ?>
                <?php } ?>
            </div>
            <div class="simpleregister-button-block buttons">
                <div class="simpleregister-button-right">
                    <a class="button btn-primary button_oc btn account_button" data-onclick="submit" id="simpleregister_button_confirm"><span><?php echo $button_continue; ?></span></a>
                </div>
            </div>
        </div>
        <?php if ($redirect) { ?>
            <script type="text/javascript">
                location = "<?php echo $redirect ?>";
            </script>
        <?php } ?>
    </form>
<?php if (!$ajax && !$popup && !$as_module) { ?>
</div>
<?php include $simple_footer ?>
<?php } ?>
<script>
  $(function () {
    var city = $('input#edit_your_city');
    var street = $('input#edit_novaposhta_warehouse');

    var city_id = $(city).attr("id");
    var city_class = $(city).attr("class");
    var city_name = $(city).attr("name");
    var city_val = $(city).val();
    var city_onchange = $(city).attr('data-onchange');

    var street_id = $(street).attr("id");
    var street_class = $(street).attr("class");
    var street_name = $(street).attr("name");
    var street_val = $(street).val();
    var street_onchange = $(street).attr('data-onchange');

    $(city).replaceWith('<select id="' + city_id + '" class="' + city_class + '" name="' + city_name + '" data-onchange="' + city_onchange + '"><option value=""> --- Выберите --- </option></select>');
    $(street).replaceWith('<select id="' + street_id + '" class="' + street_class + '" name="' + street_name + '" data-onchange="' + street_onchange + '"><option value=""> --- Выберите --- </option></select>');

    var city = $('select#edit_your_city');
    var street = $('select#edit_novaposhta_warehouse');

    $(city).attr("disabled", "true");
    $(street).attr("disabled", "true");

    $.ajax({
      url: 'index.php?route=checkout/simplecheckout_shipping/getNovaPoshtaCities',
      dataType: 'json',
      type: "GET",
      cache: true,
      success: function (json_city) {
        $.map(json_city.data, function (item_city) {
          var selected = "";
          if (city_val === item_city.DescriptionRu) {
            selected = "selected";
          }
          $(city).append('<option value="' + item_city.DescriptionRu + '" data-ref="' + item_city.Ref + '" ' + selected + ' >' + item_city.DescriptionRu + '</option>');
        });
        $(city).removeAttr("disabled");
        if (city_val !== "") {
          var ref = $(city).find(':selected').attr('data-ref');
          $.ajax({
            url: 'index.php?route=checkout/simplecheckout_shipping/getNovaPoshtaWarehouses&city_ref=' + ref,
            dataType: 'json',
            type: "GET",
            success: function (json_warehouse) {
              $(street).html('<option value=""> --- Выберите --- </option>');
              $.map(json_warehouse.data, function (item_warehouse) {
                var selected = "";
                if (street_val === item_warehouse.DescriptionRu) {
                  selected = "selected";
                }
                $(street).append('<option data-ref="' + item_warehouse.Ref + '" value=\'' + item_warehouse.DescriptionRu + '\' ' + selected + ' >' + item_warehouse.DescriptionRu + '</option>');
              });
              $(street).removeAttr("disabled");
            }
          });
        }
      }
    });

    $(city).change(function (obj) {
      $(street).attr("disabled", "true");
      var value = $(this).val();
      if (value.length !== 0) {
        var ref = $(this).find(':selected').attr('data-ref');

        $.ajax({
          url: 'index.php?route=checkout/simplecheckout_shipping/getNovaPoshtaWarehouses&city_ref=' + ref,
          dataType: 'json',
          type: "GET",
          success: function (json_warehouse) {
            $(street).html('<option value=""> --- Выберите --- </option>');
            $.map(json_warehouse.data, function (item_warehouse) {
              $(street).append('<option data-ref="' + item_warehouse.Ref + '" value=\'' + item_warehouse.DescriptionRu + '\'>' + item_warehouse.DescriptionRu + '</option>');
            });
            $(street).removeAttr("disabled");
          }
        });
      }
    });
  });
</script>
            
<div id="cart" class="btn-group btn-block">
  <button type="button" data-toggle="modal" data-target="#cart_modal" data-loading-text="<?php echo $text_loading; ?>" class="my-header-cart-button"><span id="cart-total"><?php echo $text_cart_header; ?> <span class="my-shop-cart-total">(<?php echo $total_product; ?>)</span></span><span class="my-shop-cart"></span></button>

  <div class="modal fade korzina" id="cart_modal" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?php echo $text_cart; ?></h4>
        </div>
        <div class="modal-body">
          <?php if ($products || $vouchers || $propositions) { ?>
            <div>
              <table class="table">
                <thead>

                <?php foreach ($products as $product) { ?>
                  <tr>
                    <td class="product-image"><?php if ($product['thumb']) { ?>
                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail my-img-thumbnail"/></a>
                      <?php } ?></td>
                    <td class="text-left product-name">
                      <span style="color: #8d8d8d;" class="model"><?php echo $text_model . $product['model']; ?></span><br/>
                      <a style="color: #fb6362;" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                      <?php if ($product['option']) { ?>
                        <?php foreach ($product['option'] as $option) { ?>
                          <br/>
                          -
                          <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                        <?php } ?>
                      <?php } ?>
                      <?php if ($product['recurring']) { ?>
                        <br/>
                        -
                        <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                      <?php } ?></td>
                    <td class="text-center product-number">
                      <div style="position: relative;">
                        <i class="fa fa-angle-down minus-modal" id="minus" onclick="cart.quantity.minus('<?php echo $product['key']; ?>', $(this).parent().find('input').val());"></i>
                        <input class="modal-cart-input-quan" type="text" size="3" value="<?php echo $product['quantity']; ?>" onkeypress="cart.validateInteger(event)" onchange="cart.quantity.update('<?php echo $product['key']; ?>', $(this).val())"/>
                        <i class="fa fa-angle-up plus-modal" id="plus" onclick="cart.quantity.plus('<?php echo $product['key']; ?>', $(this).parent().find('input').val());"></i>
                      </div>
                    </td>
                    <td style="color: #000;font-size: 15px;" class="text-right price"><?php echo $product['total']; ?></td>
                    <td class="text-center closeButtonModal">
                      <button type="button" onclick="cart.remove('<?php echo $product['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-close fontClose"><i class="fa fa-times"></i></button>
                    </td>
                  </tr>
                <?php } ?>

<?php foreach ($propositions as $proposition) { ?>
                  <?php foreach ($proposition['products'] as $product) { ?>
                  <tr>
                    <td class="product-image"><?php if ($product['thumb']) { ?>
                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail my-img-thumbnail"/></a>
                      <?php } ?></td>
                    <td class="text-left product-name">
                      <span style="color: #8d8d8d;" class="model"><?php echo $text_model . $product['model']; ?></span><br/>
                      <a style="color: #fb6362;" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                      <?php if ($product['option']) { ?>
                        <?php foreach ($product['option'] as $option) { ?>
                          <br/>
                          -
                          <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                        <?php } ?>
                      <?php } ?>
                      <?php if ($product['recurring']) { ?>
                        <br/>
                        -
                        <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                      <?php } ?></td>
                    <?php if ($product == $proposition['products'][0]) { ?>
                    <td class="text-center product-number" style="vertical-align: middle" rowspan="<?php echo count($proposition['products']); ?>">
                      <div style="position: relative;">
                        <i class="fa fa-angle-down minus-modal" id="minus" onclick="proposition.quantity.minus('<?php echo $proposition['key']; ?>', $(this).parent().find('input').val());"></i>
                        <input class="modal-cart-input-quan" type="text" size="3" value="<?php echo $proposition['quantity']; ?>" onchange="proposition.quantity.update('<?php echo $proposition['key']; ?>', this.value)" />
                        <i class="fa fa-angle-up plus-modal" id="plus" onclick="proposition.quantity.plus('<?php echo $proposition['key']; ?>', $(this).parent().find('input').val());"></i>
                      </div>
                    </td>
                    <td class="text-right price" style="color: #000;font-size: 15px; vertical-align: middle" rowspan="<?php echo count($proposition['products']); ?>"><?php echo $proposition['total']; ?></td>
                    <td class="text-center closeButtonModal" style="vertical-align: middle" rowspan="<?php echo count($proposition['products']); ?>">
                      <button type="button" onclick="proposition.remove('<?php echo $proposition['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-close fontClose profitPropos"><i class="fa fa-times"></i></button>
                    </td>
                    <?php } ?>
                  </tr>
                  <?php } ?>
                <?php } ?>
			
                <?php foreach ($vouchers as $voucher) { ?>
                  <tr>
                    <td class="text-center"></td>
                    <td class="text-left"><?php echo $voucher['description']; ?></td>
                    <td class="text-right">x&nbsp;1</td>
                    <td class="text-right"><?php echo $voucher['amount']; ?></td>
                    <td class="text-center text-danger">
                      <button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>
                    </td>
                  </tr>
                <?php } ?>
              </table>
            </div>
            <h4  class="dop_discount_h4" ><?php echo $text_discount; ?><h4>
            <div>
              <div class="clearfix">

                <?php foreach ($totals as $total) { ?>
                <?php if($total['code'] == 'total') { ?>
                <div class="total-modal-block">
                  <p class="total-modal-right"><span><?php echo $total['title']; ?>: </span><?php echo $total['text']; ?></p>
                </div>
                <?php } ?>
                <?php } ?>
              </div>

            </div>
          <?php } else { ?>
            <div>
              <p class="text-center"><?php echo $text_empty; ?></p>
            </div>
          <?php } ?>
        </div>
        <div style="border: none;padding-top: 0px;" class="modal-footer">
          <?php $i = 0; ?>
          <?php if (!empty($products_featured)) { ?>
          <h3 style="text-align: center;margin-top: 0px"><?php echo $text_featured; ?></h3>
          <div class="col-md-12" style="margin-top: 15px; padding: 0;">
            <?php foreach ($products_featured as $product)if ($i++ < 3) { ?>
            <div style="padding: 0px;" class="col-md-4">
              <div class="modal-product-block">
                <div style="float: left;width: 40%;">
                  <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail my-img-thumbnail"/>
                  </a>
                </div>
                <div class="nameModalBlock" style="float: left;width: 60%;">
                  <a style="color: #fb6362;" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                </div>
                <div class="modal-price-block">
                  <?php if ($product['special']) { ?>
                  <span class="modal-fute-price"><?php echo $product['special']; ?></span>
                  <?php } else { ?>
                  <span class="modal-fute-price"><?php echo $product['price']; ?></span>
                  <?php } ?>
                  <button class="my-slider-product-buy-modal" onclick="cart.add('<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?><span class="fa fa-shopping-cart my-buy-cart"></button>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
          <?php } elseif (!empty($products_related))  { ?>
          <h3 style="text-align: center;margin-top: 0px;"><?php echo $text_related; ?></h3>
          <div  class="col-md-12" style="margin-top: 15px;">
            <?php foreach ($products_related as $product)if ($i++ < 3) { ?>
              <div style="padding: 0px;" class="col-md-4">
                <div class="modal-product-block">
                <div style="float: left;width: 40%;">
                  <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail my-img-thumbnail"/>
                  </a>
                </div>
                <div style="float: left;width: 60%;">
                  <a style="color: #fb6362;" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                </div>
                <div class="modal-price-block">
                    <?php if ($product['special']) { ?>
                      <span class="modal-fute-price"><?php echo $product['special']; ?></span>
                    <?php } else { ?>
                      <span class="modal-fute-price"><?php echo $product['price']; ?></span>
                    <?php } ?>
                  <button class="my-slider-product-buy-modal" onclick="cart.add('<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?><span class=" my-buy-cart"></button>
                </div>
                </div>
              </div>
            <?php } ?>
          </div>
          <?php } ?>
          <?php if ($products || $vouchers || $propositions) { ?>
          <p style="float: right;margin-top: 20px;" class="wrapper-button">
            <p data-toggle="modal" data-target="#cart_modal" class="modal-next-button"><i class="fa fa-angle-double-left"></i><?php echo $text_cart_continue_shipping; ?></p>
            <a href="<?php echo $checkout; ?>" class="pull-right my-button-custom"><?php echo $text_checkout; ?></a>
          </p>
          <?php } ?>
          <div style="display: none;" id="fast-order">
            <form name="fast_order_modal_cart" id="fast_order_modal_cart" action="javascript: void(null);" onsubmit="fast_order.add(this);" class="text-center">
              <p class="text-danger hidden"></p>

              <div class="form-group">
                <input type="text" name="telephone" placeholder="+38 (___) ___-__-__"/>
                <input type="submit" name="submit" value="<?php echo $button_fast_order; ?>"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).on('shown.bs.modal', function (e) {
    $(e.target).before($(e.target).find('.modal-backdrop').clone().css('z-index', $(e.target).css('z-index')-1)); 
    $(e.target).find('.modal-backdrop').removeClass('in').css('transition', 'none');
});

$(document).on('hide.bs.modal', function (e) {
    $(e.target).prev('.modal-backdrop').remove();
});
</script>

<?php echo $header; ?><?php if( ! empty( $mfilter_json ) ) { echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; } ?>
<!-- start category -->
<div style="background: #eee">
<!-- bread crombs -->
  <div class="breadCrombs">
    <div class="my-conteiner">
      <ul class="my-breadcrumb">
        <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
          <?php if (count($breadcrumbs) != ($key + 1)) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
          <?php } else { ?>
            <li><a><?php echo $breadcrumb['text']; ?><span>>></span></a></li>
          <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </div>
  <!-- start content category -->
  <div style="background: #eee">
    <div class="my-conteiner">
      <div class="">
<?php //###################### SMART FILTER BEGIN ########################## ?>

        <input id="count_products" type="hidden" value="<?php echo $tot_prod;?>">
<?php //###################### SMART FILTER BEGIN ########################## ?>
        
        <div class="leftColumn">
          <?php echo $column_left; ?>
        </div>
        <div class="mainColumn">
          <?php require 'catalog/view/theme/homealone/template/product/product_list.tpl'; ?>

        </div>
        <div class="rightColumn">
          <?php echo $column_right; ?>
          <!-- <div class="my-home-special-block">
            <img src="catalog/view/theme/homealone/image/special1.jpg">
          </div>
          <div class="my-home-special-block">
            <img src="catalog/view/theme/homealone/image/special2.jpg">
          </div>
          <div class="my-home-special-block">
            <img src="catalog/view/theme/homealone/image/special3.jpg">
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <div style="background: #eee">
    <div class="my-conteiner">
      <div class="oneColumn">
        <?php echo $content_bottom; ?>
        <?php //###################### SMART FILTER BEGIN ########################## ?>
        <div class="seo">
      <div class="seo-cont">
      <h1>
        <?php echo $heading_title; ?></h1>
      <?php echo $description; ?></div>
      </div>
<?php //###################### SMART FILTER END ########################## ?>
      </div>
      <div class="row for-top-buy">
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<?php if ($categories) { ?>
<script type="text/html" id="bottom-block-sell-top">
<% if(main.sell.length > 0 ) { %>
<h3 style="padding: 0px 30px" class="my-product-slider-heading"><%= name %></h3>
<div style="" class="my-product-slider-url2">
  <% _.each(main.sell, function(main) { %>
  <div class="product-list col-md-3 col-sm-4">
    <div class="my-slider-product-block-catalog">
      <a href="<?php echo $product['href']; ?>">
        <img src="<%= main.image %>" class="my-slider-product-img"/>
      </a>
      <a href="<?php echo $product['href']; ?>"><p class="my-slider-product-name"><%= main.name %></p></a>
      <div class="my-slider-product-button-block">
        <% var totaltotal = sellProd.reviewSort(main.total_review,main.review); %>
        <p class="my-catalog-product-ratang">
          <% if(isNaN(totaltotal)) { %>
          <span class="my-cat-star-no"></span>
          <span class="my-cat-star-no"></span>
          <span class="my-cat-star-no"></span>
          <span class="my-cat-star-no"></span>
          <span class="my-cat-star-no"></span>
          <% } else { %>
          <% for(var i = 1; i <= 5; i++) { %>
          <% if(totaltotal < i) { %>
          <span class="my-cat-star-no"></span>
          <% } else { %>
          <span class="my-cat-star-yes"></span>
          <% } %>
          <% } %>
          <% } %>
        </p>
        <span class="my-slider-product-review">Отзывов (<%= main.total_review %>)</span>
        <button class="my-slider-wishlist" onclick="wishlist.add(<%= main.product_id %>);"><span class="fa fa-heart"></span></button>
        <button class="my-slider-compare" onclick="compare.add(<%= main.product_id %>);"><span class="fa fa-balance-scale"></span></button>
      </div>
      <div class="my-slider-product-button-block2">
        <% if(main.special <= 0) { %>
        <p class="my-slider-product-price"><%= main.price %></p>
        <% } else { %>
        <p class="my-slider-product-price">
          <span class="my-slider-product-special"><%= main.special %></span>
          <span class="my-slider-product-nospecial"><%= main.price %></span>
        </p>
        <% } %>
        <button class="my-slider-product-buy" onclick="cart.add(<%= main.product_id %>);"><?php echo $button_cart; ?><span class="fa fa-shopping-cart my-buy-cart"></span></button>
      </div>
    </div>
  </div>
  <% }) %>
</div>
<% loadSlick('.my-product-slider-url2'); %>
<% } %>
</script>
<script>
$(document).ready(function(){
sellProd.get(3,'.for-top-buy','Топ продаж данной категории','#bottom-block-sell-top',sellProd.getUrl('path').substring(0,2));
});
</script>
<script>
$('.subcategory-list').slick({
accessibility: true,
adaptiveHeight: true,
arrows: true,
dots: false,
draggable: true,
infinite: true,
slidesToShow: 6,
slidesToScroll: 1,
swipe: true,
responsive: [
{
breakpoint: 1024,
settings: {
slidesToShow: 4
}
},
{
breakpoint: 600,
settings: {
slidesToShow: 2
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1
}
}
]
});

$('.my-product-slider-url').slick({
slidesToShow: 6,
slidesToScroll: 1,
draggable: true,
arrows: true,
swipeToSlide: true,
responsive: [
{
breakpoint: 1360,
settings: {
slidesToShow: 5,
slidesToScroll: 1,
}
},
{
breakpoint: 1200,
settings: {
slidesToShow: 4,
slidesToScroll: 1,
}
},
]
});
</script>
<?php } ?>
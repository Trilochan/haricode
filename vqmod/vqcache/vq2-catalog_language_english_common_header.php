<?php
// Text

				/**
					*Ajax advanced search starts
					*/
				$_['ajaxadvancedsearch_model'] = 'Model :';
				$_['ajaxadvancedsearch_manufacturer'] = 'Manufacturer :';
				$_['ajaxadvancedsearch_price'] = 'Price :';
				$_['ajaxadvancedsearch_stock'] = 'Stock status :';
				$_['ajaxadvancedsearch_quantity'] = 'Quantity :';
				$_['ajaxadvancedsearch_rating'] = 'Rating :';
				$_['ajaxadvancedsearch_allresults'] = 'All results';
					/**
					*Ajax advanced search ends
					*/
			
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';

				$_['text_callback'] = 'Callback';
			
$_['text_logged']        = 'Account';
$_['text_contact']       = 'Contact';
$_['text_callback']      = 'Request a call';
$_['text_greet']         = 'Welcome!';
$_['text_register_success'] = 'Thank you for registering. Now you can fully make purchases in our online store.';

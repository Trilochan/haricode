<?php
// Text

				/**
					*Ajax advanced search starts
					*/
				$_['ajaxadvancedsearch_model'] = 'Модель:';
				$_['ajaxadvancedsearch_manufacturer'] = 'Производитель:';
				$_['ajaxadvancedsearch_price'] = 'Цена:';
				$_['ajaxadvancedsearch_stock'] = 'Статус на складе:';
				$_['ajaxadvancedsearch_quantity'] = 'Количество:';
				$_['ajaxadvancedsearch_rating'] = 'Рейтинг:';
				$_['ajaxadvancedsearch_allresults'] = 'Все результаты';
					/**
					*Ajax advanced search ends
					*/
			
$_['text_home']          = 'Главная';
$_['text_wishlist']      = 'Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'Загрузки';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Смотреть Все';

				$_['text_callback'] = 'Обратный звонок';
			
$_['text_logged']        = 'Личный кабинет';
$_['text_contact']       = 'Контакты';
$_['text_callback']      = 'Обратный звонок';
$_['text_greet']         = 'Приветствуем!';
$_['text_register_success'] = 'Спасибо за регистрацию. Теперь Вы можете полноценно совершать покупки в нашем интернет-магазине.';

$_['your_city'] = 'Ваш город:';
$_['your_city_yes'] = 'Да';
$_['your_city_no'] = 'Нет';
$_['select_your_city'] = 'Выберите свой город';
$_['your_city_kiev'] = 'Киев';
$_['your_city_charkov'] = 'Харьков';
$_['your_city_lviv'] = 'Львов';
$_['your_city_dnepr'] = 'Днепропетровск';
$_['select_your_city_other'] = 'Или введите другой населенный пункт Украины';
$_['your_city_placeholder'] = 'Ваш город';
$_['your_city_info'] = 'Выбор города поможет быстрее оформить заказ<br> Это сохранит больше свободного времени для Вас!';
$_['text_compare'] = 'Сравнения';
$_['text_wishlist'] = 'Желания';
$_['text_login_enter'] = 'Войти';
$_['text_register_submit'] = 'Зарегистрироваться';
$_['text_login_socials'] = 'Или войдите через соцсети';
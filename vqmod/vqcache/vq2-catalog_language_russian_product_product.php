<?php
// Text
$_['text_search']                             = 'Поиск';
$_['text_brand']                              = 'Производитель';
$_['text_manufacturer']                       = 'Производитель:';
$_['text_model']                              = "Код товара:"; /*suppler*/
$_['text_model']                              = 'Артикул:';
$_['text_reward']                             = 'Бонусные баллы:';
$_['text_points']                             = 'Цена в бонусных баллах:';
$_['text_stock']                              = 'Наличие:';
$_['text_instock']                            = 'Есть в наличии';
$_['text_tax']                                = 'Без НДС:';
$_['text_discount']                           = ' или более: ';
$_['text_option']                             = 'Доступные варианты';
$_['text_minimum']                            = 'Минимальное количество для заказа: %s';
$_['text_reviews']                            = '%s отзывов';
$_['text_write']                              = 'Написать отзыв';
$_['text_login']                              = 'Пожалуйста <a href="%s">авторизируйтесь</a> или <a href="%s">создайте учетную запись</a> перед тем как написать отзыв';
$_['text_no_reviews']                         = 'Нет отзывов о данном товаре.';
$_['text_note']                               = '<span style="color: #FF0000;">Примечание:</span> HTML разметка не поддерживается! Используйте обычный текст.';
$_['text_success']                            = 'Спасибо за ваш отзыв. Он поступил администратору для проверки на спам и вскоре будет опубликован.';
$_['text_success_qaa']                            = 'Дякуємо за Ваше запитання. Він направлений для затвердження модератору.';
$_['text_related']                            = 'Рекомендуемые товары';
$_['text_tags']                               = '<i class="fa fa-tags"></i>';
$_['text_error']                              = 'Товар не найден!';
$_['text_payment_recurring']                    = 'Платежный профиль';
$_['text_trial_description']                  = 'Сумма: %s; Периодичность: %d %s; Кол-во платежей: %d, Далее ';
$_['text_payment_description']                = 'Сумма: %s; Периодичность:  %d %s; Кол-во платежей:  %d ';
$_['text_payment_until_canceled_description'] = 'Сумма: %s; Периодичность:  %d %s ; Кол-во платежей: до отмены';
$_['text_day']                                = 'день';
$_['text_week']                               = 'недели';
$_['text_semi_month']                         = 'полмесяца';
$_['text_month']                              = 'месяц';
$_['text_year']                               = 'год';
$_['text_no_video']                           = 'К данному товару пока нет видеообзора';
$_['text_add_complect']                       = 'Добавить в комплект';
$_['text_product_added']                      = 'Товар добавлен';
$_['text_details']                            = 'Подробнее';

$_['text_one_click_buy'] = 'Купить в один клик';
$_['text_one_click_buy_success'] = 'Спасибо за покупку! В ближайшее время с Вами свяжется наш менеджер.';
$_['text_one_click_buy_close'] = 'Закрыть';

//complect
$_['text_complect_head'] = 'Вы покупаете %s из %s товаров комплекта';
$_['text_complect_get_all'] = 'Выбрать все товары комплекта';
$_['text_complect_get_all_discount'] = 'Скидка %s%';
$_['text_complect_dimensions'] = '%s см';
$_['text_complect_add'] = 'Добавить в комплект';
$_['text_complect_header'] = '<h2>Вы покупаете <span id="count">0</span> из %s товаров комплекта</h2>';
$_['text_complect_not_enough'] = 'Подберите товар более %s';
//********

// Entry
$_['entry_qty']                               = 'Количество';
$_['entry_name']                              = 'Ваше имя:';
$_['entry_review']                            = 'Ваш отзыв:';
$_['entry_rating']                            = 'Оценка:';
$_['entry_good']                              = 'Хорошо';
$_['entry_bad']                               = 'Плохо';
$_['entry_captcha']                           = 'Введите код, указанный на картинке:';

// Tabs
$_['tab_description']                         = 'Описание';
$_['tab_attribute']                           = 'Характеристики';
$_['tab_review']                              = '(%s) отзыва';
$_['tab_reviews']                             = 'Отзывы';
$_['tab_video']                               = 'Видеообзор';
$_['tab_all']                                 = 'Описание и Характеристики';
$_['button_continue']                         = 'Отправить отзыв';

// Error
$_['error_name']                              = 'Имя должно быть от 3 до 25 символов!';
$_['error_text']                              = 'Текст отзыва должен быть от 25 до 1000 символов!';
$_['error_rating']                            = 'Пожалуйста, выберите оценку!';
$_['error_captcha']                           = 'Код, указанный на картинке, введен неверно!';

// qaa
$_['text_answer']    = 'Ответ';
$_['text_write_qaa'] = 'Написать вопрос';
$_['text_no_qaas']   = 'Нет вопросов о данном товаре.';

$_['entry_qaa'] = 'Ваш вопрос:';

$_['tab_qaa'] = 'Вопросов (%s)';
				$_['text_proposition']                 = 'Выгодное предложение';
			
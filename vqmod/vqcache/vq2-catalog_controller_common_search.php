<?php
class ControllerCommonSearch extends Controller {
	public function index() {
		$this->load->language('common/search');

		$data['text_search'] = $this->language->get('text_search');

                $data['popular_searches'] = false;
                if ($this->config->get('search_key_status')) {
                  $data['popular_searches'] = true;

                  $data['random_tags'] = array();

                  $this->load->language('module/search_key');

                  $data['text_here_searched'] = $this->language->get('text_here_searched');


                  $searches        = $this->config->get('search_key_module_description');
                  $language_id = $this->config->get('config_language_id');

                  if (!empty($searches[$language_id])) {
                    foreach ($searches[$language_id] as $search) {
                      if (!empty($search)) {
                        $tags         = explode(';', $search);

                        if(end($tags) == "") {
                            array_pop($tags);
                        }

                        if (count($tags) == 1 || count($tags) == 0) {
                            $data['popular_searches'] = false;
                        } else {
                          $random_tag_keys    = array_rand($tags, 2);
                          $random_tags = array(trim($tags[$random_tag_keys[0]]), trim($tags[$random_tag_keys[1]]));
                          foreach ($random_tags as $random_tag) {
                            $data['random_tags'][] = $random_tag;
                          }
                        }

                      } else {
                        $data['popular_searches'] = false;
                      }
                    }

                  } else {
                    $data['popular_searches'] = false;
                  }
                }
			
		$data['text_find_product'] = $this->language->get('text_find_product');

		if (isset($this->request->get['search'])) {
			$data['search'] = $this->request->get['search'];
		} else {
			$data['search'] = '';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/search.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/search.tpl', $data);
		} else {
			return $this->load->view('default/template/common/search.tpl', $data);
		}
	}
}
<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');

				$data['text_callback_header'] = $this->language->get('text_callback_header');
				$data['text_callback_name'] = $this->language->get('text_callback_name');
				$data['text_callback_telephone'] = $this->language->get('text_callback_telephone');
				$data['text_callback_submit'] = $this->language->get('text_callback_submit');
				$data['text_callback_success_header'] = $this->language->get('text_callback_success_header');
				$data['text_callback_success'] = $this->language->get('text_callback_success');
				$data['callback_error_name'] = $this->language->get('callback_error_name');
				$data['callback_error_phone'] = $this->language->get('callback_error_phone');
				$data['text_callback_email_subject'] = $this->language->get('text_callback_email_subject');
			
		$data['telephone'] = explode(',',$this->config->get('config_telephone'));
		$data['shop_email'] = $this->config->get('config_email');
		$data['address'] = nl2br($this->config->get('config_address'));

		// if (!$this->language->get('code') == 'ru') {
	 //      $data['logo-lang_ukr'] = true;
	 //    } else {
	 //      $data['logo-lang_ukr'] = false;
	 //    }

		$data['work_time'] = $this->language->get('work_time');
		$data['order_time'] = $this->language->get('order_time');
		$data['text_information'] = $this->language->get('text_information');
		$data['text_gift_for_all'] = $this->language->get('text_gift_for_all');
		$data['text_gift_take_naw'] = $this->language->get('text_gift_take_naw');
		$data['text_developer'] = $this->language->get('text_developer');

		if ($this->language->get('code') == 'uk') {
			$data['language_code'] = 'uk';
		} else if ($this->language->get('code') == 'ru') {
			$data['language_code'] = 'ru';
		}

		/***SOCIAL***/
		$socials  = array();
		$data['socials'] = array();

		if($this->config->get('config_social')){
			$socials= $this->config->get('config_social');
		}else{
			$socials = array();
		}
		$this->load->model('tool/image');
		if($socials){
			foreach($socials as $social){
				$sort_keys[] = $social['sort_order'];
			}

			array_multisort($sort_keys, $socials);

			foreach ($socials as $social) {
				
				$thumb = $social['image'];
				
				$data['socials'][] = array(
					'thumb' => $this->model_tool_image->resize($thumb, 35, 35),
					'name'  => $social['soc_name'],
					'href'  => $social['href'],
				);
			}
		}
		/***AND SOCIAL***/

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom'] || $result['bottom_infoblock']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'bottom' => $result['bottom'],
					'bottom_infoblock' => $result['bottom_infoblock'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		if ($this->request->server['HTTPS']) {
	      $server = $this->config->get('config_ssl');
	    } else {
	      $server = $this->config->get('config_url');
	    }

	    $data['name'] = $this->config->get('config_name');
	    $data['home'] = $this->url->link('common/home');

	    if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
	      $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
	    } else {
	      $data['logo'] = '';
	    }

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);
		}


				$data['text_register_header'] = $this->language->get('text_register_header');
				$data['entry_register_email'] = $this->language->get('entry_register_email');
				$data['entry_register_password'] = $this->language->get('entry_register_password');
				$data['text_register_success_header'] = $this->language->get('text_register_success_header');
				$data['text_register_success'] = $this->language->get('text_register_success');
				$data['text_button_register'] = $this->language->get('text_button_register');

				$data['text_login_header'] = $this->language->get('text_login_header');
				$data['entry_login_email'] = $this->language->get('entry_login_email');
				$data['entry_login_password'] = $this->language->get('entry_login_password');
				$data['entry_login_remember'] = $this->language->get('entry_login_remember');
				$data['text_button_login'] = $this->language->get('text_button_login');
				$data['text_login_forgotten_password'] = $this->language->get('text_login_forgotten_password');

				$data['ulogin_form_marker'] = $this->load->controller('module/ulogin');
			

                $this->load->model('account/customer');

                $data['register_confirm'] = false;
                $data['registered'] = false;
                if(isset($this->request->get['email']) && isset($this->request->get['key'])) {
                    $data['register_confirm'] = true;
                    $data['register_confirm_email'] = $this->request->get['email'];
                    $data['register_confirm_key'] = $this->request->get['key'];

                    if(!$this->model_account_customer->getConfirm($data['register_confirm_email'], $data['register_confirm_key'])) {
                        $data['registered'] = true;
                        $data['text_registered'] = $this->language->get('text_registered');
                    }
                }
            
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}
}